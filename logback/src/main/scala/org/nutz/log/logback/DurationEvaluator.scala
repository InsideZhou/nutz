package org.nutz.log.logback

import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.boolex.EventEvaluatorBase
import org.nutz.lang.Times

import scala.beans.BeanProperty

class DurationEvaluator extends EventEvaluatorBase[ILoggingEvent] {
    @BeanProperty
    var duration = "5 minutes"

    var lastTime: Long = System.currentTimeMillis()

    override def evaluate(event: ILoggingEvent) = {
        val now = System.currentTimeMillis()

        if (now - lastTime >= Times.parseDuration(duration)) {
            lastTime = now
            true
        }
        else {
            false
        }
    }
}
