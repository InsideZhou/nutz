package org.nutz.log.logback

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.boolex.EventEvaluatorBase

import scala.beans.BeanProperty

class LevelEvaluator extends EventEvaluatorBase[ILoggingEvent] {
    @BeanProperty
    var level = "ERROR"

    override def evaluate(event: ILoggingEvent) = {
        event.getLevel.isGreaterOrEqual(Level.toLevel(level))
    }
}
