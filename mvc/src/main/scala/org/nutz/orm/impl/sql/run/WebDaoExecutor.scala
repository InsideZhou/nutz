package org.nutz.orm.impl.sql.run

import org.nutz.log.Logs
import org.nutz.orm.sql.DaoStatement
import org.nutz.web.WebUtils
import org.nutz.web.mvc.MvcUtils

class WebDaoExecutor extends NutDaoExecutor {
    protected val log = Logs.getLog(classOf[WebDaoExecutor])

    override protected def afterExec(stmt: DaoStatement): Unit = {
        val context = MvcUtils.getReq match {
            case null => "WEB DAO WITHOUT REQUEST"
            case any => WebUtils.getURIIncludeQuery(any)
        }

        log.infof("%s\n%s", context, stmt.toString)
    }
}
