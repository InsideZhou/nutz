package org.nutz.web.mvc.view

import org.nutz.ioc.Ioc
import org.nutz.web.mvc.{View, ViewMaker}

class RythmViewMaker extends ViewMaker {
    def make(ioc: Ioc, viewType: String, path: String): View = {
        if ("rythm".equalsIgnoreCase(viewType)) {
            new RythmView(path)
        }
        else {
            null
        }
    }
}