package org.nutz.web.mvc.view

import org.nutz.ioc.Ioc
import org.nutz.web.mvc.{View, ViewMaker}

class FreemarkerViewMaker extends ViewMaker {
    def make(ioc: Ioc, viewType: String, path: String): View = {
        if ("ftl".equalsIgnoreCase(viewType)) {
            new FreemarkerView(path)
        }
        else {
            null
        }
    }
}