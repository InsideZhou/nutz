package org.nutz.web.mvc.adaptor

import java.io.{File, InputStream, Reader}
import java.lang.reflect.{ParameterizedType, Type}
import javax.servlet.ServletContext
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import org.nutz.json.Json
import org.nutz.lang.{Lang, Maths, Mirror, Strings}
import org.nutz.web.WebUtils
import org.nutz.web.mvc.injector.{ArrayInjector, MapPairInjector, ObjectNavlPairInjector}
import org.nutz.web.mvc.annotation.Param
import org.nutz.web.mvc.injector.{NutObjPairInjector, NutNameInjector}
import org.nutz.web.mvc.upload.injector._
import org.nutz.web.upload.{TempFile, FieldMeta}

import scala.collection.JavaConversions._

class NutAdaptor extends AbstractAdaptor {
    override def evalInjectorBy(t: Type, i: Int, param: Param): ParamInjector = {
        Lang.getTypeClass(t) match {
            case null => null
            case cls =>
                val paramName = if (null == param) {
                    this.paramName(this.method, i)
                }
                else {
                    assert(Strings.isBlank(param.dateFormat()), "不支持该参数选项，param.dateFormat。")
                    assert(param.defaultValue() == "//NOT EXIST IN//", "不支持该参数选项，param.defaultValue。")

                    if (Strings.isBlank(param.value())) {
                        ""
                    }
                    else if (param.value().startsWith("{}")) {
                        param.value().substring(2)
                    }
                    else {
                        param.value()
                    }
                }

                if (Strings.isBlank(paramName)) {
                    return null
                }

                if (classOf[File].isAssignableFrom(cls)) {
                    new FileInjector(paramName)
                }
                else if (classOf[FieldMeta].isAssignableFrom(cls)) {
                    new FileMetaInjector(paramName)
                }
                else if (classOf[TempFile].isAssignableFrom(cls)) {
                    new TempFileInjector(paramName)
                }
                else if (classOf[InputStream].isAssignableFrom(cls)) {
                    new InputStreamInjector(paramName)
                }
                else if (classOf[Reader].isAssignableFrom(cls)) {
                    new ReaderInjector(paramName)
                }
                else if (paramName.startsWith("..")) {
                    if (classOf[java.util.Map[_, _]].isAssignableFrom(cls)) {
                        new MapPairInjector(t)
                    }
                    else if (paramName.length < 3) {
                        new NutObjPairInjector(null, t)
                    }
                    else {
                        new NutObjPairInjector(paramName.substring(2), t)
                    }
                }
                else if (paramName.startsWith("::")) {
                    new ObjectNavlPairInjector(paramName.substring(2), t)
                }
                else if (cls.isArray) {
                    new ArrayInjector(paramName, null, t, t match {
                        case pt: ParameterizedType => pt.getActualTypeArguments
                        case _ => null
                    }, null)
                }
                else {
                    new NutNameInjector(paramName, cls, t match {
                        case pt: ParameterizedType => pt.getActualTypeArguments
                        case _ => null
                    })
                }
        }
    }

    override def getReferObject(sc: ServletContext, req: HttpServletRequest, resp: HttpServletResponse, pathArgs: Array[String]): AnyRef = {
        if (WebUtils.isMultipart(req)) {
            mapAsJavaMap(WebUtils.getFormParameterMap(req, sep = null) ++ WebUtils.getQueryParameterMap(req, sep = null))
        }
        else if (Strings.sNull(req.getContentType).toLowerCase.contains("application/json")) {
            Json.fromJson(WebUtils.getBodyStr(req))
        }
        else {
            null
        }
    }

    override def adapt(sc: ServletContext, req: HttpServletRequest, resp: HttpServletResponse, pathArgs: Array[String]): Array[AnyRef] = {
        val argCount = argTypes.size
        val args = new Array[AnyRef](argCount)
        val existCustomErrorCtx = argCount > 0 && classOf[AdaptorErrorContext].isAssignableFrom(argTypes(argCount - 1))
        val errorCtx = if (existCustomErrorCtx) {
            Mirror.me(argTypes(argCount - 1)).born(argCount.asInstanceOf[AnyRef]).asInstanceOf[AdaptorErrorContext]
        }
        else {
            new AdaptorErrorContext(argCount)
        }

        val obj = try {
            getReferObject(sc, req, resp, pathArgs)
        }
        catch {
            case e: Throwable =>
                if (existCustomErrorCtx) {
                    errorCtx.setAdaptorError(e, this)
                    args.update(argCount, errorCtx)
                    return args
                }
                else {
                    throw Lang.wrapThrow(e)
                }
        }

        val len = Maths.min(argCount, if (null == pathArgs) 0 else pathArgs.length)
        for ((_, i) <- args.zipWithIndex) {
            val value = if (i < len) {
                // 路径参数
                if (null == pathArgs) null else pathArgs(i)
            }
            else {
                // 普通参数
                obj
            }

            try {
                args.update(i, injs(i).get(sc, req, resp, value))
            }
            catch {
                case e: Throwable => errorCtx.setError(i, e, method, value, injs(i))
            }

            if (argTypes(i).isPrimitive && null == args(i)) {
                args.update(i, Lang.getPrimitiveDefaultValue(argTypes(i)))
            }
        }

        errorCtx.getErrors.find(null != _) match {
            case None =>
            case Some(e) =>
                if (existCustomErrorCtx) {
                    args.update(argCount - 1, errorCtx)
                }
                else {
                    throw Lang.wrapThrow(e)
                }
        }

        args
    }
}
