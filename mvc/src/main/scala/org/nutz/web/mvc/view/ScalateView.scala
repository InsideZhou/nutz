package org.nutz.web.mvc.view

import java.io.File
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import org.fusesource.scalate.TemplateEngine
import org.nutz.web.freemarker.AbstractScalateView
import org.nutz.web.mvc.{MvcUtils, View}

import scala.beans.BeanProperty

class ScalateView(templatePath: String) extends AbstractScalateView with View {
    engine = ScalateView.engine

    override def getTemplatePath: String = templatePath

    override def render(req: HttpServletRequest, resp: HttpServletResponse, obj: scala.Any): Unit = {
        doRender(req, resp, obj)
    }
}

object ScalateView {
    @BeanProperty
    var engine = TemplateEngine(List(new File(MvcUtils.getServletContext.getRealPath("/"))), "production")
}
