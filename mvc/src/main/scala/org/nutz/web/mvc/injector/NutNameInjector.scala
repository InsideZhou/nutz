package org.nutz.web.mvc.injector

import java.lang.reflect.Type
import javax.servlet.ServletContext
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import org.nutz.castor.Castors
import org.nutz.lang.{Lang, Validate}
import org.nutz.web.mvc.adaptor.ParamInjector

import scala.collection.JavaConversions._

class NutNameInjector(
                            protected val name: String,
                            protected val paramType: Class[_],
                            protected val paramTypes: Array[Type]) extends ParamInjector {

    Validate.notNull(name, "Can not accept null as name, type '%s'", paramType.getName)

    def get(sc: ServletContext, req: HttpServletRequest, resp: HttpServletResponse, refer: Any) = {
        refer match {
            case null => fromReqParam(req).asInstanceOf[AnyRef]
            case map: scala.collection.Map[_, _] => fromReferMap(map.toMap, req)
            case map: java.util.Map[_, _] => fromReferMap(map.toMap, req)
            case _ => Castors.me().castTo(refer, paramType).asInstanceOf[AnyRef]
        }
    }

    private def fromReferMap(map: Map[_, _], req: HttpServletRequest) = {
        map.get(name) match {
            case null => fromReqParam(req).asInstanceOf[AnyRef]

            case c: java.util.Collection[_] if null != paramTypes && paramTypes.nonEmpty =>
                try {
                    val nw = c.getClass.newInstance().asInstanceOf[java.util.Collection[Any]]
                    val eleType = Lang.getTypeClass(paramTypes(0))
                    c.foreach(ele => {
                        nw.add(Castors.me().castTo(ele, eleType))
                    })
                    nw
                }
                catch {
                    case e: Throwable => throw Lang.wrapThrow(e)
                }

            case any => Castors.me().castTo(any, paramType).asInstanceOf[AnyRef]
        }
    }


    private def fromReqParam(req: HttpServletRequest) = {
        val params = req.getParameterValues(name)
        // 默认用转换器转换
        if (null == params) {
            null
        }
        else if (params.length == 1) {
            Castors.me().castTo(params(0), paramType)
        }
        else {
            Castors.me().castTo(params, paramType)
        }
    }
}
