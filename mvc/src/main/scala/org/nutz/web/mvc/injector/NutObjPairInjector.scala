package org.nutz.web.mvc.injector

import java.lang.reflect.Type
import javax.servlet.ServletContext
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}



import scala.collection.JavaConversions._

class NutObjPairInjector(prefix: String, t: Type) extends ObjectPairInjector(prefix, t) {
    override def get(sc: ServletContext, req: HttpServletRequest, resp: HttpServletResponse, refer: Any): AnyRef = {
        val obj = mirror.born()
        val paraMap = refer match {
            case m: java.util.Map[_, _] => req.getParameterMap.asInstanceOf[java.util.Map[String, Any]].toMap ++ m
            case _ => req.getParameterMap.asInstanceOf[java.util.Map[String, Any]].toMap
        }

        for ((inject, index) <- injs.zipWithIndex) {
            val name = names(index)
            val param = paraMap.getOrElse(name, null)

            if (paraMap.containsKey(name)) {
                if (null != param) {
                    inject.inject(obj, param)
                }
            }
        }

        obj.asInstanceOf[AnyRef]
    }
}
