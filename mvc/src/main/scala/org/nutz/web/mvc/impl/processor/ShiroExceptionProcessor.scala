package org.nutz.web.mvc.impl.processor

import java.net.HttpURLConnection

import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authz.AuthorizationException
import org.nutz.web.WebUtils
import org.nutz.web.mvc.view.{HttpStatusView, ServerRedirectView}
import org.nutz.web.mvc.{ActionContext, View}

class ShiroExceptionProcessor extends ExceptionProcessor {
    var LoginURLInContext = "ShiroExceptionProcessor.LoginURLInContext"

    override def process(ac: ActionContext): Unit = {
        ac.getError match {
            case e: AuthenticationException => processException(ac)
            case e: AuthorizationException => processException(ac)
            case _ => super.process(ac)
        }
    }

    def processException(ac: ActionContext) = {
        ac.getError match {
            case e: AuthenticationException =>
                val req = ac.getRequest
                val headerMap = WebUtils.getHeaderMap(req)

                log.infof(
                    """%s
                      |%s %s
                      |Header(
                      |%s
                      |)
                      |Msg(
                      |AuthenticationException
                      |%s
                      |)""".stripMargin,
                    WebUtils.getRealIP(req),
                    req.getMethod, WebUtils.getURIIncludeQuery(req),
                    headerMap.mkString("\n"),
                    e.getMessage
                )

                val view = req.getAttribute(LoginURLInContext) match {
                    case url: String => getAuthenticationView(ac, url)
                    case _ => getAuthenticationFailView(ac)
                }

                view.render(ac.getRequest, ac.getResponse, null)

            case e: AuthorizationException =>
                val req = ac.getRequest
                val headerMap = WebUtils.getHeaderMap(req)

                log.infof(
                    """%s
                      |%s %s
                      |Header(
                      |%s
                      |)
                      |Msg(
                      |AuthorizationException
                      |%s
                      |)""".stripMargin,
                    WebUtils.getRealIP(req),
                    req.getMethod, WebUtils.getURIIncludeQuery(req),
                    headerMap.mkString("\n"),
                    e.getMessage
                )

                getAuthorizationFailView(ac).render(ac.getRequest, ac.getResponse, null)

            case _ =>
        }
    }

    def getAuthenticationView(ac: ActionContext, authUrl: String): View = new ServerRedirectView(authUrl)

    def getAuthenticationFailView(ac: ActionContext): View = new HttpStatusView(HttpURLConnection.HTTP_FORBIDDEN)

    def getAuthorizationFailView(ac: ActionContext): View = new HttpStatusView(HttpURLConnection.HTTP_FORBIDDEN)
}
