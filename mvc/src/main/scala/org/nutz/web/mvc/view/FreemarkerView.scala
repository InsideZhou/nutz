package org.nutz.web.mvc.view

import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import freemarker.template.Configuration
import org.nutz.web.freemarker.{AbstractFreemarkerView, FreemarkerConfiguration}
import org.nutz.web.mvc.{MvcUtils, View}

import scala.beans.BeanProperty

class FreemarkerView(templatePath: String) extends AbstractFreemarkerView with View {
    config = FreemarkerView.config
    contentType = "text/html"

    override def getTemplatePath: String = templatePath

    override def render(req: HttpServletRequest, resp: HttpServletResponse, obj: scala.Any): Unit = {
        this.doRender(req, resp, obj)
    }
}

object FreemarkerView {
    @BeanProperty
    var config: Configuration = null
    if (null == config) {
        config = FreemarkerConfiguration.getConfig
        config.setServletContextForTemplateLoading(MvcUtils.getServletContext, "/")
    }
}
