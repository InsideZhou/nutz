package org.nutz.web.mvc.impl.processor

import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authz.UnauthenticatedException
import org.apache.shiro.authz.annotation.{RequiresAuthentication, RequiresGuest, RequiresRoles, RequiresUser}
import org.apache.shiro.web.env.{EnvironmentLoader, WebEnvironment}
import org.nutz.shiro.{AnnotationMethodInterceptor, NeedsPermissions}
import org.nutz.web.mvc.{MethodInvocationImpl, ActionContext}

class ShiroProcessor extends AbstractProcessor {
    override def process(ac: ActionContext): Unit = {
        val ctx = ac.getServletContext
        ctx.getAttribute(EnvironmentLoader.ENVIRONMENT_ATTRIBUTE_KEY) match {
            case attr: WebEnvironment =>
                if ((ac.getMethod.getDeclaredAnnotations ++: ac.getModule.getClass.getAnnotations).exists {
                    case a: NeedsPermissions => true
                    case a: RequiresAuthentication => true
                    case a: RequiresUser => true
                    case a: RequiresRoles => true
                    case a: RequiresGuest => true
                    case _ => false
                }) {
                    try {
                        AnnotationMethodInterceptor.assertAuthorized(new MethodInvocationImpl(ac))
                    }
                    catch {
                        case e: UnauthenticatedException => throw new AuthenticationException(e)
                        case e: Throwable => throw e
                    }
                }
            case _ =>
        }

        doNext(ac)
    }
}
