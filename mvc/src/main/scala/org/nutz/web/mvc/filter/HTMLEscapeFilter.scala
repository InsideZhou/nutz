package org.nutz.web.mvc.filter

import org.nutz.log.Logs
import org.nutz.web.WebUtils
import org.nutz.web.mvc.{ActionContext, ActionFilter, View}
import org.nutz.web.servlet.ReenterableHttpServletRequest

class HTMLEscapeFilter extends ActionFilter {
    private val log = Logs.getLog(classOf[HTMLEscapeFilter])

    override def `match`(ac: ActionContext): View = {
        val req = ac.getRequest
        if (WebUtils.isMultipart(req)) {
            log.debug("HTMLEscapeFilter已命中，但当前请求是multipart表单，所以未做任何escape处理。")
        }
        else {
            req match {
                case request: ReenterableHttpServletRequest => request.escapeParameterValues()
                case _ => throw new AssertionError("使用html escape时，必须启用org.nutz.servlet.RequestWrapperFilter。")
            }
        }

        null
    }
}
