package org.nutz.web.mvc.impl.processor

import org.nutz.log.Logs
import org.nutz.web.WebUtils
import org.nutz.web.mvc.ActionContext

class HitProcessor extends AbstractProcessor {
    protected val log = Logs.getLog(classOf[HitProcessor])

    override def process(ac: ActionContext): Unit = {
        val req = ac.getRequest

        if ("POST".equalsIgnoreCase(req.getMethod) && !WebUtils.isMultipart(req)) {
            log.infof(
                """%s %s
                  |Header(
                  |%s
                  |)
                  |Param(
                  |%s
                  |)""".stripMargin, req.getMethod, WebUtils.getURIIncludeQuery(req)
                , WebUtils.getHeaderMap(req).mkString("\n"), WebUtils.getBodyRawStr(req)
            )
        }
        else {
            log.infof(
                """%s %s
                  |Header(
                  |%s
                  |)""".stripMargin, req.getMethod, WebUtils.getURIIncludeQuery(req), WebUtils.getHeaderMap(req).mkString("\n")
            )
        }

        this.doNext(ac)
    }
}
