package org.nutz.web.mvc.filter

import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authz.{AuthorizationException, UnauthenticatedException}
import org.nutz.shiro.AnnotationMethodInterceptor
import org.nutz.web.mvc.{MethodInvocationImpl, ActionContext, ActionFilter, View}

class ShiroAuthFilter extends ActionFilter {
    override def `match`(ac: ActionContext): View = {
        try {
            AnnotationMethodInterceptor.assertAuthorized(new MethodInvocationImpl(ac))
            null
        }
        catch {
            case e: AuthenticationException => this.onUnauthenticated(ac, e)
            case e: UnauthenticatedException => this.onUnauthenticated(ac, new AuthenticationException(e))
            case e: AuthorizationException => this.onUnauthorized(ac, e)
            case e: Throwable => throw e
        }
    }

    def onUnauthenticated(ac: ActionContext, e: AuthenticationException): View = {
        throw e
    }

    def onUnauthorized(ac: ActionContext, e: AuthorizationException): View = {
        throw e
    }
}
