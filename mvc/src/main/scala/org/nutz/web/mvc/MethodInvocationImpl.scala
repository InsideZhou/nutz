package org.nutz.web.mvc

import java.lang.reflect.Method

import org.apache.shiro.aop.MethodInvocation

class MethodInvocationImpl(ac: ActionContext) extends MethodInvocation {
    override def proceed(): AnyRef = ???

    override def getMethod: Method = {
        ac.getMethod
    }

    override def getThis: AnyRef = {
        ac.getModule
    }

    override def getArguments: Array[AnyRef] = {
        ac.getMethodArgs
    }
}
