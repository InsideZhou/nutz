package org.nutz.web.mvc.view

import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import org.nutz.web.freemarker.AbstractRythmView
import org.nutz.web.mvc.{MvcUtils, View}
import org.rythmengine.RythmEngine

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

class RythmView(templatePath: String) extends AbstractRythmView with View {
    engine = RythmView.engine

    override def getTemplatePath: String = templatePath

    override def render(req: HttpServletRequest, resp: HttpServletResponse, obj: scala.Any): Unit = {
        doRender(req, resp, obj)
    }
}

object RythmView {
    @BeanProperty
    var engine = new RythmEngine(Map(
        "resource.name.suffix" -> ".rythm",
        "feature.natural_template" -> "true",
        "home.template.dir" -> MvcUtils.getServletContext.getRealPath("/")
    ))
}
