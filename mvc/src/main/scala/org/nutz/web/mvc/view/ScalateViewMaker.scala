package org.nutz.web.mvc.view

import org.nutz.ioc.Ioc
import org.nutz.web.mvc.{View, ViewMaker}

class ScalateViewMaker extends ViewMaker {
    def make(ioc: Ioc, viewType: String, path: String): View = {
        viewType.toLowerCase match {
            case "ssp" => new ScalateView(path)
            case "scaml" => new ScalateView(path)
            case "jade" => new ScalateView(path)
            case "mustache" => new ScalateView(path)
            case _ => null
        }
    }
}