package org.nutz.web.mvc.impl.processor

import org.nutz.web.mvc.adaptor.NutAdaptor
import org.nutz.web.mvc.{ActionContext, ActionInfo, HttpAdaptor, NutConfig}

import scala.collection.JavaConversions._

class NutAdaptorProcessor extends AbstractProcessor {
    var adaptor: HttpAdaptor = null

    override def init(config: NutConfig, ai: ActionInfo): Unit = {
        val re = AbstractProcessor.evalObj(config, ai.getAdaptorInfo) match {
            case null => new NutAdaptor
            case obj => obj
        }

        re.init(ai.getMethod)
        adaptor = re
    }

    override def process(ac: ActionContext): Unit = {
        val args = adaptor.adapt(ac.getServletContext, ac.getRequest, ac.getResponse, ac.getPathArgs.toBuffer.toArray)
        ac.setMethodArgs(args)
        doNext(ac)
    }
}
