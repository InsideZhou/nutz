package org.nutz.web.mvc.impl.processor

import org.nutz.log.Logs
import org.nutz.web.{WebUtils, WebSettings}
import org.nutz.web.mvc.view.{HttpStatusView, RawView, VoidView}
import org.nutz.web.mvc.{ActionContext, ActionInfo, NutConfig}

class ExceptionProcessor extends ViewProcessor {
    protected val log = Logs.getLog(classOf[ExceptionProcessor])

    override def init(config: NutConfig, ai: ActionInfo): Unit = {
        ViewProcessor.evalView(config, ai, ai.getFailView) match {
            case v: VoidView =>
                if (WebSettings.debugging) {
                    this.view = new RawView("text/plain")
                }
                else {
                    this.view = new HttpStatusView(500)
                }
            case v => this.view = v
        }
    }

    override def process(ac: ActionContext): Unit = {
        val req = ac.getRequest
        val resp = ac.getResponse
        val error = ac.getError

        val errorTxt = s"""${WebUtils.getRealIP(req)}
              |${req.getMethod} ${WebUtils.getURIIncludeQuery(req)}
              |Header(
              |${WebUtils.getHeaderMap(req).mkString("\n")}
              |)
              |Params(
              |${WebUtils.getBodyRawStr(req)}
              |)
              |Msg(
              |${error.getMessage}
              |)
              |Cause(
              |${error.getCause}
              |)
              |StackTrace(
              |${error.getClass}
              |${error.getStackTrace.mkString("\n")}
              |)""".stripMargin

        log.error(errorTxt)

        if (!resp.isCommitted) {
            resp.reset()
            this.view.render(req, resp, errorTxt)
        }
    }
}
