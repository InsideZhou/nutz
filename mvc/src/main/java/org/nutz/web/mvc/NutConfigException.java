package org.nutz.web.mvc;

@SuppressWarnings("serial")
public class NutConfigException extends RuntimeException {

    public NutConfigException() {
        super();
    }

    public NutConfigException(String message, Throwable cause) {
        super(message, cause);
    }

    public NutConfigException(String message) {
        super(message);
    }

    public NutConfigException(Throwable cause) {
        super(cause);
    }

}
