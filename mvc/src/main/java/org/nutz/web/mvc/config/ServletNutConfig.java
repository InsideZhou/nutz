package org.nutz.web.mvc.config;

import org.nutz.web.mvc.MvcUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.util.List;

public class ServletNutConfig extends AbstractNutConfig {

    private ServletConfig config;

    public ServletNutConfig(ServletConfig config) {
        super(config.getServletContext());
        this.config = config;
        MvcUtils.setAtMap(new AtMap());
    }

    public ServletContext getServletContext() {
        return config.getServletContext();
    }

    public String getInitParameter(String name) {
        return config.getInitParameter(name);
    }

    public List<String> getInitParameterNames() {
        return enum2list(config.getInitParameterNames());
    }

    public String getAppName() {
        return config.getServletName();
    }

}
