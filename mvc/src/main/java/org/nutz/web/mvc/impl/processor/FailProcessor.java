package org.nutz.web.mvc.impl.processor;

import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.web.mvc.ActionContext;
import org.nutz.web.mvc.ActionInfo;
import org.nutz.web.mvc.MvcUtils;
import org.nutz.web.mvc.NutConfig;

/**
 * 
 * @author zozoh(zozohtnt@gmail.com)
 * @author wendal(wendal1985@gmail.com)
 * 
 */
public class FailProcessor extends ViewProcessor {

    private static final Log log = Logs.getLog(FailProcessor.class);

    @Override
    public void init(NutConfig config, ActionInfo ai) throws Throwable {
        view = evalView(config, ai, ai.getFailView());
    }

    public void process(ActionContext ac) throws Throwable {
        if (log.isWarnEnabled()) {
            String uri = MvcUtils.getRequestPath(ac.getRequest());
            log.warn(String.format("Error@%s :", uri), ac.getError());
        }
        super.process(ac);
    }
}
