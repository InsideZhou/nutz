package org.nutz.web.mvc.upload.util;

public enum MarkMode {

    STREAM_END, NOT_FOUND, FOUND

}
