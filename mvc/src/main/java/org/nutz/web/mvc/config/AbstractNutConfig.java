package org.nutz.web.mvc.config;

import org.nutz.castor.Castors;
import org.nutz.ioc.Ioc;
import org.nutz.json.Json;
import org.nutz.lang.Lang;
import org.nutz.lang.util.Context;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.web.mvc.*;
import org.nutz.web.mvc.annotation.LoadingBy;
import org.nutz.web.mvc.impl.NutLoading;
import org.nutz.resource.Scans;
import org.nutz.resource.impl.ResourceLocation;
import org.nutz.lang.Mirror;
import org.nutz.lang.Strings;

import javax.servlet.ServletContext;
import java.io.File;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public abstract class AbstractNutConfig implements NutConfig {

    private static final Log log = Logs.getLog(AbstractNutConfig.class);

    private SessionProvider sessionProvider;

    public AbstractNutConfig(ServletContext context) {
        initResourceScanner(context);
        Json.clearEntityCache();
    }

    @SuppressWarnings("unchecked")
    private void initResourceScanner(ServletContext sc) {
        Set<ResourceLocation> locations = Scans.me().getLocations();

        // 获取classes文件夹的路径
        String classesPath = sc.getRealPath("/WEB-INF/classes/");
        if (classesPath != null) {
            locations.add(ResourceLocation.file(new File(classesPath)));
        }
        else {
            log.warn("/WEB-INF/classes/ NOT found?!");
        }

        // 获取lib文件夹中的全部jar
        Set<String> jars = sc.getResourcePaths("/WEB-INF/lib/");
        if (jars != null) // 这个文件夹不一定存在,尤其是Maven的WebApp项目
            for (String path : jars) {
                if (!path.toLowerCase().endsWith(".jar"))
                    continue;
                locations.add(ResourceLocation.jar(sc.getRealPath(path)));
            }
        else {
            log.warn("/WEB-INF/lib/ NOT found?!");
        }
        log.debug("Locations for Scans:\n" + locations);
    }

    public Loading createLoading() {
        /*
         * 确保用户声明了 MainModule
         */
        Class<?> mainModule = getMainModule();
        
        /*
         * 获取 Loading
         */
        LoadingBy by = mainModule.getAnnotation(LoadingBy.class);
        if (null == by) {
            log.debug("Loading by " + NutLoading.class);
            return new NutLoading();
        }
        try {
            log.debug("Loading by " + by.value());
            return Mirror.me(by.value()).born();
        }
        catch (Exception e) {
            throw Lang.wrapThrow(e);
        }
    }

    public Context getLoadingContext() {
        return (Context) this.getServletContext().getAttribute(Loading.CONTEXT_NAME);
    }

    public String getAppRoot() {
        String webinf = getServletContext().getRealPath("/WEB-INF/");
        if (webinf == null) {
            log.info("/WEB-INF/ not Found?!");
            return "";
        }
        String root = getServletContext().getRealPath("/").replace('\\', '/');
        if (root.endsWith("/"))
            return root.substring(0, root.length() - 1);
        else if (root.endsWith("/."))
            return root.substring(0, root.length() - 2);
        return root;
    }

    public Ioc getIoc() {
        return MvcUtils.getIoc();
    }

    public Object getAttribute(String name) {
        return this.getServletContext().getAttribute(name);
    }

    public List<String> getAttributeNames() {
        return enum2list(this.getServletContext().getAttributeNames());
    }

    @SuppressWarnings("unchecked")
    public <T> T getAttributeAs(Class<T> type, String name) {
        Object obj = getAttribute(name);
        if (null == obj)
            return null;
        if (type.isInstance(obj))
            return (T) obj;
        return Castors.me().castTo(obj, type);
    }

    public void setAttribute(String name, Object obj) {
        this.getServletContext().setAttribute(name, obj);
    }

    public void setAttributeIgnoreNull(String name, Object obj) {
        if (null != obj)
            setAttribute(name, obj);
    }

    public Class<?> getMainModule() {
        String name = Strings.trim(getInitParameter("modules"));
        try {
            Class<?> mainModule = null;
            if (Strings.isBlank(name))
                throw new NutConfigException("You need declare 'modules' parameter in your context configuration file or web.xml ! Only found -> " + getInitParameterNames());
            mainModule = Lang.loadClass(name);
            log.debugf("MainModule: <%s>", mainModule.getName());
            return mainModule;
        }
        catch (NutConfigException e) {
            throw e;
        }
        catch (Exception e) {
            throw new NutConfigException(e);
        }
    }

    public AtMap getAtMap() {
        return MvcUtils.getAtMap();
    }

    protected List<String> enum2list(Enumeration<?> enums) {
        LinkedList<String> re = new LinkedList<String>();
        while (enums.hasMoreElements())
            re.add(enums.nextElement().toString());
        return re;
    }

    public void setSessionProvider(SessionProvider provider) {
        this.sessionProvider = provider;
    }

    public SessionProvider getSessionProvider() {
        return sessionProvider;
    }
}
