package org.nutz.web.mvc.upload.injector;

import org.nutz.web.mvc.adaptor.ParamInjector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MapSelfInjector implements ParamInjector {

    public Object get(ServletContext sc, HttpServletRequest req, HttpServletResponse resp, Object refer) {
        return refer;
    }

}
