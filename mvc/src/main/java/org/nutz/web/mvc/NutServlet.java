package org.nutz.web.mvc;

import org.nutz.lang.util.Context;
import org.nutz.web.mvc.config.ServletNutConfig;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 挂接到 JSP/Servlet 容器的入口
 * 
 * @author zozoh(zozohtnt@gmail.com)
 * @author wendal(wendal1985@gmail.com)
 * @author juqkai(juqkai@gmail.com)
 */
@SuppressWarnings("serial")
public class NutServlet extends HttpServlet {

    protected ActionHandler handler;
    
    private String selfName;
    
    private SessionProvider sp;
    
    protected ServletContext sc;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
    	sc = servletConfig.getServletContext();
        MvcUtils.setServletContext(sc);
        selfName = servletConfig.getServletName();
        MvcUtils.set(selfName, null, null);
        NutConfig config = new ServletNutConfig(servletConfig);
        MvcUtils.setNutConfig(config);
        handler = new ActionHandler(config);
        sp = config.getSessionProvider();
    }

    public void destroy() {
        MvcUtils.resetALL();
        MvcUtils.set(selfName, null, null);
        if(handler != null)
            handler.depose();
        MvcUtils.setServletContext(null);
        MvcUtils.close();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
    	ServletContext prCtx = MvcUtils.getServletContext();
        MvcUtils.setServletContext(sc);
        String preName = MvcUtils.getName();
        Context preContext = MvcUtils.resetALL();
        try {
            if (sp != null)
                req = sp.filter(req, resp, getServletContext());
            MvcUtils.set(selfName, req, resp);
            if (!handler.handle(req, resp))
                resp.sendError(404);
        } finally {
            MvcUtils.resetALL();
            //仅当forward/incule时,才需要恢复之前设置
            if (null != (req.getAttribute("javax.servlet.forward.request_uri"))) {
            	if (prCtx != sc)
            		MvcUtils.setServletContext(prCtx);
                if (preName != null)
                    MvcUtils.set(preName, req, resp);
                if (preContext != null)
                    MvcUtils.ctx().reqThreadLocal.set(preContext);
            }
        }
    }
}
