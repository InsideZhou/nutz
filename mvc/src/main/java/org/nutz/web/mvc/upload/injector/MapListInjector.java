package org.nutz.web.mvc.upload.injector;

import org.nutz.web.mvc.adaptor.ParamInjector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MapListInjector implements ParamInjector {
    
    public MapListInjector(String name) {
        this.name = name;
    }

    private String name;

    public Object get(ServletContext sc, HttpServletRequest req, HttpServletResponse resp, Object refer) {
        Object obj = ((Map<?,?>) refer).get(name);
        if (obj == null)
            return null;
        
        if(obj instanceof List)
            return obj;
        
        List<Object> re = new ArrayList<Object>(1);
        re.add(obj);
        return re;
    }

}
