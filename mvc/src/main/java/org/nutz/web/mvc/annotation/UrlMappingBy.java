package org.nutz.web.mvc.annotation;

import org.nutz.web.mvc.UrlMapping;
import org.nutz.web.mvc.impl.UrlMappingImpl;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface UrlMappingBy {

    Class<? extends UrlMapping> value() default UrlMappingImpl.class;
    
    String[] args() default {};
    
}
