package org.nutz.web.mvc.impl.processor;

import org.nutz.web.mvc.ActionContext;
import org.nutz.web.mvc.MvcUtils;

/**
 * 更新 Request 中的属性，增加诸如 '${base}', '${msg}' 等属性，以便 JSP 网页访问
 * 
 * @author zozoh(zozohtnt@gmail.com)
 */
public class UpdateRequestAttributesProcessor extends AbstractProcessor{

    public void process(ActionContext ac) throws Throwable {
        MvcUtils.updateRequestAttributes(ac.getRequest());
        doNext(ac);
    }

}
