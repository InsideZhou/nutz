package org.nutz.web.mvc.injector;

import org.nutz.castor.Castors;
import org.nutz.lang.Lang;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Type;

public class AppAttrInjector extends AttrInjector {
    private Type type;

    public AppAttrInjector(String name, Type type) {
        super(name);
        this.type = type;
    }

    public Object get(ServletContext sc, HttpServletRequest req, HttpServletResponse resp, Object refer) {
        return Castors.me().castTo(sc.getAttribute(name), Lang.getTypeClass(type));
    }
}
