package org.nutz.web.mvc.upload;

import org.nutz.web.upload.FieldMeta;

@SuppressWarnings("serial")
public class UploadOutOfSizeException extends RuntimeException {

    public UploadOutOfSizeException(FieldMeta meta) {
        super(String.format("File '%s' out of size!", meta.getFileLocalPath()));
    }

}
