package org.nutz.web.mvc.injector;

import org.nutz.castor.Castors;
import org.nutz.lang.Lang;
import org.nutz.web.mvc.MvcUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Type;

public class SessionAttrInjector extends AttrInjector {
    private Type type;

    public SessionAttrInjector(String name, Type type) {
        super(name);
        this.type = type;
    }

    public Object get(ServletContext sc, HttpServletRequest req, HttpServletResponse resp, Object refer) {
        HttpSession session = MvcUtils.getHttpSession(false);
        if (session == null)
            return null;
        return Castors.me().castTo(session.getAttribute(name), Lang.getTypeClass(type));
    }
}
