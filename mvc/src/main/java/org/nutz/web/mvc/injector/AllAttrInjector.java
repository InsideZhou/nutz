package org.nutz.web.mvc.injector;

import org.nutz.castor.Castors;
import org.nutz.lang.Lang;
import org.nutz.web.mvc.MvcUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Type;

public class AllAttrInjector extends AttrInjector {
    private Type type;

    public AllAttrInjector(String name, Type type) {
        super(name);
        this.type = type;
    }

    public Object get(ServletContext sc, HttpServletRequest req, HttpServletResponse resp, Object refer) {
        Object re = req.getAttribute(name);

        if (null == re) {
            HttpSession session = MvcUtils.getHttpSession(false);
            if (session != null) {
                re = session.getAttribute(name);
            }
        }

        if (null == re) {
            re = sc.getAttribute(name);
        }

        return Castors.me().castTo(re, Lang.getTypeClass(type));
    }
}
