package org.nutz.web.mvc.view;

import org.nutz.web.mvc.View;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class VoidView implements View {

    public void render(HttpServletRequest req, HttpServletResponse resp, Object obj)
            throws Throwable {}

}
