package org.nutz.web.mvc.impl;

import org.nutz.lang.Lang;
import org.nutz.lang.Mirror;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.web.mvc.*;
import org.nutz.web.mvc.impl.chainconfig.ActionChainMakerConfiguration;
import org.nutz.web.mvc.impl.chainconfig.JsonActionChainMakerConfiguration;

import java.util.ArrayList;
import java.util.List;

public class NutActionChainMaker implements ActionChainMaker {

    private static final Log logger = Logs.getLog(NutActionChainMaker.class);

    ActionChainMakerConfiguration co;

    public NutActionChainMaker(String... args) {
        co = new JsonActionChainMakerConfiguration(args);
    }

    public ActionChain eval(NutConfig config, ActionInfo ai) {

        try {
            List<Processor> list = new ArrayList<Processor>();
            for (String name : co.getProcessors(ai.getChainName())) {
                Processor processor = getProcessorByName(config, name);
                processor.init(config, ai);
                list.add(processor);
            }

            Processor errorProcessor = getProcessorByName(config, co.getErrorProcessor(ai.getChainName()));
            errorProcessor.init(config, ai);
            /*
             * 返回动作链实例
             */
            return new NutActionChain(list, errorProcessor, ai.getMethod());
        }
        catch (Throwable e) {
            logger.debugf("Eval FAIL!! : %s", ai.getMethod());
            throw Lang.wrapThrow(e);
        }
    }

    protected static Processor getProcessorByName(NutConfig config, String name) throws Exception {
        if (name.startsWith("ioc:") && name.length() > 4)
            return config.getIoc().get(Processor.class, name.substring(4));
        else
            return (Processor) Mirror.me(Lang.loadClass(name)).born();
    }
}
