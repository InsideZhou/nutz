package org.nutz.web.mvc.upload;

import org.nutz.web.upload.FieldMeta;

@SuppressWarnings("serial")
public class UploadUnsupportedFileTypeException extends RuntimeException {

    public UploadUnsupportedFileTypeException(FieldMeta meta) {
        super(String.format("Unsupport file '%s' [%s] ",
                            meta.getFileLocalPath(),
                            meta.getContentType()));
    }

}
