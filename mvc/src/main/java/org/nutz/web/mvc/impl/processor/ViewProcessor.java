package org.nutz.web.mvc.impl.processor;

import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.nutz.lang.util.Context;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.web.mvc.*;
import org.nutz.web.mvc.view.ViewWrapper;
import org.nutz.web.mvc.view.VoidView;

import javax.servlet.http.HttpServletRequest;

public class ViewProcessor extends AbstractProcessor {

    protected View view;
    public static final String DEFAULT_ATTRIBUTE = "obj";
    private static final Log log = Logs.getLog(ViewProcessor.class);
    
    @Override
    public void init(NutConfig config, ActionInfo ai) throws Throwable {
        //需要特别提醒一下使用jsonView,但方法的返回值是String的!!
        if("json".equals(ai.getOkView()) && String.class.equals(ai.getMethod().getReturnType())) {
            log.warn("Not a good idea : Return String ,and using JsonView!! (Using @Ok(\"raw\") or return map/list/pojo)--> " + Lang.simpleMetodDesc(ai.getMethod()));
        }
        view = evalView(config, ai, ai.getOkView());
    }

    public void process(ActionContext ac) throws Throwable {
        Object re = ac.getMethodReturn();
        Object err = ac.getError();
        if (re != null && re instanceof View) {
            if (re instanceof ViewWrapper)
                putRequestAttribute(ac.getRequest(), ((ViewWrapper)re).getData());
            ((View) re).render(ac.getRequest(), ac.getResponse(), err);
        } else {
            putRequestAttribute(ac.getRequest(), null == re ? err : re);
            view.render(ac.getRequest(), ac.getResponse(), null == re ? err : re);
        }
        doNext(ac);
    }
    
    /**
     * 保存对象到attribute
     */
    public static void putRequestAttribute(HttpServletRequest req, Object re){
        if (null != re){
            if(re instanceof Context){
                Context context = (Context) re;
                for(String key : context.keys()){
                    req.setAttribute(key, context.get(key));
                }
            } else {
                req.setAttribute(ViewProcessor.DEFAULT_ATTRIBUTE, re);
            }
        }
    }

    public static View evalView(NutConfig config, ActionInfo ai, String viewType) {
        if (Strings.isBlank(viewType))
            return new VoidView();

        int pos = viewType.indexOf(':');
        String type, value;
        if (pos > 0) {
            type = Strings.trim(viewType.substring(0, pos).toLowerCase());
            value = Strings.trim(pos >= (viewType.length() - 1) ? null : viewType.substring(pos + 1));
        } else {
            type = viewType;
            value = null;
        }
        
        for (ViewMaker maker : ai.getViewMakers()) {
        	if (maker instanceof ViewMaker2) {
        		View view = ((ViewMaker2)maker).make(config, ai, type, value);
        		if (view != null)
        			return view;
        	}
        	View view = maker.make(config.getIoc(), type, value);
            if (null != view)
                return view;
        }
        throw Lang.makeThrow("Can not eval %s(\"%s\") View for %s", viewType, viewType, ai.getMethod());
    }
}
