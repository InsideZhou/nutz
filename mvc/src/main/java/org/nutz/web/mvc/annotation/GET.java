package org.nutz.web.mvc.annotation;

import java.lang.annotation.*;

/**
 * 描述一个入口函数，是不是仅仅响应 GET 请求
 * 
 * @author zozoh(zozohtnt@gmail.com)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface GET {}
