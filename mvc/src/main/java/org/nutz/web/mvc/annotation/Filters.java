package org.nutz.web.mvc.annotation;

import java.lang.annotation.*;

/**
 * 声明一组过滤器
 * 
 * @author zozoh(zozohtnt@gmail.com)
 * 
 * @see By
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
public @interface Filters {

    By[] value() default {};

}
