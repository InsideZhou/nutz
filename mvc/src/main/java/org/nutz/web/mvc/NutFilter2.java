package org.nutz.web.mvc;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 为了兼容老的NutFilter,把逻辑独立出来, 仅用于过滤Jsp请求之类的老特性
 *
 */
public class NutFilter2 implements Filter {
	
	private String selfName;
	
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		if (selfName == null) {
			selfName = MvcUtils.ctx().nutConfigs.keySet().iterator().next();
			if (selfName == null) {
				chain.doFilter(req, resp);
				return;
			}
		}
		boolean needReset = false;
		if (MvcUtils.getName() == null) {
			HttpServletRequest req2 = (HttpServletRequest)req;
			HttpServletResponse resp2 = (HttpServletResponse)resp;
			MvcUtils.set(selfName, req2, resp2);
			MvcUtils.updateRequestAttributes(req2);
			needReset = true;
		}
		try {
			chain.doFilter(req, resp);
		} finally {
			if (needReset)
				MvcUtils.resetALL();
		}
	}

	public void init(FilterConfig conf) throws ServletException {}

	public void destroy() {}

}
