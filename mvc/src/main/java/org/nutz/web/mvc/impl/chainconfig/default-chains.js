var chain = {
    "default": {
        "ps": [
            "org.nutz.web.mvc.impl.processor.UpdateRequestAttributesProcessor",
            "org.nutz.web.mvc.impl.processor.EncodingProcessor",
            "org.nutz.web.mvc.impl.processor.ModuleProcessor",
            "org.nutz.web.mvc.impl.processor.ActionFiltersProcessor",
            "org.nutz.web.mvc.impl.processor.NutAdaptorProcessor",
            "org.nutz.web.mvc.impl.processor.MethodInvokeProcessor",
            "org.nutz.web.mvc.impl.processor.ViewProcessor"
        ],
        "error": 'org.nutz.web.mvc.impl.processor.ExceptionProcessor'
    }
}