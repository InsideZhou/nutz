package org.nutz.web.mvc.upload.injector;

import org.nutz.web.mvc.adaptor.ParamInjector;
import org.nutz.web.upload.TempFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public class TempFileInjector implements ParamInjector {

    public TempFileInjector(String name) {
        this.name = name;
    }

    private String name;

    @SuppressWarnings("unchecked")
    public TempFile get(ServletContext sc, HttpServletRequest req, HttpServletResponse resp, Object refer) {
        return (TempFile) ((Map<String, Object>) refer).get(name);
    }

}
