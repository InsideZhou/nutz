package org.nutz.web.mvc.injector;

import org.nutz.web.mvc.adaptor.ParamInjector;

public abstract class AttrInjector implements ParamInjector {

    protected String name;

    protected AttrInjector(String name) {
        this.name = name;
    }

}
