package org.nutz.web.mvc.annotation;

import org.nutz.web.mvc.Loading;

import java.lang.annotation.*;

/**
 * 在主模块上声明加载逻辑加载逻辑
 * 
 * @author zozoh(zozohtnt@gmail.com)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface LoadingBy {

    Class<? extends Loading> value();

}
