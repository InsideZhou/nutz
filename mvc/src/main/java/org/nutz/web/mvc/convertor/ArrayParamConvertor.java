package org.nutz.web.mvc.adaptor.convertor;

import org.nutz.lang.Lang;
import org.nutz.web.mvc.adaptor.ParamConvertor;
import org.nutz.web.mvc.adaptor.Params;

import java.lang.reflect.Array;

public class ArrayParamConvertor implements ParamConvertor {

    private Class<?> eleType;

    private ParamConvertor convertor;

    public ArrayParamConvertor(Class<?> eleType) {
        this.eleType = eleType;
        this.convertor = Params.makeParamConvertor(eleType, null);
    }

    public Object convert(String[] ss) {
        if (null == ss)
            return null;

        Object re = Array.newInstance(eleType, ss.length);
        for (int i = 0; i < ss.length; i++) {
            Array.set(re, i, convertor.convert(Lang.array(ss[i])));
        }
        return re;
    }

}
