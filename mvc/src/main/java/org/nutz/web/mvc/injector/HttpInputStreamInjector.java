package org.nutz.web.mvc.injector;

import org.nutz.lang.Lang;
import org.nutz.web.mvc.adaptor.ParamInjector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HttpInputStreamInjector implements ParamInjector {

    public Object get(ServletContext sc,
                      HttpServletRequest req,
                      HttpServletResponse resp,
                      Object refer) {
        try {
            return req.getInputStream();
        }
        catch (IOException e) {
            throw Lang.wrapThrow(e);
        }
    }

}
