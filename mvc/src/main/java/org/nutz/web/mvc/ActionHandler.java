package org.nutz.web.mvc;

import org.nutz.web.mvc.impl.ActionInvoker;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ActionHandler {

    private Loading loading;

    private UrlMapping mapping;

    private NutConfig config;

    public ActionHandler(NutConfig config) {
        this.config = config;
        this.loading = config.createLoading();
        this.mapping = loading.load(config);
    }

    public boolean handle(HttpServletRequest req, HttpServletResponse resp) {
        ActionContext ac = new ActionContext();
        ac.setRequest(req).setResponse(resp).setServletContext(config.getServletContext());

        MvcUtils.setActionContext(ac);
        
        ActionInvoker invoker = mapping.get(ac);
        if (null == invoker)
            return false;
        return invoker.invoke(ac);
    }

    public void depose() {
        loading.depose(config);
    }

}
