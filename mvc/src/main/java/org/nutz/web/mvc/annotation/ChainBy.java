package org.nutz.web.mvc.annotation;

import org.nutz.web.mvc.ActionChainMaker;
import org.nutz.web.mvc.impl.NutActionChainMaker;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface ChainBy {

    Class<? extends ActionChainMaker> type() default NutActionChainMaker.class;

    String[] args();

}
