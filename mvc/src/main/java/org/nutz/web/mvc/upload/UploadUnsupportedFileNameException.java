package org.nutz.web.mvc.upload;

import org.nutz.web.upload.FieldMeta;

@SuppressWarnings("serial")
public class UploadUnsupportedFileNameException extends RuntimeException {

    public UploadUnsupportedFileNameException(FieldMeta meta) {
        super(String.format("Unsupport file name '%s' ", meta.getFileLocalPath()));
    }

}
