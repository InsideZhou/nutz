package org.nutz.web.mvc.annotation;

import java.lang.annotation.*;

/**
 * 可以声明在 POJO 字段上，或者 入口函数的参数上。 描述，应该对应到 HTTP 请求哪一个参数
 * 
 * @author zozoh(zozohtnt@gmail.com)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Documented
public @interface Param {

    /**
     * 对应到 HTTP 参数里的参数名称
     */
    String value();

    /**
     * 如果是日期对象，这个参数可以声明其特殊的格式，如果不声明，则用 Times 函数来转换
     */
    String dateFormat() default "";

    String defaultValue() default "//NOT EXIST IN//";//这个值不能随便改，org.nutz.web.mvc.adaptor.NutAdaptor也硬编码了。
}
