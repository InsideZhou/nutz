package org.nutz.web.mvc.annotation;

import org.nutz.web.mvc.Setup;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface SetupBy {

    Class<? extends Setup> value() default Setup.class;

    String[] args() default {};
}
