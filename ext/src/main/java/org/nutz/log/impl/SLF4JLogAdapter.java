package org.nutz.log.impl;

import org.nutz.log.Log;
import org.nutz.log.LogAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SLF4JLogAdapter implements LogAdapter {
    public final static SLF4JLogAdapter instance = new SLF4JLogAdapter();

    SLF4JLogAdapter() {}

    public boolean canWork() {
        try {
            Class.forName("org.slf4j.Logger", false, SLF4JLogAdapter.class.getClassLoader());
        }
        catch (ClassNotFoundException e) {
            return false;
        }

        return true;
    }

    public Log getLogger(String className) {
        return new SLF4JLogger(className);
    }

    static class SLF4JLogger extends AbstractLog {
        private Logger logger;

        SLF4JLogger(String className) {
            logger = LoggerFactory.getLogger(className);
        }

        @Override
        public boolean isDebugEnabled() {
            return logger.isDebugEnabled();
        }

        @Override
        public boolean isErrorEnabled() {
            return logger.isErrorEnabled();
        }

        @Override
        public boolean isFatalEnabled() {
            return logger.isErrorEnabled();
        }

        @Override
        public boolean isInfoEnabled() {
            return logger.isInfoEnabled();
        }

        @Override
        public boolean isTraceEnabled() {
            return logger.isTraceEnabled();
        }

        @Override
        public boolean isWarnEnabled() {
            return logger.isWarnEnabled();
        }

        @Override
        public void debug(Object message) {
            logger.debug(message.toString());
        }

        @Override
        public void error(Object message) {
            logger.error(message.toString());
        }

        @Override
        public void fatal(Object message) {
            logger.error(message.toString());
        }

        @Override
        public void info(Object message) {
            logger.info(message.toString());
        }

        @Override
        public void trace(Object message) {
            logger.trace(message.toString());
        }

        @Override
        public void warn(Object message) {
            logger.warn(message.toString());
        }

        public void debug(Object message, Throwable t) {
            logger.debug(message.toString(), t);
        }

        public void error(Object message, Throwable t) {
            logger.error(message.toString(), t);
        }

        public void fatal(Object message, Throwable t) {
            logger.error(message.toString(), t);
        }

        public void info(Object message, Throwable t) {
            logger.info(message.toString(), t);
        }

        public void trace(Object message, Throwable t) {
            logger.trace(message.toString(), t);
        }

        public void warn(Object message, Throwable t) {
            logger.warn(message.toString(), t);
        }

        @Override
        protected void log(int level, Object message, Throwable tx) {
            switch (level) {
                case LEVEL_FATAL:
                    logger.error(message.toString(), tx);
                    break;
                case LEVEL_ERROR:
                    logger.error(message.toString(), tx);
                    break;
                case LEVEL_WARN:
                    logger.warn(message.toString(), tx);
                    break;
                case LEVEL_INFO:
                    logger.info(message.toString(), tx);
                    break;
                case LEVEL_DEBUG:
                    logger.debug(message.toString(), tx);
                    break;
                case LEVEL_TRACE:
                    logger.trace(message.toString(), tx);
                    break;
                default:
                    break;
            }
        }
    }
}
