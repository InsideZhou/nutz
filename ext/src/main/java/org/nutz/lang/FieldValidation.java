package org.nutz.lang;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 被标注的字段在注入值时，验证该值是否合法。
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface FieldValidation {
    /**
     * 当返回true时，不对该字段进行注入。
     */
    public boolean readonly() default false;

    /**
     * 当返回true时，注入内容不能为null、空字符串。
     */
    public boolean required() default false;

    public long min() default Long.MIN_VALUE;

    public long max() default Long.MAX_VALUE;

    /**
     * 不为空时，注入内容必须符合该正则表达式。
     */
    public String regexp() default "";

    /**
     * 验证描述。
     */
    public String description() default "";
}
