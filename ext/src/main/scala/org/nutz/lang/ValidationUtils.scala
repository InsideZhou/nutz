package org.nutz.lang

import scala.collection.JavaConversions._

object ValidationUtils {
    def validate(value: Any, v: FieldValidation): String = {
        value match {
            case i: Int =>
                val min = if (v.min < Int.MinValue) Int.MinValue else v.min
                val max = if (v.max > Int.MaxValue) Int.MaxValue else v.max

                if (i < min || i > max) {
                    if (v.description.isEmpty) "数字范围必须在[%s,%s]之内".format(min, max) else v.description
                }
                else if (!v.regexp.isEmpty) {
                    v.regexp.r.findFirstMatchIn(i.toString) match {
                        case Some(m) => ""
                        case None => if (v.description.isEmpty) "内容不符合要求" else v.description
                    }
                }
                else {
                    ""
                }
            case i: Long =>
                if (i < v.min || i > v.max) {
                    if (v.description.isEmpty) "数字范围必须在[%s,%s]之内".format(v.min, v.max) else v.description
                }
                else if (!v.regexp.isEmpty) {
                    v.regexp.r.findFirstMatchIn(i.toString) match {
                        case Some(m) => ""
                        case None => if (v.description.isEmpty) "内容不符合要求" else v.description
                    }
                }
                else {
                    ""
                }
            case s: String =>
                if (s.isEmpty) {
                    if (v.required) {
                        if (v.description.isEmpty) "必须填写该项" else v.description
                    }
                    else {
                        ""
                    }
                }
                else {
                    if (s.size < v.min || s.size > v.max) {
                        if (v.description.isEmpty) "长度范围必须在[%s,%s]之内".format(v.min, v.max) else v.description
                    }
                    else if (!v.regexp.isEmpty) {
                        v.regexp.r.findFirstMatchIn(s) match {
                            case Some(m) => ""
                            case None => if (v.description.isEmpty) "内容不符合要求" else v.description
                        }
                    }
                    else {
                        ""
                    }
                }
            case null =>
                if (v.required) {
                    if (v.description.isEmpty) "必须填写该项" else v.description
                }
                else {
                    ""
                }
            case other =>
                if (!v.regexp.isEmpty) {
                    v.regexp.r.findFirstMatchIn(other.toString) match {
                        case Some(m) => ""
                        case None => if (v.description.isEmpty) "内容不符合要求" else v.description
                    }
                }
                else {
                    ""
                }
        }
    }

    /**
     * 忽略验证规则中的required项，强制使用规则进行验证。
     */
    def forceValidation(value: Any, v: FieldValidation): String = {
        value match {
            case i: Int =>
                validate(value, v)
            case i: Long =>
                validate(value, v)
            case s: String =>
                if (s.isEmpty) {
                    if (v.description.isEmpty) "必须填写该项" else v.description
                }
                else {
                    validate(value, v)
                }
            case null => if (v.description.isEmpty) "必须填写该项" else v.description
            case other =>
                validate(value, v)
        }
    }

    def validate[T](obj: T, names: java.util.List[String] = null, prefix: String = ""): java.util.Map[String, String] = {
        val mirror = Mirror.me(obj)
        var fields = mirror.getFields(classOf[FieldValidation])
        if (null != names && names.nonEmpty) {
            fields = fields.filter(f => names.contains(f.getName))
        }

        val pairs = fields.map(f => {
            val fName = f.getName
            val key = prefix + fName
            key -> validate(mirror.getEjecting(fName).eject(obj), f.getAnnotation(classOf[FieldValidation]))
        }).filterNot(item => item._2 == "")

        Map(pairs: _*)
    }

    def forceValidation[T](obj: T, names: java.util.List[String] = null, prefix: String = ""): java.util.Map[String, String] = {
        val mirror = Mirror.me(obj)
        var fields = mirror.getFields(classOf[FieldValidation])
        if (null != names && names.nonEmpty) {
            fields = fields.filter(f => names.contains(f.getName))
        }

        val pairs = fields.map(f => {
            val fName = f.getName
            val key = prefix + fName
            key -> forceValidation(mirror.getEjecting(fName).eject(obj), f.getAnnotation(classOf[FieldValidation]))
        }).filterNot(item => item._2 == "")

        Map(pairs: _*)
    }
}
