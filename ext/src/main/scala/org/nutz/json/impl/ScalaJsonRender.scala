package org.nutz.json.impl

import scala.collection.JavaConversions._

class ScalaJsonRender extends JsonRenderImpl {
    override def render(obj: scala.Any): Unit = obj match {
        case m: scala.collection.Map[_, _] => super.render(mapAsJavaMap(m))
        case l: Seq[_] => super.render(seqAsJavaList(l))
        case _ => super.render(obj)
    }
}
