package org.nutz.json

import org.junit.runner.RunWith
import org.nutz.json.impl.ScalaJsonRender
import org.scalatest.FreeSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class JsonSpec extends FreeSpec {
    "JsonRenderImpl" in {
        assert(Json.getJsonRender.getClass == classOf[ScalaJsonRender])
    }
}
