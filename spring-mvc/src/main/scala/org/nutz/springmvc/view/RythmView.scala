package org.nutz.springmvc.view

import javax.servlet.ServletContext
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import org.nutz.web.freemarker.AbstractRythmView
import org.rythmengine.RythmEngine
import org.springframework.web.servlet.view.AbstractUrlBasedView

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

class RythmView extends AbstractUrlBasedView {
    @BeanProperty
    var engine: RythmEngine = null

    private val internal = new AbstractRythmView {
        override def getTemplatePath: String = getUrl
    }

    override def initServletContext(servletContext: ServletContext): Unit = {
        super.initServletContext(servletContext)

        engine = new RythmEngine(Map(
            "resource.name.suffix" -> ".rythm",
            "feature.natural_template" -> "true",
            "home.template.dir" -> servletContext.getRealPath("/")
        ))
    }

    override def afterPropertiesSet(): Unit = {}

    override def renderMergedOutputModel(model: java.util.Map[String, AnyRef], request: HttpServletRequest, response: HttpServletResponse): Unit = {
        internal.doRender(request, response, model)
    }
}
