package org.nutz.springmvc.view

import java.util
import javax.servlet.ServletContext
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import org.nutz.web.freemarker.{AbstractFreemarkerView, FreemarkerConfiguration}
import org.springframework.web.servlet.view.AbstractUrlBasedView

import scala.beans.BeanProperty

class FreemarkerView extends AbstractUrlBasedView {
    @BeanProperty
    var config = FreemarkerConfiguration.getConfig

    private val internal = new AbstractFreemarkerView {
        override def getTemplatePath: String = getUrl
    }

    override def initServletContext(servletContext: ServletContext): Unit = {
        super.initServletContext(servletContext)

        config.setServletContextForTemplateLoading(FreemarkerView.this.getServletContext, "/")
        internal.config = FreemarkerView.this.config
    }

    override def setContentType(contentType: String): Unit = {
        super.setContentType(contentType)
        internal.contentType = contentType
    }

    override def afterPropertiesSet(): Unit = {}

    override def renderMergedOutputModel(model: util.Map[String, AnyRef], request: HttpServletRequest, response: HttpServletResponse): Unit = {
        internal.doRender(request, response, model)
    }
}
