package org.nutz.springmvc.view

import java.io.File
import javax.servlet.ServletContext
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import org.fusesource.scalate.TemplateEngine
import org.nutz.web.freemarker.AbstractScalateView
import org.springframework.web.servlet.view.AbstractUrlBasedView

import scala.beans.BeanProperty

class ScalateView extends AbstractUrlBasedView {
    @BeanProperty
    var engine: TemplateEngine = null

    private val internal = new AbstractScalateView {
        override def getTemplatePath: String = getUrl
    }

    override def initServletContext(servletContext: ServletContext): Unit = {
        super.initServletContext(servletContext)

        engine = TemplateEngine(List(new File(servletContext.getRealPath("/"))), "production")
    }

    override def afterPropertiesSet(): Unit = {}

    override def renderMergedOutputModel(model: java.util.Map[String, AnyRef], request: HttpServletRequest, response: HttpServletResponse): Unit = {
        internal.doRender(request, response, model)
    }
}
