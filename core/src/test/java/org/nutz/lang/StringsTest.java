package org.nutz.lang;

import org.junit.Assert;
import org.junit.Test;

public class StringsTest {
    @Test
    public void isDomain() {
        Assert.assertTrue(Strings.isDomain("popalm.com"));
        Assert.assertTrue(Strings.isDomain("www.popalm.com"));
        Assert.assertTrue(Strings.isDomain("www.admin.popalm.com"));
        Assert.assertTrue(Strings.isDomain("dev-team.popalm.com"));

        Assert.assertTrue(Strings.isDomain("www.popalm.com"));
        Assert.assertTrue(Strings.isDomain("www.-popalm.com"));
        Assert.assertTrue(Strings.isDomain("www-.popalm.com"));

        Assert.assertFalse(Strings.isDomain(null));
        Assert.assertFalse(Strings.isDomain(".popalm.com"));
        Assert.assertFalse(Strings.isDomain("popalm.com."));
        Assert.assertFalse(Strings.isDomain("www..popalm.com"));
        Assert.assertFalse(Strings.isDomain("www$.popalm.com"));
    }

    @Test
    public void isEmail() {
        Assert.assertTrue(Strings.isEmail("zhoujian@popalm.com"));
        Assert.assertTrue(Strings.isEmail("zhou-jian@popalm.com"));
        Assert.assertTrue(Strings.isEmail("zhou.jian@popalm.com"));
        Assert.assertTrue(Strings.isEmail("zhou.jian@dev.popalm.com"));

        Assert.assertTrue(Strings.isEmail("zhoujian@www.-popalm.com"));
        Assert.assertTrue(Strings.isEmail("zhoujian@www-.popalm.com"));

        Assert.assertFalse(Strings.isEmail(null));
        Assert.assertFalse(Strings.isEmail("@popalm.com"));
        Assert.assertFalse(Strings.isEmail("zhoujian@popalm.com."));
        Assert.assertFalse(Strings.isEmail("zhoujian@popalm..com"));
        Assert.assertFalse(Strings.isEmail("@zhoujian@popalm..com"));
        Assert.assertFalse(Strings.isEmail(".zhoujian@popalm..com"));
        Assert.assertFalse(Strings.isEmail("zhoujian@www$.popalm.com"));
    }

    @Test
    public void contains() {
        Assert.assertFalse(Strings.contains(null, ".."));
        Assert.assertFalse(Strings.contains("", ".."));
    }
}
