package org.nutz.lang;

public enum AlipayPaymentType {
    SUCCESS("1");

    private String state;

    private AlipayPaymentType(String s) {
        state = s;
    }

    @Override
    public String toString() {
        return state;
    }
}
