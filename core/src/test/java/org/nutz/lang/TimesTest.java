package org.nutz.lang;

import org.junit.Assert;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimesTest {
    @Test
    public void parseDuration() {
        Assert.assertEquals(3 * Times.MILLIS_PER_WEEK, Times.parseDuration("3 w"));
        Assert.assertEquals(3 * Times.MILLIS_PER_WEEK, Times.parseDuration("3 week"));
        Assert.assertEquals(3 * Times.MILLIS_PER_WEEK, Times.parseDuration("3 weeks"));
        Assert.assertEquals(3 * Times.MILLIS_PER_WEEK, Times.parseDuration("3w"));
        Assert.assertEquals(3 * Times.MILLIS_PER_WEEK, Times.parseDuration("3周"));
        Assert.assertEquals(3 * Times.MILLIS_PER_WEEK, Times.parseDuration("3星期"));
    }

    @Test
    public void toDuration() {
        Times.Duration duration = Times.toDuration(2 * Times.MILLIS_PER_WEEK + 4 * Times.MILLIS_PER_DAY + 2 * Times.MILLIS_PER_HOUR
            + 13 * Times.MILLIS_PER_MINUTE + 54 * Times.MILLIS_PER_SECOND + 103);

        Assert.assertEquals(2, duration.getWeeks());
        Assert.assertEquals(4, duration.getDays());
        Assert.assertEquals(2, duration.getHours());
        Assert.assertEquals(13, duration.getMinutes());
        Assert.assertEquals(54, duration.getSeconds());
        Assert.assertEquals(103, duration.getMillis());

        Assert.assertEquals(Times.toDuration(2 * Times.MILLIS_PER_WEEK), Times.toDuration("2 weeks"));
    }

    @Test
    public void parseDate() {
        Date d = new Date();
        String s = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").format(d);
        Assert.assertTrue(Times.JAVA_DATETIME_PATTERN_CHINESE.matcher(s).matches());
        Assert.assertTrue(Times.D(d.toString()).toString().equals(d.toString()));
        Assert.assertTrue(Times.D(s).toString().equals(d.toString()));
    }

    @Test
    public void formatDate() {
        System.out.println(Times.format(Times.now(), "MMdd"));
    }
}
