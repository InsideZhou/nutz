package org.nutz.lang;

import org.nutz.castor.Castors;
import org.junit.Assert;
import org.junit.Test;

public class CastorsTest {
    @Test
    public void string2Enum() {
        Assert.assertEquals(AlipayNotifyType.StatusSync, Castors.create().castTo("StatusSync", AlipayNotifyType.class));
        Assert.assertEquals(AlipayNotifyType.StatusSync, Castors.create().castTo("trade_status_sync", AlipayNotifyType.class));

        Assert.assertEquals(AlipayPaymentType.SUCCESS, Castors.create().castTo("SUCCESS", AlipayPaymentType.class));
        Assert.assertEquals(AlipayPaymentType.SUCCESS, Castors.create().castTo("1", AlipayPaymentType.class));

        Assert.assertEquals(AlipayRefundStatus.退款关闭, Castors.create().castTo("REFUND_CLOSE", AlipayRefundStatus.class));
        Assert.assertEquals(AlipayRefundStatus.退款成功, Castors.create().castTo("REFUND_SUCCESS", AlipayRefundStatus.class));
    }
}
