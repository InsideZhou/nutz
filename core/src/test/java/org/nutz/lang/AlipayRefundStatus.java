package org.nutz.lang;

public enum AlipayRefundStatus {
    退款成功("REFUND_SUCCESS"),
    退款关闭("REFUND_CLOSE");

    private String state;

    private AlipayRefundStatus(String s) {
        state = s;
    }

    @Override
    public String toString() {
        return state;
    }
}
