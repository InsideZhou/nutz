package org.nutz.castor.castor;

import org.nutz.castor.Castor;

import java.sql.Timestamp;
import java.util.Calendar;

public class Calendar2Timestamp extends Castor<Calendar, Timestamp> {

    @Override
    public Timestamp cast(Calendar src, Class<?> toType, String... args) {
        long ms = src.getTimeInMillis();
        return new Timestamp(ms);
    }
}
