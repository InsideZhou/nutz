package org.nutz.castor.castor;

import org.nutz.castor.Castor;

import java.sql.Timestamp;
import java.util.Calendar;

public class Timestamp2Calendar extends Castor<Timestamp, Calendar> {

    @Override
    public Calendar cast(Timestamp src, Class<?> toType, String... args) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(src.getTime());
        return c;
    }

}
