package org.nutz.castor.castor;

import org.nutz.castor.Castor;

import java.sql.Timestamp;

public class Timestamp2Long extends Castor<Timestamp, Long> {

    @Override
    public Long cast(Timestamp src, Class<?> toType, String... args) {
        return src.getTime();
    }

}
