package org.nutz.log;

import org.nutz.lang.Lang;
import org.nutz.log.impl.SystemLogAdapter;

/**
 * 获取 Log 的静态工厂方法
 *
 * @author Young(sunonfire@gmail.com)
 * @author zozoh(zozohtnt@gmail.com)
 * @author Wendal(wendal1985@gmail.com)
 */
public final class Logs {
    private static LogAdapter adapter = SystemLogAdapter.instance;

    /**
     * 返回以调用者的类命名的Log,是获取Log对象最简单的方法!
     */
    public static Log get() {
        StackTraceElement[] sts = Thread.currentThread().getStackTrace();
        if (Lang.isAndroid) {
            for (int i = 0; i < sts.length; i++) {
                if (sts[i].getClassName().equals(Logs.class.getName())) {
                    return adapter.getLogger(sts[i+1].getClassName());
                }
            }
        }
        return adapter.getLogger(sts[2].getClassName());
    }

    /**
     * Get a Log by Class
     *
     * @param clazz your class
     * @return Log
     */
    public static Log getLog(Class<?> clazz) {
        return getLog(clazz.getName());
    }

    /**
     * Get a Log by name
     *
     * @param className the name of Log
     * @return Log
     */
    public static Log getLog(String className) {
        return adapter.getLogger(className);
    }

    /**
     * 开放自定义设置LogAdapter,注意,不能设置为null!! 如果你打算完全禁用Nutz的log,可以设置为NOP_ADAPTER
     *
     * @param logAdapter 你所偏好的LogAdapter
     */
    public static void setAdapter(LogAdapter logAdapter) {
        if (null == logAdapter) {
            logAdapter = SystemLogAdapter.instance;
        }

        Logs.adapter = logAdapter;

        getLog(Logs.class).infof("ALL Nutz Log now via %s.", logAdapter.getClass().getSimpleName());
    }
}
