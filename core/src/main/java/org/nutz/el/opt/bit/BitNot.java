package org.nutz.el.opt.bit;

import org.nutz.el.opt.AbstractOpt;

import java.util.Queue;

/**
 * 非
 * @author juqkai(juqkai@gmail.com)
 *
 */
public class BitNot extends AbstractOpt {
    private Object right;
    public int fetchPriority() {
        return 2;
    }
    public void wrap(Queue<Object> operand) {
        right = operand.poll();
    }
    public Object calculate() {
        Integer rval = (Integer) calculateItem(right);
        return ~rval;
    }
    public String fetchSelf() {
        return "~";
    }
}
