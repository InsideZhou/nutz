package org.nutz.lang.meta;

import org.nutz.json.ToJson;
import org.nutz.lang.Strings;
import org.nutz.lang.Validate;

import java.io.Serializable;
import java.util.regex.Matcher;

@ToJson
public class Email implements Cloneable, Serializable {
    private static final long serialVersionUID = -6685558889524021556L;

    private String user;
    private String domain;

    public Email() {}

    public Email(String str) {
        Matcher m = Strings.EMAIL_REGEX_PATTERN.matcher(str);
        Validate.isTrue(m.matches());

        this.user = m.group("user");
        this.domain = m.group("domain");
    }

    public Email(String user, String domain) {
        Validate.isTrue(Strings.isDomain(domain));

        this.user = user;
        this.domain = domain;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        Validate.isTrue(Strings.isDomain(domain));

        this.domain = domain;
    }

    public String toJson() {
        return "\"" + toString() + "\"";
    }

    @Override
    public int hashCode() {
        if (null == user)
            return 0;
        return user.hashCode();
    }

    @SuppressWarnings("all")
    @Override
    public Email clone() throws CloneNotSupportedException {
        return new Email(user, domain);
    }

    @Override
    public boolean equals(Object obj) {
        if (null == obj)
            return false;
        if (!Email.class.isAssignableFrom(obj.getClass()))
            return false;
        if (!user.equals(((Email) obj).user))
            return false;
        if (!domain.equals(((Email) obj).domain))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return String.format("%s@%s", user, domain);
    }

}
