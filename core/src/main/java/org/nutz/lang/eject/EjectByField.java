package org.nutz.lang.eject;

import org.nutz.lang.Lang;
import org.nutz.log.Log;
import org.nutz.log.Logs;

import java.lang.reflect.Field;

public class EjectByField implements Ejecting {

    private static final Log log = Logs.getLog(EjectByField.class);

    private Field field;

    public EjectByField(Field field) {
        this.field = field;
        this.field.setAccessible(true);
    }

    public Object eject(Object obj) {
        try {
            return null == obj ? null : field.get(obj);
        }
        catch (Exception e) {
            log.info("Fail to get value by field", e);
            throw Lang.makeThrow(    "Fail to get field %s.'%s' because [%s]: %s",
                                    field.getDeclaringClass().getName(),
                                    field.getName(),
                                    Lang.unwrapThrow(e),
                                    Lang.unwrapThrow(e).getMessage());
        }
    }

}
