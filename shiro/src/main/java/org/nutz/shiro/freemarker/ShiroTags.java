package org.nutz.shiro.freemarker;

import freemarker.template.SimpleHash;

/**
 * Shortcut for injecting the tags into Freemarker
 * <p/>
 * <p>Usage: cfg.setSharedVeriable("shiro", new ShiroTags());</p>
 */
public class ShiroTags extends SimpleHash {
    public static final String NAMES_DELIMETER = ",";

    public ShiroTags() {
        put("logged", new AuthenticatedTag());
        put("guest", new GuestTag());
        put("hasAnyRole", new HasAnyRolesTag());
        put("hasPerm", new HasPermissionTag());
        put("hasRole", new HasRoleTag());
        put("lackPerm", new LacksPermissionTag());
        put("lackRole", new LacksRoleTag());
        put("notLogged", new NotAuthenticatedTag());
        put("user", new UserTag());
        put("userHasAllRole", new UserHasAllRole());
        put("userHasAllPerm", new UserHasAllPermission());
        put("hasAnyPerm", new UserTag());
    }
}