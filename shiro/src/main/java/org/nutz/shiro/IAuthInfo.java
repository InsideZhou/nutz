package org.nutz.shiro;

import java.util.Set;

/**
 * 从realm中获取的身份、权限信息。
 */
public interface IAuthInfo {
    String getPwd();

    Set<String> getStringRoles();

    Set<String> getStringPermissions();
}
