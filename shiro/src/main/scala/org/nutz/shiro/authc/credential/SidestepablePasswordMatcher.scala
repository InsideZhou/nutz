package org.nutz.shiro.authc.credential

import org.apache.shiro.authc.{AuthenticationInfo, AuthenticationToken}
import org.apache.shiro.authc.credential.PasswordMatcher
import org.nutz.shiro.authc.PassAllToken

class SidestepablePasswordMatcher extends PasswordMatcher {
    override def doCredentialsMatch(token: AuthenticationToken, info: AuthenticationInfo): Boolean = token match {
        case passAllToken: PassAllToken => true
        case _ => super.doCredentialsMatch(token, info)
    }
}
