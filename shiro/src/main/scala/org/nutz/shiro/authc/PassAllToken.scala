package org.nutz.shiro.authc

import org.apache.shiro.authc.UsernamePasswordToken

class PassAllToken(username: String, rememberMe: Boolean, host: String)
    extends UsernamePasswordToken(username, "", rememberMe, host) {

    def this(username: String, host: String) = this(username, false, host)

    def this(username: String) = this(username, "")
}
