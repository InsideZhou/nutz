package org.nutz.shiro

import java.util

import org.apache.shiro.authc._
import org.apache.shiro.authc.pam.ModularRealmAuthenticator
import org.apache.shiro.realm.Realm
import org.apache.shiro.util.SoftHashMap

import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer

/**
 * 在原本的多realm验证的基础上，遇到CredentialsException、AccountException两个异常时，优先抛出这两个异常，方便调用者处理异常。
 */
class RealmAuthenticator extends ModularRealmAuthenticator {
    override def doMultiRealmAuthentication(realms: util.Collection[Realm], token: AuthenticationToken): AuthenticationInfo = {
        val strategy = getAuthenticationStrategy
        var aggregate = strategy.beforeAllAttempts(realms, token)
        val errors = new ListBuffer[Throwable]

        realms.foreach(realm => {
            aggregate = strategy.beforeAttempt(realm, token, aggregate)
            var info: AuthenticationInfo = null
            var t: Throwable = null
            try {
                info = realm.getAuthenticationInfo(token)
            }
            catch {
                case e: Throwable =>
                    t = e
                    errors.append(e)
            }

            aggregate = strategy.afterAttempt(realm, token, info, aggregate, t)
        })

        try {
            strategy.afterAllAttempts(token, aggregate)
        }
        catch {
            case error: AuthenticationException =>
                errors.find(e => e.isInstanceOf[CredentialsException]) match {
                    case Some(e) =>
                        val subjectName = token.getPrincipal.asInstanceOf[String]
                        if (RealmAuthenticator.softMap.containsKey(subjectName)) {
                            val i = RealmAuthenticator.softMap.get(subjectName)
                            RealmAuthenticator.softMap.put(subjectName, i + 1)

                            if (i > ShiroSettings.maxAuthAttempts) {
                                throw new ExcessiveAttemptsException(subjectName)
                            }
                        }
                        else {
                            RealmAuthenticator.softMap.put(subjectName, 1)
                        }
                        throw e
                    case None =>
                        errors.find(e => e.isInstanceOf[AccountException]) match {
                            case Some(e) => throw e
                            case None => throw error
                        }
                }
        }
    }
}

object RealmAuthenticator {
    val softMap = new SoftHashMap[String, Int]()
}
