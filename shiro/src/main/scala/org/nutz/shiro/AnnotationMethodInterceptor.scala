package org.nutz.shiro

import org.apache.shiro.aop.MethodInvocation
import org.apache.shiro.authz.aop._

object AnnotationMethodInterceptor {
    val methodInterceptors = Array(new NeedsPermissionsAnnotationMethodInterceptor
        , new AuthenticatedAnnotationMethodInterceptor
        , new UserAnnotationMethodInterceptor
        , new RoleAnnotationMethodInterceptor
        , new GuestAnnotationMethodInterceptor
    )

    def assertAuthorized(methodInvocation: MethodInvocation) = {
        methodInterceptors.foreach(i => {
            if (i.supports(methodInvocation)) {
                i.assertAuthorized(methodInvocation)
            }
        })
    }
}
