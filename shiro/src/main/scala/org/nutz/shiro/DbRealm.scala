package org.nutz.shiro

import org.apache.shiro.authc.{AuthenticationInfo, AuthenticationToken, SimpleAuthenticationInfo, UnknownAccountException}
import org.apache.shiro.authz.{AuthorizationInfo, SimpleAuthorizationInfo}
import org.apache.shiro.realm.AuthorizingRealm
import org.apache.shiro.subject.{PrincipalCollection, SimplePrincipalCollection}

import scala.collection.JavaConversions._

abstract class DbRealm extends AuthorizingRealm {
    override def doGetAuthenticationInfo(token: AuthenticationToken): AuthenticationInfo = {
        val username = token.getPrincipal.asInstanceOf[String]
        val info = getAuthInfo(username)

        if (null == info) {
            throw new UnknownAccountException(username)
        }

        val authInfo = new SimpleAuthenticationInfo

        authInfo.setPrincipals(new SimplePrincipalCollection(username, this.getName))
        authInfo.setCredentials(info.getPwd)

        authInfo
    }

    override def doGetAuthorizationInfo(principals: PrincipalCollection): AuthorizationInfo = {
        val username = getAvailablePrincipal(principals).asInstanceOf[String]
        val info = getAuthInfo(username)
        val authInfo = new SimpleAuthorizationInfo()

        if (null != info) {
            authInfo.setRoles(info.getStringRoles)
            authInfo.setStringPermissions(info.getStringPermissions)
        }

        authInfo
    }

    def clearCache(username: String*): Unit = {
        super.doClearCache(new SimplePrincipalCollection(asJavaCollection(username), this.getName))
    }

    protected def getAuthInfo(username: String): IAuthInfo
}
