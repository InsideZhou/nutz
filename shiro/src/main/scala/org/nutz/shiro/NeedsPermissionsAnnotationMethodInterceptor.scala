package org.nutz.shiro

import org.apache.shiro.aop.AnnotationResolver
import org.apache.shiro.authz.aop.AuthorizingAnnotationMethodInterceptor

class NeedsPermissionsAnnotationMethodInterceptor(resolver: AnnotationResolver)
    extends AuthorizingAnnotationMethodInterceptor(new NeedsPermissionsAnnotationHandler, resolver) {

    def this() = this(null)
}
