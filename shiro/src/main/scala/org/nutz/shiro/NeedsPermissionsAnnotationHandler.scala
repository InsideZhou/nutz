package org.nutz.shiro

import java.lang.annotation.Annotation

import org.apache.shiro.authz.UnauthorizedException
import org.apache.shiro.authz.annotation.Logical
import org.apache.shiro.authz.aop.AuthorizingAnnotationHandler

class NeedsPermissionsAnnotationHandler extends AuthorizingAnnotationHandler(classOf[NeedsPermissions]) {
    override def assertAuthorized(a: Annotation): Unit = a match {
        case p: NeedsPermissions =>
            val perms = p.value
            val subject = getSubject

            if (Logical.AND.equals(p.logical)) {
                subject.checkPermissions(perms: _*)
            }
            else if (Logical.OR.equals(p.logical)) {
                if (!perms.exists(perm => {subject.isPermitted(perm)})) {
                    throw new UnauthorizedException("权限验证未通过")
                }
            }
        case _ =>
    }
}
