package org.nutz.shiro.freemarker

import java.util

import freemarker.core.Environment
import freemarker.template.{TemplateDirectiveBody, TemplateModelException}
import org.apache.shiro.SecurityUtils
import org.apache.shiro.mgt.RealmSecurityManager
import org.apache.shiro.subject.SimplePrincipalCollection

import scala.collection.JavaConversions._

class UserHasAllPermission extends SecureTag {
    override def render(env: Environment, params: util.Map[_, _], body: TemplateDirectiveBody): Unit = {
        val users = getParam(params, "user") match {
            case s if null == s || s.isEmpty => throw new TemplateModelException("用户名参数无效")
            case s => s.split(",")
        }

        val perms = getParam(params, "perms") match {
            case s if null == s || s.isEmpty => throw new TemplateModelException("权限参数无效")
            case s => s.split(",")
        }

        val settings = SecurityUtils.getSecurityManager.asInstanceOf[RealmSecurityManager]
        val principals = new SimplePrincipalCollection()
        settings.getRealms.foreach(realm => {
            principals.addAll(users.toIterable, realm.getName)
        })

        if (null != body && settings.isPermittedAll(principals, perms:_*)) {
            renderBody(env, body)
        }
    }
}
