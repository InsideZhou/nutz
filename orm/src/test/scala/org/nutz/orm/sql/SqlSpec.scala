package org.nutz.orm.sql

import org.junit.runner.RunWith
import org.nutz.lang.Strings
import org.nutz.orm.impl.sql.NutSql
import org.nutz.orm.impl.{NutDao, SimpleDataSource}
import org.nutz.orm._
import org.scalatest.FreeSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SqlSpec extends FreeSpec {
    val ds = new SimpleDataSource()
    ds.setJdbcUrl("jdbc:h2:./test")

    val dao = new NutDao()
    dao.setDataSource(ds)

    dao.create(classOf[User], true)
    dao.create(classOf[Role], true)
    dao.create(classOf[Permission], true)

    "nested" - {
        "statements" - {
            "plain" in {
                val user = dao.queryStatement(classOf[User], Cnd.where("flags", "=", 3).and("name", "=", "user"))
                val role = dao.queryStatement(classOf[Role], Cnd.where("flags", "=", 1).and("name", "=", "role"))
                val sql = Sqls.create("select * from (#usr) t1 JOIN (#role) t2 on t1.OID = t2.OID")

                sql.statements().set("usr", user)
                sql.statements().set("role", role)

                val result = sql.getParamMatrix.map(_.mkString(",")).mkString("\n")
//                println(result)

                assert("3,user,1,role".equals(result))
            }
            "vars" in {
                val user = dao.queryStatement(classOf[User], Cnd.where("flags", "=", 3).and("name", "=", "user"))
                val role = dao.queryStatement(classOf[Role], Cnd.where("flags", "=", 1).and("name", "=", "role"))
                val sql = Sqls.create("select * from (#usr) t1 JOIN (#role) t2 on t1.OID = t2.OID where t1.flags=$flags1 and t2.flags=$flags2")

                sql.statements().set("usr", user)
                sql.statements().set("role", role)

                sql.vars().set("flags1", 3)
                sql.vars().set("flags2", 1)

                val result = sql.getParamMatrix.map(_.mkString(",")).mkString("\n")
//                println(result)

                assert("3,user,1,role".equals(result))
            }
            "params" in {
                val user = dao.queryStatement(classOf[User], Cnd.where("flags", "=", 3).and("name", "=", "user"))
                val role = dao.queryStatement(classOf[Role], Cnd.where("flags", "=", 1).and("name", "=", "role"))
                val sql = Sqls.create("select * from (#usr) t1 JOIN (#role) t2 on t1.OID = t2.OID where t1.flags=@flags1 and t2.flags=@flags2")

                sql.statements().set("usr", user)
                sql.statements().set("role", role)

                sql.params().set("flags1", 3)
                sql.params().set("flags2", 1)
                sql.addBatch()
                sql.params().set("flags1", 33)
                sql.params().set("flags2", 11)

                val result = sql.getParamMatrix.map(_.mkString(",")).mkString("\n")
//                println(result)

                assert("3,user,1,role,3,1\n3,user,1,role,33,11".equals(result))
            }
            "params with vars" in {
                val user = dao.queryStatement(classOf[User], Cnd.where("flags", "=", 3).and("name", "=", "user"))
                val role = dao.queryStatement(classOf[Role], Cnd.where("flags", "=", 1).and("name", "=", "role"))
                val sql = Sqls.create("select * from (#usr) t1 JOIN (#role) t2 on t1.OID = t2.OID where t1.flags=$flags1 and t2.flags=@flags2")

                sql.statements().set("usr", user)
                sql.statements().set("role", role)

                sql.vars().set("flags1", 3)

                sql.params().set("flags2", 1)
                sql.addBatch()
                sql.params().set("flags2", 11)

                val result = sql.getParamMatrix.map(_.mkString(",")).mkString("\n")
//                println(result)

                assert("3,user,1,role,1\n3,user,1,role,11".equals(result))
            }
            "prepared" in {
                val user = dao.queryStatement(classOf[User], Cnd.where("flags", "=", 3).and("name", "=", "user"))
                val role = dao.queryStatement(classOf[Role], Cnd.where("flags", "=", 1).and("name", "=", "role"))
                val sql = Sqls.create(
                    s"""select * from (#usr) t1
                       |JOIN (#role) t2 on t1.OID = t2.OID
                       |where t1.flags=@flags1 and t2.flags=@flags2""".stripMargin
                )
                sql.statements().set("usr", user)
                sql.statements().set("role", role)
                sql.params().set("flags1", 3)
                sql.params().set("flags2", 1)

                //            println(s"""select * from (${user.toPreparedStatement}) t1
                //                   |JOIN (${role.toPreparedStatement}) t2 ON t1.OID = t2.OID
                //                   |where t1.flags=? and t2.flags=?""".stripMargin)
                //            println(sql.toPreparedStatement)

                assert(
                    s"""select * from (${user.toPreparedStatement}) t1
                                                                    |JOIN (${role.toPreparedStatement}) t2 ON t1.OID = t2.OID
                                                                                                        |where t1.flags=? and t2.flags=?""".stripMargin.equalsIgnoreCase(sql.toPreparedStatement)
                )
            }
            "execute" in {
                val user = dao.queryStatement(classOf[User], Cnd.where("flags", "=", 3).and("name", "=", "user"))
                val role = dao.queryStatement(classOf[Role], Cnd.where("flags", "=", 1).and("name", "=", "role"))
                val sql = Sqls.create("select * from (#usr) t1 JOIN (#role) t2 on t1.OID = t2.OID where t1.flags=$flags1 and t2.flags=@flags2")

                sql.statements().set("usr", user)
                sql.statements().set("role", role)

                sql.vars().set("flags1", 3)

                sql.params().set("flags2", 1)
                sql.addBatch()
                sql.params().set("flags2", 11)

                dao.execute(sql)
            }
        }
    }

    "sql" - {
        "attributes" in {
            val sql = new NutSql("drop table usr")
            assert(null != sql.getParamMatrix)
            assert(null != sql.toPreparedStatement)
            assert(Strings.isNoneBlank(sql.toString))
        }
    }
}
