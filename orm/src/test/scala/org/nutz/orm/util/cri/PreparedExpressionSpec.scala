package org.nutz.orm.util.cri

import org.junit.runner.RunWith
import org.nutz.orm.impl.{NutDao, SimpleDataSource}
import org.nutz.orm.{Cnd, Permission, Role, User}
import org.scalatest.FreeSpec
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PreparedExpressionSpec extends FreeSpec {
    val ds = new SimpleDataSource()
    ds.setJdbcUrl("jdbc:h2:./test")

    val dao = new NutDao()
    dao.setDataSource(ds)

    dao.create(classOf[User], false)
    dao.create(classOf[Role], false)
    dao.create(classOf[Permission], false)

    "Spec" - {
        "PreparedExpression" in {
            println(dao.queryStatement(classOf[User], Cnd.where("flags", "=", 3).and("name", "=", "user")
                .and(Exps.prepared("e'TT\\TT'= ALL('153415243vzwer454523'->>'code') AND prepared=? AND p2=? OR p3=?")
                .addParam(1).addParam("a").addParam(1321L)).and("state", "=", "无语")))
        }
    }
}
