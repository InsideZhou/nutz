package org.nutz.orm;


import org.nutz.lang.random.R;
import org.nutz.orm.entity.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Table("u_role")
public class Role {
    @BeforeInsert
    static void beforeInsert(Role r) {
        System.out.println(String.format("before insert role: %s,%s", r.getOID(), r.getName()));
    }

    @BeforeUpdate
    static void beforeUpdate(Role r) {
        System.out.println(String.format("before update role: %s,%s", r.getOID(), r.getName()));
    }

    @BeforeDelete
    static void beforeDelete(Role r) {
        System.out.println(String.format("before delete role: %s,%s", r.getOID(), r.getName()));
    }

    @Name
    @ColDefine(width = 22)
    private String OID = R.UU64();

    @Column
    @ColDefine(width = 64)
    private String name = "";

    @Column
    @ColDefine(type = ColType.INT)
    private int flags = 3;

    @ManyMany(target = Permission.class, relation = "rel_role_permission", from = "roid", to = "poid")
    private List<Permission> permissions = new ArrayList<>();


    public String getOID() {
        return OID;
    }

    public void setOID(String OID) {
        this.OID = OID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }
}
