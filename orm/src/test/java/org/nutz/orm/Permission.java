package org.nutz.orm;

import org.nutz.lang.random.R;
import org.nutz.orm.entity.annotation.*;

@Table("u_permission")
public class Permission {
    @BeforeInsert
    static void beforeInsert(Permission p) {
        System.out.println(String.format("before insert permission: %s,%s", p.getOID(), p.getName()));
    }

    @BeforeUpdate
    static void beforeUpdate(Permission p) {
        System.out.println(String.format("before update permission: %s,%s", p.getOID(), p.getName()));
    }

    @BeforeDelete
    static void beforeDelete(Permission p) {
        System.out.println(String.format("before delete permission: %s,%s", p.getOID(), p.getName()));
    }

    @Name
    @ColDefine(width = 22)
    private String OID = R.UU64();

    @Column
    @ColDefine(width = 64)
    private String name = "";

    public String getOID() {
        return OID;
    }

    public void setOID(String OID) {
        this.OID = OID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
