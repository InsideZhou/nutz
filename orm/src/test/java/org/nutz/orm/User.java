package org.nutz.orm;

import org.nutz.lang.random.R;
import org.nutz.orm.entity.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Table("usr")
public class User {
    @BeforeInsert
    static void beforeInsert(User u) {
        System.out.println(String.format("before insert user: %s,%s", u.getOID(), u.getName()));
    }

    @BeforeUpdate
    static void beforeUpdate(User u) {
        System.out.println(String.format("before update user: %s,%s", u.getOID(), u.getName()));
    }

    @BeforeDelete
    static void beforeDelete(User u) {
        System.out.println(String.format("before delete user: %s,%s", u.getOID(), u.getName()));
    }

    @Name
    @ColDefine(width = 22)
    private String OID = R.UU64();

    @Column
    @ColDefine(width = 64)
    private String name = "";

    @Column
    @ColDefine(type = ColType.INT)
    private int flags = 3;

    @ManyMany(target = Role.class, relation = "rel_user_role", from = "uoid", to = "roid")
    private List<Role> roles = new ArrayList<>();

    public String getOID() {
        return OID;
    }

    public void setOID(String OID) {
        this.OID = OID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
