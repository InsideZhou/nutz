package org.nutz.orm.entity;

public interface LinkVisitor {

    void visit(Object obj, LinkField lnk);

}
