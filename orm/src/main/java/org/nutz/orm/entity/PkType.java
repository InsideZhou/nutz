package org.nutz.orm.entity;

public enum PkType {

    UNKNOWN, ID, NAME, COMPOSITE

}
