package org.nutz.orm.entity.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
@Documented
public @interface AfterUpdate {
    SQL[] value() default {};

    EL[] els() default {};

    boolean ignoreResult() default false;
}
