package org.nutz.orm.entity.annotation;

import java.lang.annotation.*;

/**
 * 一个标识，声明了本注解的字段，在插入和更新时，会被忽略。
 * <p>
 * 通常这个注解会同 '@View' 联用。
 * 
 * @author zozoh(zozohtnt@gmail.com)
 * 
 * @see View
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
public @interface Readonly {}
