package org.nutz.orm.entity.annotation;

import java.lang.annotation.*;

/**
 * 声明一组数据表的索引
 * 
 * @see Index
 * 
 * @author zozoh(zozohtnt@gmail.com)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface TableIndexes {

    Index[] value();

}
