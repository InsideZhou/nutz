package org.nutz.orm.jdbc;

import org.nutz.orm.DaoException;
import org.nutz.filepool.FilePool;
import org.nutz.filepool.NutFilePool;
import org.nutz.filepool.SynchronizedFilePool;
import org.nutz.lang.Mirror;
import org.nutz.log.Log;
import org.nutz.log.Logs;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

public class JdbcExpertConfigFile {

    private Map<String, Class<? extends JdbcExpert>> experts;

    private Map<Pattern, Class<? extends JdbcExpert>> _experts;

    private Map<String, Object> config;

    private FilePool pool;

    private static final Log log = Logs.getLog(JdbcExpertConfigFile.class);

    JdbcExpertConfigFile init() {
        // 即使初始化失败,也继续执行
        String home = null;
        try {
            File f = java.nio.file.Files.createTempDirectory(config.get("pool-home").toString()).toFile();
            home = f.getAbsolutePath();
            long max = config.containsKey("pool-max") ? ((Number) config.get("pool-max")).longValue() : 2000;
            pool = new NutFilePool(home, max);
            pool = new SynchronizedFilePool(pool);
        } catch (Throwable e) {
            log.warn("NutDao FilePool create fail!! Blob and Clob Support is DISABLE!! Home=" + home, e);
        }
        return this;
    }

    public JdbcExpert getExpert(String str) {
        Class<? extends JdbcExpert> type = experts.get(str);
        return Mirror.me(type).born(config);
    }

    public JdbcExpert matchExpert(String dbName) {
        for (Entry<Pattern, Class<? extends JdbcExpert>> entry : _experts.entrySet()) {
            if (entry.getKey().matcher(dbName).find())
                return Mirror.me(entry.getValue()).born(this);
        }
        return null;
    }

    /**
     * 注意,返回的Map实例不允许被修改
     */
    public Map<String, Class<? extends JdbcExpert>> getExperts() {
        return Collections.unmodifiableMap(experts);
    }

    public Map<String, Object> getConfig() {
        return config;
    }

    public FilePool getPool() {
        if (pool == null) {
            log.warnf("NutDao FilePool create fail!! Blob and Clob Support is DISABLE!!");
            throw new DaoException("NutDao FilePool create fail!! Blob and Clob Support is DISABLE!!");
        }
        return pool;
    }

    public synchronized void setPool(FilePool pool) {
        this.pool = pool;
    }

    // 在 fromJson 的时候，会被调用
    public void setExperts(Map<String, Class<? extends JdbcExpert>> experts) {
        this.experts = experts;
        this._experts = new HashMap<Pattern, Class<? extends JdbcExpert>>();
        for (Entry<String, Class<? extends JdbcExpert>> entry : experts.entrySet()) {
            //忽略大小写,并且让换行符与.能够匹配
            _experts.put(Pattern.compile(entry.getKey(), Pattern.DOTALL & Pattern.CASE_INSENSITIVE), entry.getValue());
        }
    }

    public void setConfig(Map<String, Object> config) {
        this.config = config;
    }

}
