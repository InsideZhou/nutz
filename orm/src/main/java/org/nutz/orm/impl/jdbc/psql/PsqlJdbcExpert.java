package org.nutz.orm.impl.jdbc.psql;

import org.nutz.lang.Lang;
import org.nutz.orm.DB;
import org.nutz.orm.Dao;
import org.nutz.orm.Sqls;
import org.nutz.orm.entity.Entity;
import org.nutz.orm.entity.MappingField;
import org.nutz.orm.entity.PkType;
import org.nutz.orm.impl.jdbc.AbstractJdbcExpert;
import org.nutz.orm.jdbc.JdbcExpertConfigFile;
import org.nutz.orm.jdbc.ValueAdaptor;
import org.nutz.orm.pager.Pager;
import org.nutz.orm.sql.Pojo;
import org.nutz.orm.sql.Sql;
import org.nutz.orm.util.Pojos;

import java.util.List;

public class PsqlJdbcExpert extends AbstractJdbcExpert {

    public PsqlJdbcExpert(JdbcExpertConfigFile conf) {
        super(conf);
    }

    public String getDatabaseType() {
        return DB.PSQL.name();
    }

    public void formatQuery(Pojo pojo) {
        Pager pager = pojo.getContext().getPager();
        // 需要进行分页
        if (null != pager && pager.getPageNumber() > 0)
            pojo.append(Pojos.Items.wrapf(" LIMIT %d OFFSET %d",
                pager.getPageSize(),
                pager.getOffset()));
    }

    public void formatQuery(Sql sql) {
        Pager pager = sql.getContext().getPager();
        if (null != pager && pager.getPageNumber() > 0) {
            sql.setSourceSql(sql.getSourceSql() + String.format(" LIMIT %d OFFSET %d",
                pager.getPageSize(),
                pager.getOffset()));
        }
    }

    public void createEntity(Dao dao, Entity<?> en) {
        StringBuilder sb = new StringBuilder("CREATE TABLE " + en.getTableName() + "(");
        // 创建字段
        for (MappingField mf : en.getMappingFields()) {
            sb.append('\n').append(mf.getColumnName());
            // 自增主键特殊形式关键字
            if (mf.isId() && mf.isAutoIncreasement()) {
                sb.append(" SERIAL");
            }
            else {
                sb.append(' ').append(evalFieldType(mf));
                // 非主键的 @Name，应该加入唯一性约束
                if (mf.isName() && en.getPkType() != PkType.NAME) {
                    sb.append(" UNIQUE NOT NULL");
                }
                // 普通字段
                else {
                    if (mf.isUnsigned())
                        sb.append(" UNSIGNED");
                    if (mf.isNotNull())
                        sb.append(" NOT NULL");
                    if (mf.isAutoIncreasement())
                        throw Lang.noImplement();
                    if (mf.hasDefaultValue())
                        sb.append(" DEFAULT '").append(getDefaultValue(mf)).append('\'');
                }
            }
            sb.append(',');
        }
        // 创建主键
        List<MappingField> pks = en.getPks();
        if (!pks.isEmpty()) {
            sb.append('\n');
            sb.append(String.format("CONSTRAINT %s_pkey PRIMARY KEY (", en.getTableName().replace('.', '_').replace('"', '_')));
            for (MappingField pk : pks) {
                sb.append(pk.getColumnName()).append(',');
            }
            sb.setCharAt(sb.length() - 1, ')');
            sb.append("\n ");
        }

        // 结束表字段设置
        sb.setCharAt(sb.length() - 1, ')');

        // 执行创建语句
        dao.execute(Sqls.create(sb.toString()));

        // 创建索引
        dao.execute(Lang.collection2array(createIndexs(en), Sql.class));

        // 创建关联表
        createRelations(dao, en);

        // 添加注释(表注释与字段注释)
        addComment(dao, en);
    }

    @Override
    protected String evalFieldType(MappingField mf) {
        if (mf.getCustomDbType() != null)
            return mf.getCustomDbType();
        switch (mf.getColumnType()) {
            case INT:
                // 用户自定义了宽度
                if (mf.getWidth() > 0) {
                    return "NUMERIC(" + mf.getWidth() + ")";
                }
                break;
            case FLOAT:
                // 用户自定义了精度
                if (mf.getWidth() > 0 && mf.getPrecision() > 0) {
                    return "NUMERIC(" + mf.getWidth() + "," + mf.getPrecision() + ")";
                }
                // 用默认精度
                if (mf.getTypeMirror().isDouble())
                    return "NUMERIC(15,10)";
                return "NUMERIC";

            case BINARY:
                return "BYTEA";

            case DATETIME:
                return "TIMESTAMP";
            default:
                break;
        }
        return super.evalFieldType(mf);
    }

    protected String createResultSetMetaSql(Entity<?> en) {
        return "SELECT * FROM " + en.getViewName() + " LIMIT 1";
    }

    @Override
    public ValueAdaptor getAdaptor(MappingField ef) {
        if ("JSON".equalsIgnoreCase(ef.getCustomDbType())) {
            return new PsqlJsonAdaptor<>(ef.getTypeMirror());
        }
        return super.getAdaptor(ef);
    }
}
