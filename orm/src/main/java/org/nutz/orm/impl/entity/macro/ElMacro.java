package org.nutz.orm.impl.entity.macro;

import org.nutz.el.El;
import org.nutz.lang.util.Context;
import org.nutz.orm.entity.Entity;
import org.nutz.orm.entity.MappingField;
import org.nutz.orm.impl.jdbc.NutPojo;
import org.nutz.orm.sql.Pojo;
import org.nutz.orm.sql.SqlType;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ElMacro extends NutPojo {

    private El bin;

    private MappingField entityField;

    private boolean ignoreMarcoValue = false;

    public SqlType getSqlType() {
        return SqlType.RUN;
    }

    public ElMacro(Entity<?> entity, MappingField field, String str) {
        this(entity, field, str, false);
    }

    public ElMacro(Entity<?> entity, MappingField field, String str, boolean ignoreMarcoValue) {
        this.ignoreMarcoValue = ignoreMarcoValue;
        this.entityField = field;
        this.bin = new El(str);
        this.setEntity(entity);
    }

    private ElMacro() {}

    public void onAfter(Connection conn, ResultSet rs) throws SQLException {
        Context context = getEntity().wrapAsContext(getOperatingObject());

        if (null == entityField) {
            context.set("view", getEntity());
            bin.eval(context);
        }
        else {
            context.set("field", entityField.getColumnName());
            context.set("view", getEntity());
            Object value = bin.eval(context);
            if (!ignoreMarcoValue) {
                entityField.setValue(getOperatingObject(), value);
            }
        }
    }

    @Override
    public Pojo duplicate() {
        ElMacro re = new ElMacro();
        re.ignoreMarcoValue = ignoreMarcoValue;
        re.bin = bin;
        re.entityField = entityField;
        re.setEntity(getEntity());
        return re;
    }
}
