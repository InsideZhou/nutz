package org.nutz.orm.impl.jdbc.sybase;

import org.nutz.lang.Lang;
import org.nutz.orm.DB;
import org.nutz.orm.Dao;
import org.nutz.orm.entity.Entity;
import org.nutz.orm.impl.jdbc.AbstractJdbcExpert;
import org.nutz.orm.jdbc.JdbcExpertConfigFile;
import org.nutz.orm.sql.Pojo;

public class SybaseIQJdbcExpert extends AbstractJdbcExpert {

    public SybaseIQJdbcExpert(JdbcExpertConfigFile conf) {
        super(conf);
    }

    public String getDatabaseType() {
        return DB.SYBASE.name();
    }

    public void createEntity(Dao dao, Entity<?> en) {
        throw Lang.noImplement();
    }

    public void formatQuery(Pojo pojo) {
        throw Lang.noImplement();
    }

}
