package org.nutz.orm.impl.sql.callback;

import org.nutz.orm.entity.Record;
import org.nutz.orm.pager.ResultSetLooping;
import org.nutz.orm.sql.Sql;
import org.nutz.orm.sql.SqlCallback;
import org.nutz.orm.sql.SqlContext;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class QueryRecordCallback implements SqlCallback {

    public Object invoke(Connection conn, ResultSet rs, Sql sql) throws SQLException {
        ResultSetLooping ing = new ResultSetLooping() {
            protected boolean createObject(int index, ResultSet rs, SqlContext context, int rowCout) {
                list.add(Record.create(rs));
                return true;
            }
        };
        ing.doLoop(rs, sql.getContext());
        return ing.getList();
    }

}
