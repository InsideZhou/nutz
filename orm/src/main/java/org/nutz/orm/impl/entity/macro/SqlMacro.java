package org.nutz.orm.impl.entity.macro;

import org.nutz.lang.Lang;
import org.nutz.orm.Sqls;
import org.nutz.orm.entity.Entity;
import org.nutz.orm.entity.MappingField;
import org.nutz.orm.impl.jdbc.NutPojo;
import org.nutz.orm.jdbc.ValueAdaptor;
import org.nutz.orm.sql.Pojo;
import org.nutz.orm.sql.Sql;
import org.nutz.orm.sql.SqlType;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlMacro extends NutPojo {

    private Sql sql;

    private MappingField entityField;

    private boolean ignoreMarcoValue = false;

    private SqlMacro() {
        super();
    }

    public SqlMacro(Entity<?> entity, MappingField field, String str) {
        this(entity, field, str, false);
    }

    public SqlMacro(Entity<?> entity, MappingField field, String str, boolean ignoreMarcoValue) {
        super();
        this.ignoreMarcoValue = ignoreMarcoValue;
        this.entityField = field;
        this.sql = Sqls.create(str);
        this.setSqlType(this.sql.getSqlType());
        this.setEntity(entity);
    }

    @Override
    public Pojo setOperatingObject(Object obj) {
        super.setOperatingObject(obj);
        if (null != obj) {
            Entity<?> en = getEntity();
            if (!en.getType().isInstance(obj))
                throw Lang.makeThrow("Invalid operating object '%s' for field '%s'",
                    obj.getClass().getName(),
                    entityField);

            // 填充占位符 ...
            for (String name : sql.varIndex().names())
                if (!name.equals("table") && !name.equals("view") && !name.equals("field"))
                    sql.vars().set(name, en.getField(name).getValue(obj));
            // 填充变量 ...
            for (String name : sql.paramIndex().names())
                sql.params().set(name, en.getField(name).getValue(obj));
        }
        return this;
    }

    public void onAfter(Connection conn, ResultSet rs) throws SQLException {
        if (rs.next() && null != entityField && !ignoreMarcoValue) {
            String colName = rs.getMetaData().getColumnName(1);
            Object obj = entityField.getAdaptor().get(rs, colName);
            entityField.setValue(this.getOperatingObject(), obj);
        }
    }

    public SqlType getSqlType() {
        return sql.getSqlType();
    }

    public ValueAdaptor[] getAdaptors() {
        return sql.getAdaptors();
    }

    public Object[][] getParamMatrix() {
        return sql.getParamMatrix();
    }

    public String toPreparedStatement() {
        return _parseSQL(sql.duplicate()).toPreparedStatement();
    }

    @Override
    public Pojo duplicate() {
        SqlMacro re = new SqlMacro();
        re.ignoreMarcoValue = ignoreMarcoValue;
        re.sql = sql.duplicate();
        re.entityField = entityField;
        re.setSqlType(sql.getSqlType());
        re.setEntity(getEntity());
        return re;
    }

    private Sql _parseSQL(Sql sql) {
        for (String name : sql.varIndex().names()) {
            if ("view".equals(name))
                sql.vars().set("view", getEntity().getViewName());
            else if ("table".equals(name))
                sql.vars().set("table", getEntity().getTableName());
            else if ("field".equals(name) && null != entityField)
                sql.vars().set("field", entityField.getColumnName());
            else
                sql.vars().set(name, getEntity().getField(name).getValue(getOperatingObject()));
        }

        for (String name : sql.paramIndex().names()) {
            sql.params().set(name, getEntity().getField(name).getValue(getOperatingObject()));
        }

        return sql;
    }
}
