package org.nutz.orm.impl.sql.callback;

import org.nutz.orm.sql.Sql;
import org.nutz.orm.sql.SqlCallback;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * 返回 String 列表
 * 
 * @author zozoh(zozohtnt@gmail.com)
 */
public class QueryStringCallback implements SqlCallback {

    public Object invoke(Connection conn, ResultSet rs, Sql sql) throws SQLException {
        List<String> list = new LinkedList<String>();
        while (rs.next())
            list.add(rs.getString(1));
        return list;
    }

}
