package org.nutz.orm.impl.sql.callback;

import org.nutz.orm.sql.Sql;
import org.nutz.orm.sql.SqlCallback;
import org.nutz.lang.util.LinkedLongArray;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 这个回调将返回一个长整型数组
 * 
 * @author zozoh(zozohtnt@gmail.com)
 */
public class QueryLongCallback implements SqlCallback {

    public Object invoke(Connection conn, ResultSet rs, Sql sql) throws SQLException {
        LinkedLongArray ary = new LinkedLongArray(20);
        while (rs.next())
            ary.push(rs.getLong(1));
        return ary.toArray();
    }

}
