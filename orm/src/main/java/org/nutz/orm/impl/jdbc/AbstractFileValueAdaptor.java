package org.nutz.orm.impl.jdbc;

import org.nutz.orm.jdbc.ValueAdaptor;
import org.nutz.filepool.FilePool;

import java.io.File;

abstract class AbstractFileValueAdaptor implements ValueAdaptor {

    private FilePool pool;

    String suffix;

    AbstractFileValueAdaptor(FilePool pool) {
        this.pool = pool;
    }

    File createTempFile() {
        return pool.createFile(suffix);
    }

}
