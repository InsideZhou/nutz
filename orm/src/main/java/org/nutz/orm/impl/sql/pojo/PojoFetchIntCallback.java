package org.nutz.orm.impl.sql.pojo;

import org.nutz.orm.sql.Pojo;
import org.nutz.orm.sql.PojoCallback;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PojoFetchIntCallback implements PojoCallback {

    public Object invoke(Connection conn, ResultSet rs, Pojo pojo) throws SQLException {
        if(null!=rs && rs.next()){
            return rs.getInt(1);
        }
        return -1;
    }

}
