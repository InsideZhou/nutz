package org.nutz.orm.impl.entity.info;

import org.nutz.lang.eject.Ejecting;
import org.nutz.lang.inject.Injecting;

import java.lang.reflect.Type;

abstract class FieldInfo {

    public String name;

    public Type fieldType;

    public Ejecting ejecting;

    public Injecting injecting;

}
