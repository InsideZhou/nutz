package org.nutz.orm.impl.sql;

import org.nutz.lang.Lang;
import org.nutz.lang.Mirror;
import org.nutz.orm.Condition;
import org.nutz.orm.entity.Entity;
import org.nutz.orm.jdbc.Jdbcs;
import org.nutz.orm.jdbc.ValueAdaptor;
import org.nutz.orm.pager.Pager;
import org.nutz.orm.sql.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class NutSql extends NutStatement implements Sql {

    SqlLiteral literal;

    private VarSet vars;

    private VarSet statements;

    private List<VarSet> rows;

    private VarSet lastRow;

    private SqlCallback callback;

    private ValueAdaptor[] adaptors;

    private NutSql() {
        super();
        vars = new SimpleVarSet();
        statements = new SimpleVarSet();
        rows = new LinkedList<>();
        lastRow = new SimpleVarSet();
        rows.add(lastRow);
    }

    public NutSql(String sql) {
        this();
        this.literal = new SqlLiteral().valueOf(sql);
        this.setSqlType(this.literal.getType());
    }

    private NutSql(SqlLiteral literal, SqlCallback callback) {
        this();
        this.literal = literal;
        this.setSqlType(this.literal.getType());
        this.callback = callback;
    }

    /**
     * 根据子查询和参数的数量实时初始化参数适配器集合。
     */
    private void generateAdaptors() {
        if (null == adaptors) {
            int index = this.getFullParams(this.lastRow, this.literal).length;
            adaptors = new ValueAdaptor[index];
        }
    }

    public Sql setEntity(Entity<?> en) {
        return (Sql) super.setEntity(en);
    }

    public ValueAdaptor[] getAdaptors() {
        this.generateAdaptors();

        //使用第一行参数来作为类型适配的基准。
        Object[] params = this.getFullParams(rows.get(0), this.literal);

        for (int i = 0; i < adaptors.length; i++) {
            // 自动决定
            if (null == adaptors[i]) {
                Object param = params[i];
                if (null == param) {
                    adaptors[i] = Jdbcs.Adaptor.asNull;
                }
                else {
                    adaptors[i] = Jdbcs.getAdaptor(Mirror.me(param));
                }
            }
        }
        return adaptors;
    }

    public void setValueAdaptor(String name, ValueAdaptor adaptor) {
        this.generateAdaptors();

        if (adaptors.length != this.getFullParams(this.lastRow, this.literal).length) {
            throw Lang.makeThrow("所有子查询占位符的值必须在所有参数适配器的值之前设置。");
        }

        int[] is = literal.getParamIndexes().getOrderIndex(name);
        if (null != is)
            for (int i : is)
                adaptors[i] = adaptor;
    }

    public Object[][] getParamMatrix() {
        Object[] lastRowParams = getFullParams(lastRow, literal);
        if (lastRowParams.length > 0) {
            Object[][] re = new Object[rows.size()][lastRowParams.length];
            for (int i = 0; i < re.length - 1; ++i) {
                VarSet row = rows.get(i);
                re[i] = getFullParams(row, literal);
            }
            re[rows.size() - 1] = lastRowParams;
            return re;
        }

        return new Object[0][0];
    }

    private Object[] getFullParams(VarSet row, SqlLiteral sqlLiteral) {
        Map<Integer, String> paramIndex2NameMap = sqlLiteral.getParamIndexes().getIndex2NameMap();
        Map<Integer, String> statementIndex2NameMap = sqlLiteral.getStatementIndexes().getIndex2NameMap();

        List<Object> fullParams = new ArrayList<>();
        for (int i : sqlLiteral.stack.getIndexes()) {
            if (statementIndex2NameMap.containsKey(i)) {
                String name = statementIndex2NameMap.get(i);
                Object value = statements.get(name);
                Object[][] paramMatrix = ((DaoStatement) value).getParamMatrix();
                if (paramMatrix.length > 0) {
                    fullParams.addAll(Lang.array2list(paramMatrix[0]));
                }
            }
            else if (paramIndex2NameMap.containsKey(i)) {
                String name = paramIndex2NameMap.get(i);
                Object value = row.get(name);
                fullParams.add(value);
            }
        }

        return fullParams.toArray();
    }

    public String toPreparedStatement() {
        String[] ss = _createSqlElements();

        // 填充参数
        VarIndex vIndex = literal.getParamIndexes();
        for (String name : vIndex.names()) {
            int[] is = vIndex.indexesOf(name);
            for (int i : is)
                ss[i] = "?";

        }
        return Lang.concat(ss).toString();
    }

    /**
     * 获取语句模板并填充占位符
     *
     * @return 语句模板
     */
    private String[] _createSqlElements() {
        String[] ss = literal.stack.cloneChain();
        VarIndex vIndex = literal.getVarIndexes();
        VarSet vs = vars;
        for (String name : vIndex.names()) {
            int[] is = vIndex.indexesOf(name);
            Object obj = vs.get(name);
            for (int i : is) {
                ss[i] = null == obj ? "" : obj.toString();
            }
        }

        VarIndex sIndex = literal.getStatementIndexes();
        VarSet sts = statements;
        for (String name : sIndex.names()) {
            int[] is = sIndex.indexesOf(name);
            Object obj = sts.get(name);
            for (int i : is) {
                ss[i] = ((DaoStatement) obj).toPreparedStatement();
            }
        }
        return ss;
    }

    public VarSet vars() {
        return vars;
    }

    public VarSet params() {
        return lastRow;
    }

    public VarSet statements() {
        return statements;
    }

    public VarIndex varIndex() {
        return literal.getVarIndexes();
    }

    public VarIndex paramIndex() {
        return literal.getParamIndexes();
    }

    public VarIndex statementIndex() {
        return literal.getStatementIndexes();
    }

    public void addBatch() {
        lastRow = new SimpleVarSet();
        rows.add(lastRow);
    }

    public void clearBatch() {
        rows.clear();
        addBatch();
    }

    public Sql setCallback(SqlCallback callback) {
        this.callback = callback;
        return this;
    }

    public Sql setCondition(Condition cnd) {
        this.vars().set("condition", cnd.toSql(this.getEntity()));
        return this;
    }

    public Sql duplicate() {
        return new NutSql(literal, callback);
    }

    public void onBefore(Connection conn) {}

    public void onAfter(Connection conn, ResultSet rs) throws SQLException {
        if (null != callback)
            getContext().setResult(callback.invoke(conn, rs, this));
    }

    @Override
    public String toString() {
        /*
         * // 语句模板 String[] ss = _createSqlElements();
         * 
         * // 填充参数 VarIndex vIndex = literal.getParamIndexes(); VarSet vs =
         * rows.get(0); for (String name : vIndex.names()) { int[] is =
         * vIndex.indexesOf(name); String s =
         * Sqls.formatFieldValue(vs.get(name)).toString(); for (int i : is)
         * ss[i] = s; }
         * 
         * return Lang.concat(ss).toString();
         */
        return super.toStatement(this.getParamMatrix(), this.toPreparedStatement());
    }

    public NutSql setPager(Pager pager) {
        this.getContext().setPager(pager);
        return this;
    }

    public void setSourceSql(String sql) {
        if (literal != null)
            literal.valueOf(sql);
        else
            literal = new SqlLiteral().valueOf(sql);
    }

    public String getSourceSql() {
        return this.literal.toString();
    }
}
