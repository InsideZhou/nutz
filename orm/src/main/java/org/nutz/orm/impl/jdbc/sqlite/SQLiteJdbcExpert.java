package org.nutz.orm.impl.jdbc.sqlite;

import org.nutz.orm.DB;
import org.nutz.orm.Dao;
import org.nutz.orm.Sqls;
import org.nutz.orm.entity.Entity;
import org.nutz.orm.entity.MappingField;
import org.nutz.orm.entity.PkType;
import org.nutz.orm.impl.entity.macro.SqlMacro;
import org.nutz.orm.impl.jdbc.mysql.MysqlJdbcExpert;
import org.nutz.orm.jdbc.JdbcExpertConfigFile;
import org.nutz.orm.sql.Pojo;
import org.nutz.orm.sql.Sql;

import java.util.List;

/**
 * 
 * @author wendal
 */
public class SQLiteJdbcExpert extends MysqlJdbcExpert {

    public SQLiteJdbcExpert(JdbcExpertConfigFile conf) {
        super(conf);
    }

    @Override
    public String getDatabaseType() {
        return DB.SQLITE.name();
    }

    @Override
    public void createEntity(Dao dao, Entity<?> en) {
        StringBuilder sb = new StringBuilder("CREATE TABLE " + en.getTableName() + "(");
        if (en.getPks().size() > 1 && en.getPkType() == PkType.ID) {
            return;
        }
        // 创建字段
        boolean mPks = en.getPks().size() > 1;
        for (MappingField mf : en.getMappingFields()) {
            sb.append('\n').append(mf.getColumnName());
            // Sqlite的整数型主键,一般都是自增的,必须定义为(PRIMARY KEY
            // AUTOINCREMENT),但这样就无法定义多主键!!
            if (mf.isId() && en.getPkType() == PkType.ID) {
                sb.append(" INTEGER PRIMARY KEY AUTOINCREMENT,");
                continue;
            } else
                sb.append(' ').append(evalFieldType(mf));
            // 非主键的 @Name，应该加入唯一性约束
            if (mf.isName() && en.getPkType() != PkType.NAME) {
                sb.append(" UNIQUE NOT NULL");
            }
            // 普通字段
            else {
                if (mf.isUnsigned())
                    sb.append(" UNSIGNED");
                if (mf.isNotNull())
                    sb.append(" NOT NULL");
                if (mf.isPk() && !mPks) {// 复合主键需要另外定义
                    sb.append(" PRIMARY KEY");
                }
                if (mf.hasDefaultValue())
                    sb.append(" DEFAULT '").append(getDefaultValue(mf)).append('\'');
            }
            sb.append(',');
        }
        // 创建主键
        List<MappingField> pks = en.getPks();
        if (mPks) {
            sb.append('\n');
            sb.append("constraint pk_").append(en.getTableName()).append(" PRIMARY KEY (");
            for (MappingField pk : pks) {
                sb.append(pk.getColumnName()).append(',');
            }
            sb.setCharAt(sb.length() - 1, ')');
            sb.append("\n ");
        }
        // 创建索引
        dao.execute(createIndexs(en).toArray(new Sql[0]));

        // 结束表字段设置
        sb.setCharAt(sb.length() - 1, ')');

        // 执行创建语句
        dao.execute(Sqls.create(sb.toString()));
        // 创建关联表
        createRelations(dao, en);
    }
    
    public Pojo fetchPojoId(Entity<?> en, MappingField idField) {
    	String autoSql = "SELECT MAX($field) AS $field FROM $view";
        Pojo autoInfo = new SqlMacro(en, idField, autoSql);
        autoInfo.setEntity(en);
        return autoInfo;
    }
}
