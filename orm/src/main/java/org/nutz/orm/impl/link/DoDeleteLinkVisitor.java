package org.nutz.orm.impl.link;

import org.nutz.orm.entity.LinkField;
import org.nutz.orm.impl.AbstractLinkVisitor;
import org.nutz.orm.sql.Pojo;
import org.nutz.orm.util.Pojos;
import org.nutz.lang.Each;
import org.nutz.lang.ExitLoop;
import org.nutz.lang.Lang;
import org.nutz.lang.LoopException;
import org.nutz.log.Log;
import org.nutz.log.Logs;

public class DoDeleteLinkVisitor extends AbstractLinkVisitor {
    
    private static final Log log = Logs.getLog(DoDeleteLinkVisitor.class);

    public void visit(Object obj, LinkField lnk) {
        Object value = lnk.getValue(obj);
        if (value == null || Lang.length(value) == 0) {
            log.infof("Value of LinkField(@%s-->%s.%s) is null or isEmtry, ingore",
                       lnk.getLinkType(), lnk.getEntity().getType().getSimpleName(),
                       lnk.getHostField().getName());
            return;
        }

        final Pojo pojo = opt.maker().makeDelete(lnk.getLinkedEntity());
        pojo.setOperatingObject(value);
        pojo.append(Pojos.Items.cndAuto(lnk.getLinkedEntity(), null));
        Lang.each(value, new Each<Object>() {
            public void invoke(int i, Object ele, int length) throws ExitLoop, LoopException {
                pojo.addParamsBy(ele);
            }
        });

        opt.add(pojo);
    }

}
