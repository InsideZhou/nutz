package org.nutz.orm.impl.link;

import org.nutz.orm.entity.LinkField;
import org.nutz.orm.impl.AbstractLinkVisitor;
import org.nutz.orm.impl.entity.field.ManyManyLinkField;
import org.nutz.orm.sql.Pojo;
import org.nutz.orm.util.Pojos;

public class DoClearRelationByHostFieldLinkVisitor extends AbstractLinkVisitor {

    public void visit(Object obj, LinkField lnk) {
        if (lnk instanceof ManyManyLinkField) {
            final ManyManyLinkField mm = (ManyManyLinkField) lnk;

            final Pojo pojo = opt.maker().makeDelete(mm.getRelationName());
            pojo.append(Pojos.Items.cndColumn(    mm.getFromColumnName(),
                                                mm.getHostField(),
                                                mm.getHostField().getValue(obj)));

            opt.add(pojo);
        }
    }

}
