package org.nutz.orm.impl.sql.callback;

import org.nutz.orm.sql.Sql;
import org.nutz.orm.sql.SqlCallback;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 这个回调将返回一个 boolean 值
 * 
 * @author Conanca(conanca2006@gmail.com)
 */
public class FetchBooleanCallback implements SqlCallback {

    public Object invoke(Connection conn, ResultSet rs, Sql sql) throws SQLException {
        if (null != rs && rs.next())
            return rs.getBoolean(1);
        return null;
    }

}
