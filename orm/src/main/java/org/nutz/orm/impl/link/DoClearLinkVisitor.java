package org.nutz.orm.impl.link;

import org.nutz.orm.entity.LinkField;
import org.nutz.orm.impl.AbstractLinkVisitor;
import org.nutz.orm.sql.Pojo;
import org.nutz.orm.util.Pojos;

public class DoClearLinkVisitor extends AbstractLinkVisitor {

    public void visit(final Object obj, final LinkField lnk) {
        Pojo pojo = opt.maker().makeDelete(lnk.getLinkedEntity());
        pojo.append(Pojos.Items.cnd(lnk.createCondition(obj)));
        pojo.setOperatingObject(obj);
        opt.add(pojo);
    }

}
