package org.nutz.orm.impl.link;

import org.nutz.orm.Condition;
import org.nutz.orm.entity.Entity;
import org.nutz.orm.entity.LinkField;
import org.nutz.orm.impl.AbstractLinkVisitor;
import org.nutz.orm.impl.entity.field.ManyManyLinkField;
import org.nutz.orm.sql.PItem;
import org.nutz.orm.sql.Pojo;
import org.nutz.orm.util.Pojos;

import java.util.Map;

public class DoUpdateRelationLinkVisitor extends AbstractLinkVisitor {

    private Map<String, Object> map;

    private PItem[] items;

    public DoUpdateRelationLinkVisitor(Map<String, Object> map, Condition cnd) {
        this.map = map;
        this.items = Pojos.Items.cnd(cnd);
    }

    public void visit(Object obj, LinkField lnk) {
        if (lnk instanceof ManyManyLinkField) {
            ManyManyLinkField mm = (ManyManyLinkField) lnk;
            Entity<?> en = opt.makeEntity(mm.getRelationName(), map);
            Pojo pojo = opt.maker().makeUpdate(en, null);
            pojo.setOperatingObject(map);
            pojo.append(items);
            opt.add(pojo);
        }
    }

}
