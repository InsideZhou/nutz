package org.nutz.orm.impl.sql.callback;

import org.nutz.orm.entity.Entity;
import org.nutz.orm.pager.ResultSetLooping;
import org.nutz.orm.sql.SqlContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class QueryEntityCallback extends EntityCallback {
    protected String prefix;

    public QueryEntityCallback() {}

    public QueryEntityCallback(String prefix) {
        this.prefix = prefix;
    }

    @Override
    protected Object process(final ResultSet rs, final Entity<?> entity, final SqlContext context)
        throws SQLException {
        ResultSetLooping ing = new ResultSetLooping() {
            protected boolean createObject(int index, ResultSet rs, SqlContext context, int rowCount) {
                list.add(entity.getObject(rs, context.getFieldMatcher(), prefix));
                return true;
            }
        };
        ing.doLoop(rs, context);
        return ing.getList();
    }
}
