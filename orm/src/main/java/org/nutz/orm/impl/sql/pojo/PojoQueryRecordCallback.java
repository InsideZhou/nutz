package org.nutz.orm.impl.sql.pojo;

import org.nutz.orm.entity.Record;
import org.nutz.orm.pager.ResultSetLooping;
import org.nutz.orm.sql.Pojo;
import org.nutz.orm.sql.PojoCallback;
import org.nutz.orm.sql.SqlContext;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PojoQueryRecordCallback implements PojoCallback {

    public Object invoke(Connection conn, ResultSet rs, Pojo pojo) throws SQLException {
        ResultSetLooping ing = new ResultSetLooping() {
            protected boolean createObject(int index, ResultSet rs, SqlContext context, int rowCount) {
                list.add(Record.create(rs));
                return true;
            }
        };
        ing.doLoop(rs, pojo.getContext());
        return ing.getList();
    }

}
