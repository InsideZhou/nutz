package org.nutz.orm.impl.sql.pojo;

import org.nutz.orm.entity.Entity;
import org.nutz.orm.entity.MappingField;
import org.nutz.orm.util.Pojos;

import java.util.List;

public class InsertFieldsPItem extends NoParamsPItem {

    public void joinSql(Entity<?> en, StringBuilder sb) {
        List<MappingField> mfs = Pojos.getFieldsForInsert(_en(en), getFieldMatcher());

        sb.append('(');
        for (MappingField mf : mfs)
            sb.append(mf.getColumnName()).append(',');

        sb.setCharAt(sb.length() - 1, ')');
        sb.append(' ');
    }

}
