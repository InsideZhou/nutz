package org.nutz.orm.impl.entity.info;

import org.nutz.orm.entity.annotation.*;

public class TableInfo {

    public Table annTable;

    public View annView;

    public TableMeta annMeta;

    public PK annPK;

    public TableIndexes annIndexes;

    public Comment tableComment;

}
