package org.nutz.orm.impl.sql.pojo;

import org.nutz.orm.sql.Pojo;
import org.nutz.orm.sql.PojoCallback;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PojoFetchEntityCallback implements PojoCallback {
    protected String prefix;

    public PojoFetchEntityCallback() {}

    public PojoFetchEntityCallback(String prefix) {
        this.prefix = prefix;
    }

    public Object invoke(Connection conn, ResultSet rs, Pojo pojo) throws SQLException {
        if (null != rs && rs.next())
            return pojo.getEntity().getObject(rs, pojo.getContext().getFieldMatcher(), prefix);
        return null;
    }
}
