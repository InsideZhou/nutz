package org.nutz.orm.impl;

import org.nutz.orm.sql.DaoStatement;

import java.sql.Connection;

/**
 * Dao 语句执行器
 * <p>
 * 这个类负责具体执行一个 Dao 语句，并负责打印 Log 等 ...
 * 
 * @author zozoh(zozohtnt@gmail.com)
 */
public interface DaoExecutor {

    void exec(Connection conn, DaoStatement st);

}
