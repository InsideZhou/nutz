package org.nutz.orm.impl.sql;

import org.nutz.orm.sql.VarIndex;
import org.nutz.lang.util.LinkedIntArray;

import java.util.*;

class VarIndexImpl implements VarIndex {

    private Map<String, LinkedIntArray> indexes;

    private Map<Integer, String> names;

    private ArrayList<String> orders;

    VarIndexImpl() {
        indexes = new LinkedHashMap<>();
        names = new LinkedHashMap<>();
        orders = new ArrayList<>();
    }

    public Map<String, LinkedIntArray> getName2IndexMap() {
        return this.indexes;
    }

    public Map<Integer, String> getIndex2NameMap() {
        return this.names;
    }

    void add(String name, int index) {
        LinkedIntArray lia = indexes.get(name);
        if (null == lia) {
            lia = new LinkedIntArray();
            indexes.put(name, lia);
        }
        lia.push(index);
        names.put(index, name);
        orders.add(name);
    }

    Collection<LinkedIntArray> values() {
        return indexes.values();
    }

    public int[] getOrderIndex(String name) {
        LinkedIntArray re = new LinkedIntArray(orders.size());
        int i = 0;
        for (String od : orders) {
            if (od.equals(name))
                re.push(i);
            i++;
        }
        return re.toArray();
    }

    public List<String> getOrders() {
        return orders;
    }

    public String getOrderName(int i) {
        return orders.get(i);
    }

    public String nameOf(int i) {
        return names.get(i);
    }

    public int[] indexesOf(String name) {
        LinkedIntArray lia = indexes.get(name);
        if (null == lia)
            return null;
        return lia.toArray();
    }

    public Set<String> names() {
        return indexes.keySet();
    }

    public int size() {
        return indexes.size();
    }
}
