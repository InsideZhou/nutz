package org.nutz.orm.impl.sql;

import org.nutz.castor.Castors;
import org.nutz.orm.Sqls;
import org.nutz.orm.entity.Entity;
import org.nutz.orm.sql.DaoStatement;
import org.nutz.orm.sql.SqlContext;
import org.nutz.orm.sql.SqlType;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.util.List;

public abstract class NutStatement implements DaoStatement {

    private Entity<?> entity;

    private SqlContext context;

    private SqlType sqlType;

    private boolean forceExecQuery;

    public NutStatement() {
        this.context = new SqlContext();
    }

    public boolean isSelect() {
        return SqlType.SELECT == sqlType;
    }

    public boolean isUpdate() {
        return SqlType.UPDATE == sqlType;
    }

    public boolean isDelete() {
        return SqlType.DELETE == sqlType;
    }

    public boolean isInsert() {
        return SqlType.INSERT == sqlType;
    }

    public boolean isCreate() {
        return SqlType.CREATE == sqlType;
    }

    public boolean isDrop() {
        return SqlType.DROP == sqlType;
    }

    public boolean isRun() {
        return SqlType.RUN == sqlType;
    }

    public boolean isAlter() {
        return SqlType.ALTER == sqlType;
    }

    public boolean isExec() {
        return SqlType.EXEC == sqlType;
    }

    public boolean isCall() {
        return SqlType.CALL == sqlType;
    }

    public boolean isOther() {
        return SqlType.OTHER == sqlType;
    }

    public Entity<?> getEntity() {
        return entity;
    }

    public DaoStatement setEntity(Entity<?> entity) {
        this.entity = entity;
        return this;
    }

    public SqlContext getContext() {
        return context;
    }

    public void setContext(SqlContext context) {
        this.context = context;
    }

    public SqlType getSqlType() {
        return sqlType;
    }

    public DaoStatement setSqlType(SqlType sqlType) {
        this.sqlType = sqlType;
        return this;
    }

    public Object getResult() {
        return context.getResult();
    }

    // TODO 是不是太暴力了涅~~~ --> 不是一般的暴力!!
    @SuppressWarnings("unchecked")
    public <T> List<T> getList(Class<T> classOfT) {
        return (List<T>) getResult();// TODO 考虑先遍历转换一次
    }

    public <T> T getObject(Class<T> classOfT) {
        return Castors.me().castTo(getResult(), classOfT);
    }

    public int getInt() {
        return getObject(Integer.class);
    }

    public Number getNumber() {
        return getObject(Number.class);
    }

    public String getString() {
        return getObject(String.class);
    }

    public boolean getBoolean() {
        return getObject(Boolean.class);
    }

    public int getUpdateCount() {
        return context.getUpdateCount();
    }

    public String toString() {
        String sql = this.toPreparedStatement();
        StringBuilder sb = new StringBuilder(sql);
        sb.append('\n');

        for (Object[] row : this.getParamMatrix()) {
            for (Object param : row) {
                sb.append(param2String(param));
                sb.append(",");
            }
            sb.delete(sb.length() - 1, sb.length());
            sb.append('\n');
        }
        sb.deleteCharAt(sb.length() - 1);

        return sb.toString();
    }

    protected String toStatement(Object[][] mtrx, String sql) {
        StringBuilder sb = new StringBuilder();
        String[] ss = sql.split("[?]");
        int i = 0;
        if (mtrx.length > 0) {
            for (; i < mtrx[0].length; i++) {
                sb.append(ss[i]);
                sb.append(Sqls.formatFieldValue(mtrx[0][i]));
            }
        }
        for (; i < ss.length; i++) {
            sb.append(ss[i]);
        }
        return sb.toString();
    }

    protected String param2String(Object obj) {
        if (obj == null)
            return "NULL";
        else {
            if (obj instanceof Blob) {
                Blob blob = (Blob) obj;
                return "Blob(" + blob.hashCode() + ")";
            }
            else if (obj instanceof Clob) {
                Clob clob = (Clob) obj;
                return "Clob(" + clob.hashCode() + ")";
            }
            else if (obj instanceof byte[] || obj instanceof char[]) {
                if (Array.getLength(obj) > 10240)
                    return "*BigData[len=" + Array.getLength(obj) + "]";
            }
            else if (obj instanceof InputStream) {
                try {
                    return "*InputStream[len=" + ((InputStream) obj).available() + "]";
                }
                catch (IOException ignored) {}
            }
            else if (obj instanceof Reader) {
                return "*Reader@" + obj.hashCode();
            }
            else if (obj instanceof String) {
                return "'" + obj + "'";
            }
            return Castors.me().castToString(obj); // TODO 太长的话,应该截取一部分
        }
    }

    public void forceExecQuery() {
        this.forceExecQuery = true;
    }

    public boolean isForceExecQuery() {
        return forceExecQuery;
    }
}
