package org.nutz.orm.impl.sql.callback;

import org.nutz.orm.sql.Sql;
import org.nutz.orm.sql.SqlCallback;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FetchStringCallback implements SqlCallback {

    public Object invoke(Connection conn, ResultSet rs, Sql sql) throws SQLException {
        if (null != rs && rs.next())
            return rs.getString(1);
        return null;
    }

}
