package org.nutz.orm.impl.sql.callback;

import org.nutz.orm.sql.Sql;
import org.nutz.orm.sql.SqlCallback;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FetchLongCallback implements SqlCallback {

    public Object invoke(Connection conn, ResultSet rs, Sql sql) throws SQLException {
        if (null != rs && rs.next())
            return rs.getLong(1);
        return null;
    }

}
