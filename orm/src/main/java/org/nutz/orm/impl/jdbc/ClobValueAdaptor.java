package org.nutz.orm.impl.jdbc;

import org.nutz.orm.util.blob.SimpleClob;
import org.nutz.filepool.FilePool;
import org.nutz.lang.Streams;

import java.io.File;
import java.sql.*;

public class ClobValueAdaptor extends AbstractFileValueAdaptor {

    public ClobValueAdaptor(FilePool pool) {
        super(pool);
        suffix = ".clob";
    }

    public Object get(ResultSet rs, String colName) throws SQLException {
        File f = this.createTempFile();
        Clob clob = rs.getClob(colName);
        if (clob == null)
            return null;
        Streams.writeAndClose(Streams.fileOutw(f), clob.getCharacterStream());
        return new SimpleClob(f);
    }

    public void set(PreparedStatement stat, Object obj, int i) throws SQLException {
        if (null == obj) {
            stat.setNull(i, Types.CLOB);
        } else {
            Clob clob = (Clob) obj;
            stat.setClob(i, clob);
        }
    }

}
