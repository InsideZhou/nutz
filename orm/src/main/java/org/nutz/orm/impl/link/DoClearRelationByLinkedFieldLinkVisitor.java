package org.nutz.orm.impl.link;

import org.nutz.lang.Lang;
import org.nutz.orm.entity.LinkField;
import org.nutz.orm.impl.AbstractLinkVisitor;
import org.nutz.orm.impl.entity.field.ManyManyLinkField;
import org.nutz.orm.sql.Pojo;
import org.nutz.orm.util.Pojos;
import org.nutz.lang.Each;
import org.nutz.lang.ExitLoop;
import org.nutz.lang.LoopException;

/**
 * 在中间表中，清除关于所有的链接对象的映射。
 * <p>
 * 即，如果 A-X, B-X， 因为链接对象是 X，它会将这两个关系都清除
 * 
 * @author zozoh(zozohtnt@gmail.com)
 */
public class DoClearRelationByLinkedFieldLinkVisitor extends AbstractLinkVisitor {

    public void visit(Object obj, LinkField lnk) {
        if (lnk instanceof ManyManyLinkField) {
            final ManyManyLinkField mm = (ManyManyLinkField) lnk;
            Object value = mm.getValue(obj);
            if (Lang.length(value) == 0)
                return;

            final Pojo pojo = opt.maker().makeDelete(mm.getRelationName());
            pojo.append(Pojos.Items.cndColumn(mm.getToColumnName(), mm.getLinkedField(), null));

            Lang.each(value, new Each<Object>() {
                public void invoke(int i, Object ele, int length) throws ExitLoop, LoopException {
                    pojo.addParamsBy(mm.getLinkedField().getValue(ele));
                }
            });

            opt.add(pojo);
        }
    }

}
