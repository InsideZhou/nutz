package org.nutz.orm.impl.sql.callback;

import org.nutz.orm.entity.Entity;
import org.nutz.orm.sql.SqlContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FetchEntityCallback extends EntityCallback {
    protected String prefix;

    public FetchEntityCallback() {}

    public FetchEntityCallback(String prefix) {
        this.prefix = prefix;
    }

    protected Object process(ResultSet rs, Entity<?> entity, SqlContext context) throws SQLException {
        if (null == rs || !rs.next()) return null;
        return entity.getObject(rs, context.getFieldMatcher(), prefix);
    }
}
