package org.nutz.orm.impl.sql.pojo;

import org.nutz.orm.Condition;
import org.nutz.orm.entity.Entity;
import org.nutz.orm.util.Pojos;

public class ConditionPItem extends NoParamsPItem {

    private Condition cnd;

    public ConditionPItem(Condition cnd) {
        this.cnd = cnd;
    }

    public void joinSql(Entity<?> en, StringBuilder sb) {
        if (null != cnd) {
            sb.append(' ').append(Pojos.formatCondition(en, cnd));
        }
    }

}
