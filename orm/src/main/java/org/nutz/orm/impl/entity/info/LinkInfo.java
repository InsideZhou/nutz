package org.nutz.orm.impl.entity.info;

import org.nutz.orm.entity.annotation.Comment;
import org.nutz.orm.entity.annotation.Many;
import org.nutz.orm.entity.annotation.ManyMany;
import org.nutz.orm.entity.annotation.One;

public class LinkInfo extends FieldInfo {

    public One one;

    public Many many;

    public ManyMany manymany;

    public Comment comment;

}
