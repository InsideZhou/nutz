package org.nutz.orm.impl;

import org.nutz.orm.entity.LinkVisitor;

public abstract class AbstractLinkVisitor implements LinkVisitor {

    protected EntityOperator opt;

    public LinkVisitor opt(EntityOperator opt) {
        this.opt = opt;
        return this;
    }

}
