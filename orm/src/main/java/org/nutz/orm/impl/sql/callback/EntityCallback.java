package org.nutz.orm.impl.sql.callback;

import org.nutz.orm.FieldFilter;
import org.nutz.orm.FieldMatcher;
import org.nutz.orm.entity.Entity;
import org.nutz.orm.sql.Sql;
import org.nutz.orm.sql.SqlCallback;
import org.nutz.orm.sql.SqlContext;
import org.nutz.lang.Lang;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class EntityCallback implements SqlCallback {

    public Object invoke(Connection conn, ResultSet rs, Sql sql) throws SQLException {
        Entity<?> en = sql.getEntity();
        if (null == en)
            throw Lang.makeThrow("SQL without entity : %s", sql.toString());
        FieldMatcher fmh = sql.getContext().getFieldMatcher();
        if (null == fmh)
            sql.getContext().setFieldMatcher(FieldFilter.get(en.getType()));
        return process(rs, en, sql.getContext());
    }

    protected abstract Object process(ResultSet rs, Entity<?> entity, SqlContext context)
            throws SQLException;
}
