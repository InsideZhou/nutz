package org.nutz.orm.impl.entity.info;

import org.nutz.lang.Lang;
import org.nutz.lang.Mirror;
import org.nutz.orm.entity.annotation.*;

public class MappingInfo extends FieldInfo {

    public PK annPK;

    public Column annColumn;

    public ColDefine annDefine;

    public Default annDefault;

    public Id annId;

    public Name annName;

    public BeforeInsert annBeforeInsert;

    public AfterInsert annAfterInsert;

    public BeforeUpdate annBeforeUpdate;

    public AfterUpdate annAfterUpdate;

    public BeforeDelete annBeforeDelete;

    public AfterDelete annAfterDelete;

    public Readonly annReadonly;

    public Comment columnComment;

    public Class<?> getFieldTypeClass() {
        return Lang.getTypeClass(fieldType);
    }

    public Mirror<?> getFieldTypeMirror() {
        return Mirror.me(getFieldTypeClass());
    }

}
