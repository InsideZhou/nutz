package org.nutz.orm.impl.sql.pojo;

import org.nutz.orm.pager.ResultSetLooping;
import org.nutz.orm.sql.Pojo;
import org.nutz.orm.sql.PojoCallback;
import org.nutz.orm.sql.SqlContext;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PojoQueryEntityCallback implements PojoCallback {
    protected String prefix;

    public PojoQueryEntityCallback() {}

    public PojoQueryEntityCallback(String prefix) {
        this.prefix = prefix;
    }

    public Object invoke(Connection conn, ResultSet rs, final Pojo pojo) throws SQLException {
        ResultSetLooping ing = new ResultSetLooping() {
            protected boolean createObject(int index, ResultSet rs, SqlContext context, int rowCount) {
                list.add(pojo.getEntity().getObject(rs, context.getFieldMatcher(), prefix));
                return true;
            }
        };
        ing.doLoop(rs, pojo.getContext());
        return ing.getList();
    }
}
