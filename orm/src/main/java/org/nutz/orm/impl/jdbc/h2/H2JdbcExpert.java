package org.nutz.orm.impl.jdbc.h2;

import org.nutz.orm.DB;
import org.nutz.orm.entity.Entity;
import org.nutz.orm.entity.MappingField;
import org.nutz.orm.impl.entity.macro.SqlMacro;
import org.nutz.orm.impl.jdbc.psql.PsqlJdbcExpert;
import org.nutz.orm.jdbc.JdbcExpertConfigFile;
import org.nutz.orm.sql.Pojo;

public class H2JdbcExpert extends PsqlJdbcExpert {

    public H2JdbcExpert(JdbcExpertConfigFile conf) {
        super(conf);
    }

    public String getDatabaseType() {
        return DB.H2.name();
    }

    public Pojo fetchPojoId(Entity<?> en, MappingField idField) {
        String autoSql = "SELECT IDENTITY() as $field from $view";
        Pojo autoInfo = new SqlMacro(en, idField, autoSql);
        autoInfo.setEntity(en);
        return autoInfo;
    }
}
