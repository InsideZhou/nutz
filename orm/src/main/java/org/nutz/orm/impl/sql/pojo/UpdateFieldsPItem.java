package org.nutz.orm.impl.sql.pojo;

import org.nutz.orm.entity.Entity;
import org.nutz.orm.entity.MappingField;
import org.nutz.orm.util.Pojos;

import java.util.List;

public class UpdateFieldsPItem extends InsertValuesPItem {

    /**
     * 参考对象，根据这个对象来决定是否忽略空值
     */
    private Object refer;

    public UpdateFieldsPItem(Object refer) {
        this.refer = refer;
    }

    protected List<MappingField> _mfs(Entity<?> en) {
        if (null == mfs)
        	return Pojos.getFieldsForUpdate(_en(en), getFieldMatcher(), refer == null ? pojo.getOperatingObject() : refer);
        return mfs;
    }

    public void joinSql(Entity<?> en, StringBuilder sb) {
        List<MappingField> mfs = _mfs(en);

        sb.append(" SET ");
        for (MappingField mf : mfs)
            sb.append(mf.getColumnName()).append("=?,");

        sb.setCharAt(sb.length() - 1, ' ');
    }

}
