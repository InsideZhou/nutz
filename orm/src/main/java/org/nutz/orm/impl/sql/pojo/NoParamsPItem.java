package org.nutz.orm.impl.sql.pojo;

import org.nutz.orm.entity.Entity;
import org.nutz.orm.jdbc.ValueAdaptor;

public abstract class NoParamsPItem extends AbstractPItem {

    private static final String[] re = new String[0];

    public String[] getParamNames() {
        return re;
    }

    public int joinAdaptor(Entity<?> en, ValueAdaptor[] adaptors, int off) {
        return off;
    }

    public int joinParams(Entity<?> en, Object obj, Object[] params, int off) {
        return off;
    }

    public int paramCount(Entity<?> en) {
        return 0;
    }

}
