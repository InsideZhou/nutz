package org.nutz.orm.impl;

import org.nutz.lang.Lang;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.resource.NutResource;
import org.nutz.resource.Scans;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class FileSqlManager extends AbstractSqlManager {
    
    private static final Log log = Logs.getLog(FileSqlManager.class);
    
    private Object lock = new Object();

    private String[] paths;

    private String regex;

    public FileSqlManager(String... paths) {
        this.paths = paths;
    }

    public String getRegex() {
        return regex;
    }

    public FileSqlManager setRegex(String regex) {
        this.regex = regex;
        return this;
    }

    public void refresh() {
        synchronized (lock) {
			List<NutResource> list = Scans.me().loadResource(regex, paths);
			_sql_map = new HashMap<String, String>();
			for (NutResource ins : list) {
                log.debug("Loading sqls from " + ins);
				try {
					loadSQL(ins.getReader());
				}
				catch (IOException e) {
					throw Lang.wrapThrow(e);
				}
			}
		}
    }

}
