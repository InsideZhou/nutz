package org.nutz.orm.impl.sql.pojo;

import org.nutz.lang.Lang;
import org.nutz.orm.FieldMatcher;
import org.nutz.orm.entity.Entity;
import org.nutz.orm.entity.MappingField;

import java.util.List;

public class QueryEntityFieldsPItem extends NoParamsPItem {

    public void joinSql(Entity<?> en, StringBuilder sb) {
        FieldMatcher fm = getFieldMatcher();
        if (null == fm) {
            sb.append("* ");
        } else {
            List<MappingField> efs = _en(en).getMappingFields();

            int old = sb.length();

            for (MappingField ef : efs) {
                if (fm.match(ef.getName()))
                    sb.append(ef.getColumnName()).append(',');
            }

            if (sb.length() == old)
                throw Lang.makeThrow("No columns be queryed: '%s'", _en(en));

            sb.setCharAt(sb.length() - 1, ' ');
        }
    }

}
