package org.nutz.orm;

import org.nutz.lang.Strings;

import java.util.regex.Pattern;

/**
 * 字段匹配器
 *
 * @author zozoh(zozohtnt@gmail.com)
 */
public class FieldMatcher {
    public static FieldMatcher active(String active) {
        return make(active, null, false);
    }

    public static FieldMatcher active(String active, boolean ignoreNull) {
        return make(active, null, ignoreNull);
    }

    public static FieldMatcher lock(String locked) {
        return make(null, locked, false);
    }

    public static FieldMatcher lock(String locked, boolean ignoreNull) {
        return make(null, locked, ignoreNull);
    }

    public static FieldMatcher ignoreNull() {
        return make(null, null, true);
    }

    public static FieldMatcher make(String active, String locked) {
        return make(active, locked, false);
    }

    public static FieldMatcher make(String active, String locked, boolean ignoreNull) {
        FieldMatcher fm = new FieldMatcher();
        fm.ignoreNull = ignoreNull;
        if (!Strings.isBlank(active))
            fm.actived = Pattern.compile(active);
        if (!Strings.isBlank(locked))
            fm.locked = Pattern.compile(locked);
        return fm;
    }

    /**
     * 哪些字段可用
     */
    private Pattern actived;
    /**
     * 哪些字段不可用
     */
    private Pattern locked;
    /**
     * 是否忽略空值
     */
    private boolean ignoreNull;

    public boolean match(String str) {
        if (null != locked)
            if (locked.matcher(str).find())
                return false;
        if (null != actived)
            if (!actived.matcher(str).find())
                return false;
        return true;
    }

    public boolean isIgnoreNull() {
        return ignoreNull;
    }

    public void setIgnoreNull(boolean ignoreNull) {
        this.ignoreNull = ignoreNull;
    }

    public Pattern getActived() {
        return actived;
    }

    public Pattern getLocked() {
        return locked;
    }

    public void setActived(Pattern actived) {
        this.actived = actived;
    }

    public void setLocked(Pattern locked) {
        this.locked = locked;
    }

}
