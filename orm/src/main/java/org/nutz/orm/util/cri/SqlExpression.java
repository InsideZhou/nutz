package org.nutz.orm.util.cri;

import org.nutz.orm.sql.PItem;

public interface SqlExpression extends PItem{

    SqlExpression setNot(boolean not);

}
