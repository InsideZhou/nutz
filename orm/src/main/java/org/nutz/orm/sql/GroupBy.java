package org.nutz.orm.sql;

import org.nutz.orm.Condition;

public interface GroupBy extends OrderBy {

	GroupBy groupBy(String... names);
	
	GroupBy having(Condition cnd);
}
