package org.nutz.orm.sql;

import org.nutz.orm.Condition;
import org.nutz.orm.pager.Pager;
import org.nutz.orm.util.cri.SqlExpressionGroup;

/**
 * 这个接口是对 Condition 接口进行扩充，主要为了能够更好的利用 PreparedStatement
 * 
 * @author zozoh(zozohtnt@gmail.com)
 */
public interface Criteria extends Condition, PItem {

    SqlExpressionGroup where();

    OrderBy getOrderBy();

    Pager getPager();
}
