package org.nutz.orm.sql;

import org.nutz.orm.Condition;

public interface OrderBy extends Condition,PItem {

    OrderBy asc(String name);

    OrderBy desc(String name);
}
