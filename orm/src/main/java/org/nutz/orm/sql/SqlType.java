package org.nutz.orm.sql;

public enum SqlType {

    SELECT, DELETE, TRUNCATE, UPDATE, INSERT, CREATE, DROP, RUN, ALTER, EXEC, CALL, OTHER

}
