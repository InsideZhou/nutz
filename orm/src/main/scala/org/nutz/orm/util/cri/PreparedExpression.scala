package org.nutz.orm.util.cri

import java.lang.StringBuilder

import org.nutz.orm.entity.Entity
import org.nutz.orm.impl.sql.pojo.AbstractPItem
import org.nutz.orm.jdbc.{ValueAdaptor, Jdbcs}

import scala.collection.mutable

class PreparedExpression(exp: String) extends AbstractPItem with SqlExpression {
    override def setNot(not: Boolean): SqlExpression = this

    override def joinSql(en: Entity[_], sb: StringBuilder): Unit = {
        sb.append(' ').append(exp).append(' ')
    }

    override def paramCount(en: Entity[_]): Int = parameters.length

    override def joinAdaptor(en: Entity[_], adaptors: Array[ValueAdaptor], off: Int): Int = {
        var index = off
        parameters.foreach(p => {
            adaptors.update(index, Jdbcs.getAdaptorBy(p))
            index += 1
        })
        
        index
    }

    override def joinParams(en: Entity[_], obj: scala.Any, params: Array[AnyRef], off: Int): Int = {
        var index = off
        parameters.foreach(p => {
            params.update(index, p.asInstanceOf[AnyRef])
            index += 1
        })

        index
    }

    private val parameters = mutable.Buffer[Any]()

    def addParam[T](en: T) = {
        parameters += en
        this
    }

    def setParam[T](index: Int, en: T) = {
        parameters.update(index, en)
        this
    }
}
