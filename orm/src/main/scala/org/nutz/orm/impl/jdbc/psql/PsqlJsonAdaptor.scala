package org.nutz.orm.impl.jdbc.psql

import java.sql.{PreparedStatement, ResultSet, Types}

import org.nutz.json.{JsonFormat, Json}
import org.nutz.lang.Mirror
import org.nutz.orm.jdbc.ValueAdaptor

class PsqlJsonAdaptor[T](mirror: Mirror[T]) extends ValueAdaptor {
    override def get(rs: ResultSet, colName: String): AnyRef = {
        val json = rs.getString(colName)
        if (null == json) {
            null
        }
        else {
            Json.fromJson(mirror.getActuallyType, rs.getString(colName))
        }
    }

    override def set(stat: PreparedStatement, obj: scala.Any, index: Int): Unit = obj match {
        case json: String => stat.setObject(index, json, Types.OTHER)
        case null => stat.setNull(index, Types.NULL)
        case any => stat.setObject(index, Json.toJson(any, JsonFormat.compact().setIgnoreNull(false)), Types.OTHER)
    }
}
