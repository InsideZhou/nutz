package org.nutz.orm.impl

import javax.sql.DataSource

import org.nutz.orm.{SqlManager, Cnd, OrmSettings, DB}
import org.nutz.orm.util.cri._

class FlagsDao(ds: DataSource, sqlMgr: SqlManager) extends NutDao(ds, sqlMgr) {
    def this(ds: DataSource) = this(ds, null)

    def this() = this(null, null)

    def exps(exp: SqlExpression): SqlExpressionGroup = {
        new SqlExpressionGroup().and(readFlag).and(exp)
    }

    def exps(name: String, op: String, value: Any): SqlExpressionGroup = {
        exps(Exps.create(name, op, value))
    }

    def where(exp: SqlExpression): Cnd = {
        Cnd.NEW().and(readFlag).and(exp)
    }

    def where(name: String, op: String, value: Any): Cnd = {
        where(Exps.create(name, op, value))
    }

    def cri = {
        val cri = new SimpleCriteria()
        cri.where().and(readFlag)
        cri
    }

    def orderBy = {
        Cnd.NEW().and(readFlag)
    }

    def flags(flags: Int): SqlExpression = {
        super.meta().getType match {
            case DB.MYSQL =>
                new Static(s"${OrmSettings.flagColumnName}&$flags=$flags")
            case DB.PSQL =>
                new Static(s"${OrmSettings.flagColumnName}&$flags=$flags")
            case DB.H2 =>
                new Static(s"BITAND(${OrmSettings.flagColumnName},$flags)=$flags")
            case DB.ORACLE =>
                new Static(s"BITAND(${OrmSettings.flagColumnName},$flags)=$flags")
            case _ =>
                throw new NotImplementedError
        }
    }

    def flags(flagColumnName: String, flags: Int): SqlExpression = {
        super.meta().getType match {
            case DB.MYSQL =>
                new Static(s"$flagColumnName&$flags=$flags")
            case DB.PSQL =>
                new Static(s"$flagColumnName&$flags=$flags")
            case DB.H2 =>
                new Static(s"BITAND($flagColumnName,$flags)=$flags")
            case DB.ORACLE =>
                new Static(s"BITAND($flagColumnName,$flags)=$flags")
            case _ =>
                throw new NotImplementedError
        }
    }

    def noFlags(flags: Int): SqlExpression = {
        super.meta().getType match {
            case DB.MYSQL =>
                new Static(s"${OrmSettings.flagColumnName}&$flags<>$flags")
            case DB.PSQL =>
                new Static(s"${OrmSettings.flagColumnName}&$flags<>$flags")
            case DB.H2 =>
                new Static(s"BITAND(${OrmSettings.flagColumnName},$flags)<>$flags")
            case DB.ORACLE =>
                new Static(s"BITAND(${OrmSettings.flagColumnName},$flags)<>$flags")
            case _ =>
                throw new NotImplementedError
        }
    }

    def noflags(flagColumnName: String, flags: Int): SqlExpression = {
        super.meta().getType match {
            case DB.MYSQL =>
                new Static(s"$flagColumnName&$flags<>$flags")
            case DB.PSQL =>
                new Static(s"$flagColumnName&$flags<>$flags")
            case DB.H2 =>
                new Static(s"BITAND($flagColumnName,$flags)<>$flags")
            case DB.ORACLE =>
                new Static(s"BITAND($flagColumnName,$flags)<>$flags")
            case _ =>
                throw new NotImplementedError
        }
    }

    def readFlag: SqlExpression = {
        flags(OrmSettings.ReadFlag)
    }

    def writeFlag: SqlExpression = {
        flags(OrmSettings.WriteFlag)
    }

    def readWriteFlag: SqlExpression = {
        flags(OrmSettings.ReadFlag | OrmSettings.WriteFlag)
    }
}
