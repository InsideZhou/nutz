package org.nutz.orm

object OrmSettings {
    val ReadFlag = 0x1
    val WriteFlag = 0x2

    var flagColumnName = "flags"
}
