package org.nutz.web.freemarker;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

import java.io.IOException;
import java.util.Map;

public class OverrideDirective implements TemplateDirectiveModel {
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
        String name = DirectiveUtils.getRequiredParam(params, "name");
        String overrideVariableName = DirectiveUtils.getOverrideVariableName(name);

        TemplateDirectiveBodyOverrideWrapper override = DirectiveUtils.getOverrideBody(env, name);
        TemplateDirectiveBodyOverrideWrapper current = new TemplateDirectiveBodyOverrideWrapper(body, env);

        if (override == null) {
            env.setVariable(overrideVariableName, current);
        }
        else {
            DirectiveUtils.setTopBodyForParentBody(current, override);
        }
    }
}
