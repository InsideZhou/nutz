package org.nutz.web.freemarker;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

import java.io.IOException;
import java.util.Map;

public class BlockDirective implements TemplateDirectiveModel {
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
        throws TemplateException, IOException {

        String name = DirectiveUtils.getRequiredParam(params, "name");
        TemplateDirectiveBodyOverrideWrapper overrideBody = DirectiveUtils.getOverrideBody(env, name);

        if (overrideBody == null) {
            if (body != null) {
                body.render(env.getOut());
            }
        }
        else {
            DirectiveUtils.setTopBodyForParentBody(new TemplateDirectiveBodyOverrideWrapper(body, env), overrideBody);
            overrideBody.render(env.getOut());
        }
    }
}
