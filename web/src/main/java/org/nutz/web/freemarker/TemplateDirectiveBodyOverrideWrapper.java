package org.nutz.web.freemarker;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

import java.io.IOException;
import java.io.Writer;

public class TemplateDirectiveBodyOverrideWrapper implements TemplateDirectiveBody, TemplateModel {
    private TemplateDirectiveBody body;
    public TemplateDirectiveBodyOverrideWrapper parentBody;
    public Environment env;

    public TemplateDirectiveBodyOverrideWrapper(TemplateDirectiveBody body, Environment env) {
        super();
        this.body = body;
        this.env = env;
    }

    public void render(Writer out) throws TemplateException, IOException {
        if (body == null) return;

        TemplateDirectiveBodyOverrideWrapper preOverride = (TemplateDirectiveBodyOverrideWrapper) env.getVariable(DirectiveUtils.OVERRIDE_CURRENT_NODE);
        try {
            env.setVariable(DirectiveUtils.OVERRIDE_CURRENT_NODE, this);
            body.render(out);
        } finally {
            env.setVariable(DirectiveUtils.OVERRIDE_CURRENT_NODE, preOverride);
        }
    }
}
