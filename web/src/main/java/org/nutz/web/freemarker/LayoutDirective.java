package org.nutz.web.freemarker;

import freemarker.template.SimpleHash;

public class LayoutDirective extends SimpleHash {
    public final static String BLOCK_DIRECTIVE = "block";
    public final static String EXTENDS_DIRECTIVE = "extends";
    public final static String OVERRIDE_DIRECTIVE = "override";
    public final static String SUPER_DIRECTIVE = "super";

    public LayoutDirective() {
        put(BLOCK_DIRECTIVE, new BlockDirective());
        put(EXTENDS_DIRECTIVE, new ExtendsDirective());
        put(OVERRIDE_DIRECTIVE, new OverrideDirective());
        put(SUPER_DIRECTIVE, new SuperDirective());
    }
}
