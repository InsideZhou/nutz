package org.nutz.web.freemarker;

import freemarker.core.Environment;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModelException;

import java.util.Map;

public class DirectiveUtils {
    public final static String OVERRIDE_CURRENT_NODE = "__ftl_override_current_node";

    public static String getOverrideVariableName(String name) {
        return "__ftl_override__" + name;
    }

    public static String getRequiredParam(Map params, String key) throws TemplateException {
        Object value = params.get(key);

        if (value == null || value.toString().isEmpty()) {
            throw new TemplateModelException("not found required parameter:" + key + " for directive");
        }

        return value.toString();
    }

    public static String getParam(Map params, String key, String defaultValue) throws TemplateException {
        Object value = params.get(key);
        return value == null ? defaultValue : value.toString();
    }

    public static TemplateDirectiveBodyOverrideWrapper getOverrideBody(Environment env, String name) throws TemplateModelException {
        return (TemplateDirectiveBodyOverrideWrapper) env.getVariable(DirectiveUtils.getOverrideVariableName(name));
    }

    public static void setTopBodyForParentBody(
        TemplateDirectiveBodyOverrideWrapper topBody
        , TemplateDirectiveBodyOverrideWrapper overrideBody
    ) {
        TemplateDirectiveBodyOverrideWrapper parent = overrideBody;

        while (parent.parentBody != null) {
            parent = parent.parentBody;
        }

        parent.parentBody = topBody;
    }
}
