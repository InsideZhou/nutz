var chain = {
    default: {
        ps: [
            "org.nutz.web.impl.processor.UpdateRequestAttributesProcessor",
            "org.nutz.web.impl.processor.EncodingProcessor",
            "org.nutz.web.impl.processor.ModuleProcessor",
            'org.nutz.web.impl.processor.ShiroProcessor',
            "org.nutz.web.impl.processor.ActionFiltersProcessor",
            "org.nutz.web.impl.processor.NutAdaptorProcessor",
            "org.nutz.web.impl.processor.MethodInvokeProcessor",
            "org.nutz.web.impl.processor.ViewProcessor"
        ],
        error: 'org.nutz.web.impl.processor.ShiroExceptionProcessor'
    }
};