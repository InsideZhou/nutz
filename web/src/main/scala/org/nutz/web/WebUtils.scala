package org.nutz.web

import java.net.URLDecoder
import javax.servlet.ServletInputStream
import javax.servlet.http.HttpServletRequest

import java.nio.file.{Files => JavaFiles}

import org.nutz.Settings
import org.nutz.lang.{Streams, Strings}
import org.nutz.web.upload.{FieldMeta, TempFile}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object WebUtils {
    def getURLIncludeQuery(req: HttpServletRequest): String = {
        val query = req.getQueryString
        if (null == query || query.isEmpty) {
            req.getRequestURL.toString
        }
        else {
            req.getRequestURL + "?" + query
        }
    }

    def getURIIncludeQuery(req: HttpServletRequest): String = {
        val query = req.getQueryString
        if (null == query || query.isEmpty) {
            req.getRequestURI
        }
        else {
            req.getRequestURI + "?" + query
        }
    }

    def getURIIncludeQueryWithoutContext(req: HttpServletRequest): String = {
        val query = req.getQueryString
        val uri = if (null == query || query.isEmpty) {
            req.getRequestURI
        }
        else {
            req.getRequestURI + "?" + query
        }

        req.getContextPath match {
            case "" => uri
            case any => uri.substring(any.length)
        }
    }

    def getRealIP(req: HttpServletRequest): String = {
        req.getHeader("X-Real-IP") match {
            case s if null == s || s.isEmpty => req.getRemoteAddr
            case ip => ip
        }
    }

    def getHeaderMap(req: HttpServletRequest) = {
        val map = new mutable.HashMap[String, String]
        val names = req.getHeaderNames
        while (names.hasMoreElements) {
            val n = names.nextElement
            map.put(n, req.getHeader(n))
        }

        map.toMap
    }

    def isMultipart(req: HttpServletRequest) = null != req.getContentType && req.getContentType.toLowerCase.startsWith("multipart/")

    def isAjax(req: HttpServletRequest) = "XMLHttpRequest".equalsIgnoreCase(req.getHeader("X-Requested-With"))

    def getQueryParameterMap(req: HttpServletRequest, sep: String = ",", escape: Boolean = false) = {
        val map = new mutable.HashMap[String, Any]

        val queryString = req.getQueryString
        if (null != queryString && !queryString.isEmpty) {
            val qs = URLDecoder.decode(queryString, getEncoding(req))
            if (null == sep) {
                qs.split("&").foreach(item => {
                    val (k, v) = splitParameterItem(item, "=")
                    handleParameterWithoutSep(k, v, map)
                })
            }
            else {
                qs.split("&").foreach(item => {
                    val (k, v) = splitParameterItem(item, "=")
                    handleParameterWithSep(k, v, sep, map)
                })
            }
        }

        if (escape) {
            escapeValues(map.toMap)
        }
        else {
            map.toMap
        }
    }

    def escapeValues(map: Map[String, Any]): Map[String, Any] = {
        map.map {
            case (k: String, v: String) => k -> Strings.escapeHtml(v)
            case (k: String, values: Array[String]) => k -> values.map(Strings.escapeHtml(_))
        }
    }

    def handleParameterWithSep(k: String, v: String, sep: String, map: mutable.HashMap[String, Any]): Unit = {
        map.get(k) match {
            case None => map += k -> v
            case Some(exist: String) =>
                if (null == exist) {
                    map += k -> s"$v"
                }
                else {
                    map += k -> s"$exist$sep$v"
                }
            case _ => throw new AssertionError("代码不应该到达这个地方")
        }
    }

    def handleParameterWithoutSep(k: String, v: String, map: mutable.HashMap[String, Any]): Unit = {
        map.get(k) match {
            case None => map += k -> v
            case Some(exist: String) => map += k -> Array(exist, v)
            case Some(array: Array[String]) => map += k -> (array :+ v)
            case _ => throw new AssertionError("代码不应该到达这个地方")
        }
    }

    def splitParameterItem(item: String, sep: String = "=") = {
        item.split(sep, 2) match {
            case Array(key, value) => (key, value)
            case Array(key) => (key, "")
            case a => throw new AssertionError("代码不该到达这个位置")
        }
    }

    private val CacheKey_GetFormParameterMap = "org.nutz.WebUtils.getFormParameterMap"

    /**
     * 从表单中获取请求参数对象
     */
    def getFormParameterMap(req: HttpServletRequest, sep: String = ",", escape: Boolean = false): Map[String, Any] = {
        val map = req.getAttribute(CacheKey_GetFormParameterMap) match {
            case null => mutable.HashMap[String, Any]()
            //这个地方之所以要写成Map[_, _]为了避免编译警告；因为类型擦除的缘故，编译器无法保证Map中的数据类型。
            case m: Map[_, _] => return m.asInstanceOf[Map[String, _]]
            case _ => throw new AssertionError("代码不应该到达这个地方")
        }

        if (isMultipart(req)) {
            val encoding = getEncoding(req)

            val boundary = req.getContentType.split(';').find(item => item.toLowerCase.contains("boundary=")) match {
                case None => return map.toMap
                case Some(item) => item.split('=')(1).getBytes(encoding)
            }

            val stream = req.getInputStream
            val buffer = new Array[Byte](Settings.IO_BUFFER_SIZE)

            var matching = false //是否已经进入boundary区域
            var headEnded = false //是否已经到达或越过part中header的结束位

            var headers = ""

            val lineEnd = "\r\n".getBytes

            var i = stream.readLine(buffer, 0, buffer.length)
            while (-1 != i) {
                val b = buffer.take(i)
                if (matching) {
                    if (headEnded) {
                        val meta = new FieldMeta(headers)
                        if (Strings.isNotBlank(meta.getName)) {
                            val name = meta.getName
                            if (meta.isFile) {
                                val tf = new TempFile(meta, parseFilePart(stream, boundary, b))
                                map.get(name) match {
                                    case None => map += name -> tf
                                    case Some(exist: TempFile) => map += name -> Array(exist, tf)
                                    case Some(parts: Array[TempFile]) => map += name -> (parts :+ tf)
                                    case _ => throw new AssertionError("代码不应该到达这个地方")
                                }
                            }
                            else {
                                val v = if (escape) {
                                    Strings.escapeHtml(parseFieldPart(stream, boundary, b, encoding).trim)
                                }
                                else {
                                    parseFieldPart(stream, boundary, b, encoding).trim
                                }

                                if (null == sep) {
                                    handleParameterWithoutSep(name, v, map)
                                }
                                else {
                                    handleParameterWithSep(name, v, sep, map)
                                }
                            }

                            headEnded = false
                        }
                    }
                    else {
                        val currentHeadEnd = java.util.Arrays.equals(b, lineEnd) //当前是否part header的结束位
                        if (currentHeadEnd) {
                            headEnded = currentHeadEnd
                        }
                        else {
                            headers = headers ++: new String(b, encoding)
                        }
                    }
                }
                else {
                    matching = b.containsSlice(boundary)
                }

                i = stream.readLine(buffer, 0, buffer.length)
            }

            val m = map.toMap
            req.setAttribute(CacheKey_GetFormParameterMap, m)
            m
        }
        else if ("POST".equalsIgnoreCase(req.getMethod)) {
            getBodyStr(req) match {
                case null =>
                case s if s.isEmpty =>
                case s =>
                    if (null == sep) {
                        s.split("&").foreach(item => {
                            val (k, v) = splitParameterItem(item)
                            handleParameterWithoutSep(k, v, map)
                        })
                    }
                    else {
                        s.split("&").foreach(item => {
                            val (k, v) = splitParameterItem(item)
                            handleParameterWithSep(k, v, sep, map)
                        })
                    }
            }

            if (escape) {
                escapeValues(map.toMap)
            }
            else {
                map.toMap
            }
        }
        else {
            map.toMap
        }
    }

    private def parseFilePart(stream: ServletInputStream, boundary: Array[Byte], currentBytes: Array[Byte]) = {
        val buffer = new Array[Byte](Settings.IO_BUFFER_SIZE)

        val tmpFile = JavaFiles.createTempFile("nutz-inside-filepart", ".tmp").toFile
        tmpFile.deleteOnExit()
        val fileStream = Streams.fileOut(tmpFile)

        try {
            Streams.write(fileStream, currentBytes)

            var i = stream.readLine(buffer, 0, buffer.length)
            var b = buffer.take(i)
            var nextMatch = b.containsSlice(boundary) //是否匹配part尾部的boundary

            while (!nextMatch) {
                Streams.write(fileStream, b)

                i = stream.readLine(buffer, 0, buffer.length)
                b = buffer.take(i)
                nextMatch = b.containsSlice(boundary)
            }
        }
        finally {
            fileStream.close()
        }

        tmpFile
    }

    private def parseFieldPart(stream: ServletInputStream, boundary: Array[Byte], currentBytes: Array[Byte], encoding: String) = {
        val buffer = new Array[Byte](Settings.IO_BUFFER_SIZE)

        val formData = ArrayBuffer[Byte]()
        formData ++= currentBytes

        var i = stream.readLine(buffer, 0, buffer.length)
        var b = buffer.take(i)
        var nextMatch = b.containsSlice(boundary) //是否匹配part尾部的boundary

        while (!nextMatch) {
            formData ++= b

            i = stream.readLine(buffer, 0, buffer.length)
            b = buffer.take(i)
            nextMatch = b.containsSlice(boundary)
        }

        new String(formData.toArray, encoding)
    }

    def getMIMEType(req: HttpServletRequest) = {
        req.getContentType match {
            case null => ""
            case ct if ct.isEmpty => ""
            case ct => ct.split(";")(0)
        }
    }

    def getEncoding(req: HttpServletRequest, default: String = "UTF-8") = if (null != req.getCharacterEncoding) req.getCharacterEncoding else default

    def getBodyStr(req: HttpServletRequest) = {
        getBodyRawStr(req) match {
            case null => null
            case s => URLDecoder.decode(s, getEncoding(req))
        }
    }

    def getBodyRawStr(req: HttpServletRequest) = {
        if ("POST".equalsIgnoreCase(req.getMethod) && !isMultipart(req)) {
            new String(Streams.readBytes(req.getInputStream), getEncoding(req))
        }
        else {
            null
        }
    }
}
