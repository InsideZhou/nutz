package org.nutz.web.freemarker

import java.io.{File, StringWriter}
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import org.rythmengine.RythmEngine

import scala.beans.BeanProperty
import scala.collection.JavaConversions._
import scala.collection.mutable

abstract class AbstractRythmView {
    @BeanProperty
    var engine: RythmEngine = null

    def getTemplatePath: String

    def doRender(req: HttpServletRequest, resp: HttpServletResponse, obj: scala.Any): Unit = {
        val params = Map(
            "model" -> (obj match {
                case m: scala.collection.Map[_, _] => mapAsJavaMap(m)
                case l: Seq[_] => seqAsJavaList(l)
                case any => any
            }),
            "req" -> req)

        engine.render(resp.getWriter, getTemplatePath, mapAsJavaMap(params))
    }
}

object AbstractRythmView {
    def renderAsString(templatePath: String, obj: scala.Any = null)(implicit req: HttpServletRequest, engine: RythmEngine): String = {
        val model = obj match {
            case m: scala.collection.Map[_, _] => mapAsJavaMap(m)
            case l: Seq[_] => seqAsJavaList(l)
            case any => any
        }

        val params = mutable.Map("model" -> model)

        if (null != req) {
            params += ("req" -> req)
        }

        val sw = new StringWriter
        engine.render(sw, new File(templatePath), mapAsJavaMap(params))

        sw.toString
    }
}
