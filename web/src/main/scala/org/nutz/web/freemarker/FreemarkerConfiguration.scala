package org.nutz.web.freemarker

import freemarker.cache.MruCacheStorage
import freemarker.template.{Configuration, ObjectWrapper, TemplateExceptionHandler}

object FreemarkerConfiguration {
    def getConfig = {
        val c = new Configuration
        c.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER)
        c.setObjectWrapper(ObjectWrapper.BEANS_WRAPPER)
        c.setCacheStorage(new MruCacheStorage(0, 100))
        c.setWhitespaceStripping(true)
        c.setTimeFormat("HH:mm:ss")
        c.setDateFormat("yyyy-MM-dd")
        c.setDateTimeFormat("yyyy-MM-dd HH:mm:ss")
        c.setDefaultEncoding("UTF-8")
        c.setLocalizedLookup(false)
        c.setNumberFormat("0.######")
        c
    }
}
