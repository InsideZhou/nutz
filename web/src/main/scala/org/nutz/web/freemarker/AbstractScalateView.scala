package org.nutz.web.freemarker

import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import org.fusesource.scalate.TemplateEngine

import scala.beans.BeanProperty
import scala.collection.mutable
import scala.xml.NodeSeq

abstract class AbstractScalateView {
    @BeanProperty
    var engine: TemplateEngine = null

    def getTemplatePath: String

    def doRender(req: HttpServletRequest, resp: HttpServletResponse, obj: scala.Any): Unit = {
        val params = Map(
            "model" -> (obj match {
                case m: scala.collection.Map[_, _] => m.toMap
                case l: Seq[_] => l.toList
                case any => any
            }),
            "req" -> req)

        engine.layout(getTemplatePath, resp.getWriter, params)
    }
}

object AbstractScalateView {
    def renderAsNodeSeq(templatePath: String, obj: scala.Any = null)(implicit req: HttpServletRequest, engine: TemplateEngine): NodeSeq = {
        val model = obj match {
            case m: scala.collection.Map[_, _] => m.toMap
            case l: Seq[_] => l.toList
            case any => any
        }

        val params = mutable.Map("model" -> model)

        if (null != req) {
            params += ("req" -> req)
        }

        engine.layoutAsNodes(templatePath, params.toMap)
    }
}
