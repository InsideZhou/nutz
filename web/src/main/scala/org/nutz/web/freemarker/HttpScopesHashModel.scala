package org.nutz.web.freemarker

import javax.servlet.http.HttpServletRequest

import freemarker.template.{ObjectWrapper, SimpleHash, TemplateModel}

class HttpScopesHashModel(wrapper: ObjectWrapper, req: HttpServletRequest) extends SimpleHash {
    override def get(key: String): TemplateModel = {
        val model = super.get(key)
        if (null != model) {
            return model
        }

        val param = req.getParameter(key)
        if (null != param) {
            return wrapper.wrap(param)
        }

        val obj = req.getAttribute(key)
        if (null != obj) {
            return wrapper.wrap(obj)
        }

        val s = req.getSession(false)
        if (null != s) {
            val obj = s.getAttribute(key)
            if (null != obj) {
                return wrapper.wrap(obj)
            }
        }

        wrapper.wrap(null)
    }
}
