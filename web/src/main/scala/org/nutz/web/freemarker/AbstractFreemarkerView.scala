package org.nutz.web.freemarker

import java.io.StringWriter
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

import freemarker.ext.beans.BeansWrapper
import freemarker.template.{Configuration, ObjectWrapper, SimpleHash}
import org.nutz.lang.Strings

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

abstract class AbstractFreemarkerView {
    @BeanProperty
    var contentType: String = ""

    @BeanProperty
    var config: Configuration = null

    def getTemplatePath: String

    def doRender(req: HttpServletRequest, resp: HttpServletResponse, obj: scala.Any): Unit = {
        val template = config.getTemplate(getTemplatePath)

        if (Strings.isBlank(resp.getCharacterEncoding)) {
            resp.setCharacterEncoding(template.getEncoding)
        }

        if (Strings.isBlank(resp.getContentType)) {
            resp.setContentType(
                Strings.sBlank(template.getCustomAttribute("content_type"), Strings.sBlank(contentType, req.getContentType))
            )
        }

        val model = AbstractFreemarkerView.createModel(template.getObjectWrapper, req)

        val param = obj match {
            case m: scala.collection.Map[_, _] => mapAsJavaMap(m)
            case l: Seq[_] => seqAsJavaList(l)
            case any => any
        }

        model.put("model", param)

        template.process(model, resp.getWriter)
    }
}

object AbstractFreemarkerView {
    def renderAsString(templatePath: String, obj: scala.Any = null)(implicit req: HttpServletRequest, conf: Configuration): String = {
        val template = conf.getTemplate(templatePath)

        val model = createModel(template.getObjectWrapper, req)
        val param = obj match {
            case m: scala.collection.Map[_, _] => mapAsJavaMap(m)
            case l: Seq[_] => seqAsJavaList(l)
            case any => any
        }

        model.put("model", param)

        val sw = new StringWriter
        template.process(model, sw)
        sw.toString
    }

    def createModel(wrapper: ObjectWrapper, req: HttpServletRequest) = {
        if (null == req) {
            val params = new SimpleHash()
            params.put("static", BeansWrapper.getDefaultInstance.getStaticModels)
            params.put("enum", BeansWrapper.getDefaultInstance.getEnumModels)
            params
        }
        else {
            val params = new HttpScopesHashModel(wrapper, req)
            params.put("static", BeansWrapper.getDefaultInstance.getStaticModels)
            params.put("enum", BeansWrapper.getDefaultInstance.getEnumModels)
            params.put("req", req)
            params
        }
    }
}
