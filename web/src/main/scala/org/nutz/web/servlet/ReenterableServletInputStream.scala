package org.nutz.web.servlet

import java.io.ByteArrayInputStream
import javax.servlet.ServletInputStream

/**
 * 可以重入的ServletInputStream，即可以调用reset并重新读取。
 */
class ReenterableServletInputStream(stream: ByteArrayInputStream) extends ServletInputStream {
    override def read(): Int = stream.read()

    override def read(b: Array[Byte]): Int = stream.read(b)

    override def read(b: Array[Byte], off: Int, len: Int): Int = stream.read(b, off, len)

    override def skip(n: Long): Long = stream.skip(n)

    override def available(): Int = stream.available()

    override def close(): Unit = stream.close()

    override def mark(readLimit: Int): Unit = stream.mark(readLimit)

    override def reset(): Unit = stream.reset()

    override def markSupported(): Boolean = stream.markSupported()
}
