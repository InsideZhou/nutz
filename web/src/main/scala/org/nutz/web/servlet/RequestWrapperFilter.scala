package org.nutz.web.servlet

import javax.servlet._
import javax.servlet.http.HttpServletRequest
import org.nutz.web.WebUtils

class RequestWrapperFilter extends Filter {
    override def init(filterConfig: FilterConfig): Unit = {}

    override def doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain): Unit = {
        val req = new HttpServletRequestBase(request.asInstanceOf[HttpServletRequest])
        if (!WebUtils.isMultipart(req)) {
            chain.doFilter(new ReenterableHttpServletRequest(req), response)
        }

        chain.doFilter(req, response)
    }

    override def destroy(): Unit = {}
}
