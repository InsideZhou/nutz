package org.nutz.web.servlet

import java.util.regex.Pattern
import javax.servlet.http.HttpServletRequest
import javax.servlet.{ServletRequest, ServletResponse}

import org.apache.shiro.web.servlet.ShiroFilter
import org.nutz.lang.Strings

class NutShiroFilter extends ShiroFilter {
    var ignorePtn: Pattern = null

    override def init(): Unit = {
        super.init()

        val regexp = Strings.sNull(getFilterConfig.getInitParameter("ignore"), "^.+\\.(js|css|png|gif|jpg|jpeg|bmp|swf|ico)$")
        ignorePtn = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE)
    }

    override def isEnabled(request: ServletRequest, response: ServletResponse): Boolean = {
        !ignorePtn.matcher(request.asInstanceOf[HttpServletRequest].getRequestURI).find
    }
}
