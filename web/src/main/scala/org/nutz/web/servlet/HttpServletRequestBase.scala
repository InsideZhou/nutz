package org.nutz.web.servlet

import javax.servlet.http.{HttpServletRequest, HttpServletRequestWrapper}
import org.nutz.web.WebUtils

class HttpServletRequestBase(req: HttpServletRequest) extends HttpServletRequestWrapper(req) {
    override def getRemoteAddr: String = WebUtils.getRealIP(req)
}
