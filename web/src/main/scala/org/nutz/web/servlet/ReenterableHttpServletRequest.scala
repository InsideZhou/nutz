package org.nutz.web.servlet

import java.io.{BufferedReader, ByteArrayInputStream, InputStreamReader}
import javax.servlet.ServletInputStream
import javax.servlet.http.HttpServletRequest

import org.nutz.lang.Streams
import org.nutz.web.WebUtils

import scala.collection.JavaConversions._

/**
 * 使用可重入的ServletInputStream，所以Request Body可以反复重置并读取。
 * @param req 原始的请求
 */
class ReenterableHttpServletRequest(req: HttpServletRequest) extends HttpServletRequestBase(req) {
    assert(!WebUtils.isMultipart(req), "不支持multipart request")

    // 后续的初始化可能对InputStream可重入特性有依赖，所以stream必须首先初始化。
    private val stream = new ByteArrayInputStream(Streams.readBytes(req.getInputStream))

    private val queryMap = WebUtils.getQueryParameterMap(req, null)
    private var parameterMap = if (!"POST".equalsIgnoreCase(req.getMethod)) {
        queryMap
    }
    else {
        WebUtils.getFormParameterMap(this, null) ++ queryMap
    }

    /**
     * 这个过程是不可逆的
     */
    def escapeParameterValues() = {
        parameterMap = WebUtils.escapeValues(parameterMap)
    }

    override def getInputStream: ServletInputStream = {
        stream.reset()
        if (stream.available() == 0) return null

        new ReenterableServletInputStream(stream)
    }

    override def getReader: BufferedReader = {
        val s = getInputStream
        if (null == s) return null

        Streams.buffr(new InputStreamReader(getInputStream, WebUtils.getEncoding(req)))
    }

    override def getParameter(name: String): String = parameterMap.get(name) match {
        case Some(any: String) => any
        case Some(any: Array[String]) => any.head
        case _ => null
    }

    override def getParameterMap: java.util.Map[String , Array[String]] = parameterMap.mapValues {
        case any: String => Array(any)
        case any: Array[String] => any
        case _ => null
    }

    override def getParameterNames: java.util.Enumeration[String] = parameterMap.keysIterator

    override def getParameterValues(name: String): Array[String] = parameterMap.get(name) match {
        case Some(any: String) => Array(any)
        case Some(any: Array[String]) => any
        case _ => null
    }
}
