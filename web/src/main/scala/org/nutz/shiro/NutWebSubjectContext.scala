package org.nutz.shiro

import javax.servlet.http.HttpServletRequest
import org.apache.shiro.web.subject.support.DefaultWebSubjectContext
import org.nutz.web.WebUtils

class NutWebSubjectContext extends DefaultWebSubjectContext {
    override def resolveHost(): String = WebUtils.getRealIP(resolveServletRequest().asInstanceOf[HttpServletRequest])
}
