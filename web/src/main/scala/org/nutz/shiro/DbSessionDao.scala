package org.nutz.shiro

import java.io.Serializable
import java.util

import org.apache.shiro.session.Session
import org.apache.shiro.session.mgt.eis.CachingSessionDAO
import org.nutz.log.Logs
import org.nutz.orm.TableName

/**
 * <p>
 * 当客户端session丢失（如：浏览器清空cookie）时，虽然shiro也能清除这些失效session，
 * 但其使用的是[[DbSessionDao]].getActiveSessions方法（该方法实际上获取了当前所有session），
 * 然后逐个判断并删除失效session，在大量session存在的情况下，可以预见这种方式产生的数据库IO较多，
 * 所以负责实际存储session的数据库应当根据startTimestamp、timeout的值自行清除无效session。
 * </p>
 * <p>
 * 继承链中[[org.apache.shiro.session.mgt.eis.AbstractSessionDAO]]的assignSessionId耦合了SimpleSession，
 * 如果不使用默认的SimpleSession的话，必须重写这个方法，覆盖这个耦合。
 * </p>
 * <p>
 * session ID必须是字符串。
 * </p>
 */
class DbSessionDao extends CachingSessionDAO {
    val dao = ShiroWebSettings.dbSessionDao
    val tbName = ShiroWebSettings.dbSessionTableName
    val log = Logs.get()

    override def assignSessionId(session: Session, sessionId: Serializable): Unit = {
        session.asInstanceOf[DbSession].setId(sessionId.asInstanceOf[String])
    }

    override def doReadSession(sessionId: Serializable): Session = {
        sessionId.asInstanceOf[String] match {
            case null => null
            case sid =>
                var session: DbSession = null
                TableName.run(tbName, new Runnable() {
                    def run(): Unit = {
                        session = try {
                            dao.fetch(classOf[DbSession], sid)
                        }
                        catch {
                            case e: Throwable =>
                                log.warn("读取session时，session生成失败", e)
                                null
                        }
                    }
                })
                session
        }
    }

    override def doCreate(session: Session): Serializable = {
        val sessionId = this.generateSessionId(session)
        this.assignSessionId(session, sessionId)

        val bean = DbSession(session)
        bean.generateAttributesData()

        TableName.run(tbName, new Runnable() {
            def run(): Unit = {
                dao.insert(bean)
            }
        })

        sessionId
    }

    override def getActiveSessions: util.Collection[Session] = {
        var list: util.Collection[Session] = null
        TableName.run(tbName, new Runnable() {
            def run(): Unit = {
                list = dao.query(classOf[DbSession], null).asInstanceOf[util.Collection[Session]]
            }
        })

        list
    }

    override def doDelete(s: Session): Unit = {
        val session = s.asInstanceOf[DbSession]

        TableName.run(tbName, new Runnable() {
            def run(): Unit = {
                dao.delete(classOf[DbSession], session.getId)
            }
        })
    }

    override def doUpdate(s: Session): Unit = {
        val session = s.asInstanceOf[DbSession]
        session.generateAttributesData()

        TableName.run(tbName, new Runnable() {
            def run(): Unit = {
                dao.update(session)
            }
        })

    }
}
