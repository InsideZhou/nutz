package org.nutz.shiro

import org.apache.shiro.session.Session
import org.apache.shiro.session.mgt.{SessionContext, SessionFactory}

class DbSessionFactory extends SessionFactory {
    override def createSession(initData: SessionContext): Session = initData match {
        case null => new DbSession
        case _ =>
            val s = new DbSession
            s.setHost(initData.getHost)
            s
    }
}
