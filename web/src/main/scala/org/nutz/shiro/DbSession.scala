package org.nutz.shiro

import java.sql.ResultSet
import java.util
import java.util.{Date, UUID}

import org.apache.shiro.session.mgt.ValidatingSession
import org.apache.shiro.session.{InvalidSessionException, Session}
import org.nutz.lang.Lang
import org.nutz.orm.entity.Record
import org.nutz.orm.entity.annotation._

import scala.beans.{BeanProperty, BooleanBeanProperty}
import scala.collection.JavaConversions._

/**
 * 在非web环境下使用时要特别注意，DefaultSessionManager这个类中耦合了SimpleSession，所以必须自定义一个SessionManager覆盖这个耦合。
 */
@Table("${tbName}")
class DbSession(rs: ResultSet) extends ValidatingSession with Serializable {
    def this() {
        this(null)
    }

    @BeanProperty
    @Override
    @Name
    @ColDefine(`type` = ColType.CHAR, width = 36)
    var id = ""

    @BeanProperty
    @Override
    @Column
    var startTimestamp = new Date

    @BeanProperty
    @Override
    @Column
    var lastAccessTime = new Date

    @BeanProperty
    @Override
    @Column
    var timeout = 3600000L //毫秒

    @BeanProperty
    @Override
    @Column
    @ColDefine(width = 64)
    var host = ""

    @BooleanBeanProperty
    @Column
    var stopped = false

    @Column
    @ColDefine(`type` = ColType.BINARY)
    var attributes: Array[Byte] = null

    @BeanProperty
    val attrMap = new util.HashMap[Any, Any]()

    if (null != rs) {
        id = rs.getString("id")
        startTimestamp = rs.getTimestamp("startTimestamp")
        lastAccessTime = rs.getTimestamp("lastAccessTime")
        timeout = rs.getLong("timeout")
        host = rs.getString("host")
        stopped = rs.getBoolean("stopped")

        rs.getBytes("attributes") match {
            case null =>
            case attrBytes => attrMap.putAll(Lang.bytesToMap(attrBytes))
        }
    }

    def generateAttributesData(): Unit = {
        this.attributes = Lang.mapToBytes(this.attrMap)
    }

    @throws(classOf[InvalidSessionException])
    override def removeAttribute(key: scala.Any): AnyRef = {
        if (!isValid) {
            throw new InvalidSessionException
        }

        attrMap.remove(key).asInstanceOf[AnyRef]
    }

    @throws(classOf[InvalidSessionException])
    override def setAttribute(key: scala.Any, value: scala.Any): Unit = {
        if (!isValid) {
            throw new InvalidSessionException
        }

        attrMap.put(key, value)
    }

    @throws(classOf[InvalidSessionException])
    override def getAttribute(key: scala.Any): AnyRef = {
        if (!isValid) {
            throw new InvalidSessionException
        }

        attrMap.get(key) match {
            case v if null != v => v.asInstanceOf[AnyRef]
            case _ => null
        }
    }

    @throws(classOf[InvalidSessionException])
    override def getAttributeKeys: util.Collection[AnyRef] = {
        if (!isValid) {
            throw new InvalidSessionException
        }

        attrMap.keys.map(key => {
            key.asInstanceOf[AnyRef]
        })
    }

    @throws(classOf[InvalidSessionException])
    override def stop(): Unit = {
        if (!isValid) {
            throw new InvalidSessionException
        }

        stopped = true
    }

    @throws(classOf[InvalidSessionException])
    override def touch(): Unit = {
        if (!isValid) {
            throw new InvalidSessionException
        }

        lastAccessTime = new Date
    }

    @throws(classOf[InvalidSessionException])
    override def validate(): Unit = {
        if (!isValid) {
            throw new InvalidSessionException
        }
    }

    override def isValid: Boolean = {
        !stopped && (timeout < 0 || lastAccessTime.getTime + timeout > System.currentTimeMillis)
    }
}

object DbSession {
    def apply(session: Session) = {
        val s = new DbSession

        if (null == session.getId) {
            s.id = UUID.randomUUID.toString
        }
        else {
            s.id = session.getId.asInstanceOf[String]
        }

        s.startTimestamp = session.getStartTimestamp
        s.lastAccessTime = session.getLastAccessTime
        s.timeout = session.getTimeout
        s.host = session.getHost

        session.getAttributeKeys.foreach(key => {
            s.attrMap.put(key, session.getAttribute(key))
        })

        s
    }

    def apply(record: Record) = {
        val s = new DbSession

        s.id = record.getString("id")
        s.startTimestamp = record.getTimestamp("startTimestamp")
        s.lastAccessTime = record.getTimestamp("lastAccessTime")
        s.timeout = record.get("timeout").asInstanceOf[Long]
        s.host = record.getString("host")
        s.stopped = record.get("stopped").toString.toBoolean

        record.get("attributes") match {
            case attrBytes: Array[Byte] => s.attrMap.putAll(Lang.bytesToMap(attrBytes))
            case _ =>
        }

        s
    }
}
