package org.nutz.shiro

import org.nutz.orm.{Dao, TableName}

object ShiroWebSettings {
    var dbSessionDao: Dao = null
    var dbSessionTableName: String = null
    
    def init(dao: Dao, tbName: String = "shiro_session") = {
        dbSessionDao = dao
        dbSessionTableName = tbName

        TableName.run(dbSessionTableName, new Runnable {
            override def run(): Unit = {
                dao.create(classOf[DbSession], false)
            }
        })
    }
}
