package org.nutz.http.sender

import javax.servlet.http.HttpServletRequest

import org.nutz.Settings
import org.nutz.http.{Sender, Request, Response, Header}
import org.nutz.lang.Streams
import org.nutz.web.WebUtils

import scala.collection.JavaConversions._

/**
 * 请求转发代理。
 */
class ProxySender(req: Request) extends Sender(req) {
    override def send(): Response = {
        openConnection()
        val input = request.getInputStream
        setupRequestHeader()
        setupDoInputOutputFlag()
        if (null != input) {
            val output = Streams.buff(conn.getOutputStream)
            Streams.write(output, input, Settings.IO_BUFFER_SIZE)
            Streams.safeClose(input)
            Streams.safeFlush(output)
            Streams.safeClose(output)
        }

        createResponse(getResponseHeader)
    }
}

object ProxySender {
    def apply(req: HttpServletRequest, targetUrl: String, headers: Map[String, String] = null) = {
        val h = if (null == headers) {
            WebUtils.getHeaderMap(req)
        }
        else {
            headers
        }

        val ct = if (null == req.getContentType) {
            ""
        }
        else {
            req.getContentType.split(';')(0).toLowerCase
        }
        val method = req.getMethod.toUpperCase
        val request = if (method.equals("POST")) {
            val request = Request.post(targetUrl, Header.create(h))
            if (ct.startsWith("multipart")) {
                request.setInputStream(req.getInputStream)
            }
            else {
                request.setData(WebUtils.getBodyRawStr(req))
            }

            request
        }
        else {
            Request.get(targetUrl, Header.create(headers))
        }

        new ProxySender(request)
    }
}
