package org.nutz.web

import java.net.URLDecoder

import org.junit.runner.RunWith
import org.scalatest.FreeSpec
import org.scalatest.junit.JUnitRunner

import scala.collection.mutable

@RunWith(classOf[JUnitRunner])
class WebUtilsSpec extends FreeSpec {
    "split" - {
        "state" in {
            assert("state".split("=").length == 1)
            assert("state".split("=", 2).length == 1)
            assert("state".split("=", 5).length == 1)
            assert("state".split("=", 2)(0) == "state")
        }
        "state=" in {
            assert("state=".split("=").length == 1)
            assert("state=".split("=", 2).length == 2)
            assert("state=".split("=", 2)(1) == "")
            assert("state=prop={'abc':'123'}".split("=", 2)(1) == "prop={'abc':'123'}")
        }
        "null&startDate=2014-08-30T19%3A57%3A00&pageNum=2&states=&pageSize=30&endDate=2014-09-30T19%3A57%3A00&null" in {
            val qs = URLDecoder.decode("null&startDate=2014-08-30T19%3A57%3A00&pageNum=2&states=&pageSize=30&endDate=2014-09-30T19%3A57%3A00&null", "UTF-8")
            qs.split("&").foreach(item => {
                val result = WebUtils.splitParameterItem(item) match {
                    case ("startDate", "2014-08-30T19:57:00") => true
                    case ("endDate", "2014-09-30T19:57:00") => true
                    case ("pageNum", "2") => true
                    case ("pageSize", "30") => true
                    case ("states", "") => true
                    case ("null", "") => true
                    case any =>
//                        println(any)
                        false
                }

                assert(result)
            })
        }
        "redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Fmall%2FoauthRedirect%3FreturnUrl%3Dhttp%3A%2F%2Flocalhost%3A8080%2Fmall%2Fuser%2F" in {
            val qs = URLDecoder.decode("redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Fmall%2FoauthRedirect%3FreturnUrl%3Dhttp%3A%2F%2Flocalhost%3A8080%2Fmall%2Fuser%2F", "UTF-8")
            qs.split("&").foreach(item => {
                val result = WebUtils.splitParameterItem(item) match {
                    case ("redirect_uri", "http://localhost:8080/mall/oauthRedirect?returnUrl=http://localhost:8080/mall/user/") => true
                    case any =>
//                        println(any)
                        false
                }

                assert(result)
            })

            val result = WebUtils.splitParameterItem("redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Fmall%2FoauthRedirect%3FreturnUrl%3Dhttp%3A%2F%2Flocalhost%3A8080%2Fmall%2Fuser%2F") match {
                case ("redirect_uri", "http%3A%2F%2Flocalhost%3A8080%2Fmall%2FoauthRedirect%3FreturnUrl%3Dhttp%3A%2F%2Flocalhost%3A8080%2Fmall%2Fuser%2F") => true
                case any =>
//                    println(any)
                    false
            }

            assert(result)
        }
        "sort=[\"+null\"]" in {
            val (k, v) = WebUtils.splitParameterItem("sort=[\"+null\"]")
            assert("sort" == k)
            assert("[\"+null\"]" == v)
        }
        "state=&scope=&redirect_uri=http%3A%2F%2Faizhubao.admin.popalm.com%2FoauthRedirect%3FreturnUrl%3Dhttp%3A%2F%2Faizhubao.admin.popalm.com%2Fgoods%2Flist&client_id=dNvxKw4ZHOuzX7WIilLHA-&response_type=code" in {
            val qs = URLDecoder.decode("state=&scope=&redirect_uri=http%3A%2F%2Faizhubao.admin.popalm.com%2FoauthRedirect%3FreturnUrl%3Dhttp%3A%2F%2Faizhubao.admin.popalm.com%2Fgoods%2Flist&client_id=dNvxKw4ZHOuzX7WIilLHA-&response_type=code", "UTF-8")
            qs.split("&").foreach(item => {
                val (k, v) = WebUtils.splitParameterItem(item, "=")
//                println(s"$k, $v")
            })
        }
    }
    "query to map" in {
        val map = mutable.HashMap[String, Any]()

        val content = "gem.OID=&gem.name=小小卖珠宝&gem.props[100].propGroup=图片&gem.props[100].code=IMAGE_INDEX_100&gem.props[100].name=图片100&gem.props[100].propValue=2014-12/JLbjJsJ8HZLQBhYIaZgJA1.1.origin.jpg&gem.props[200].propGroup=图片&gem.props[200].code=IMAGE_DETAILS_200&gem.props[200].name=图片200&gem.props[200].propValue=2014-12/pG5CJyNEGkeNDcb4yVtvc1.1.origin.jpg&gem.price=10000&gem.stock=1&gem.priceLower=6000&gem.bargainUpper=2000&gem.bargainLower=500&bargainDurationLimit=48 hours&gem.document=&gem.description=灰常好    &gem.categoryCode=MANAO&gem.props[300].name=尺寸&gem.props[300].code=p_CHICUN&gem.props[300].propValue=123&gem.props[301].name=重量&gem.props[301].code=p_ZHONGLIANG&gem.props[301].propValue=123&gem.props[302].name=瑕疵&gem.props[302].code=p_XIACI&gem.props[302].propValue=123&gem.props[303].name=描述&gem.props[303].code=p_DESCRIPTION&gem.props[303].propValue=123&gem.props[304].name=评价&gem.props[304].code=ac_PINGJIA&gem.props[304].propValue=123&gem.props[305].name=款式&gem.props[305].code=s_KUANSHI&gem.props[305].propValue=挂件&gem.props[400].propGroup=评级&gem.props[400].name=质地&gem.props[400].code=a_ZHIDI&gem.props[400].propValue=6&gem.props[401].propGroup=评级&gem.props[401].name=净度&gem.props[401].code=a_JINGDU&gem.props[401].propValue=6&gem.props[402].propGroup=评级&gem.props[402].name=颜⾊&gem.props[402].code=a_YANSE&gem.props[402].propValue=6&gem.props[403].propGroup=评级&gem.props[403].name=雕⼯&gem.props[403].code=a_DIAOGONG&gem.props[403].propValue=6&gem.props[404].propGroup=评级&gem.props[404].name=尺寸&gem.props[404].code=a_CHICUN&gem.props[404].propValue=6&gem.props[5].propGroup=销售&gem.props[5].code=ZHANSHIQUDAO&gem.props[5].name=展示渠道&gem.props[5].propValue=微信&gem.props[6].propGroup=销售&gem.props[6].code=SHIFOUTUIJIAN&gem.props[6].name=是否推荐&gem.props[6].propValue=是&gem.sort=&purchaseDurationLimit=&gem.props[7].propGroup=证书&gem.props[7].code=instCode&gem.props[7].name=机构识别码&gem.props[7].propValue=123123&gem.props[8].propGroup=证书&gem.props[8].code=certCode&gem.props[8].name=证书识别码&gem.props[8].propValue=123123&gem.props[9].propGroup=证书&gem.props[9].code=propMap&gem.props[9].name=证书属性&gem.props[9].propValue=123123"
        content.split("&").foreach {
            item =>
                WebUtils.splitParameterItem(item) match {
                    case (k, v) =>
                        WebUtils.handleParameterWithoutSep(k, v, map)
                }
        }

//        map.foreach(println)
    }
}
