> 基于[nutz](https://github.com/nutzam/nutz)  
混合了[apache commons lang](http://commons.apache.org/proper/commons-lang/)的部分代码


### 版本号说明
**`target`.`compatible`.`release`**

- target 目标环境
	- 0 通用Java环境
	- 1 android

	> 变化时清空其余版本号

- compatible 兼容版本号
	> 破坏兼容时版本号+1
	变化时清空发布版本号

- release 发布版本号
	> 每次发布时版本号+1


### 目前包括的模块
- core
> 基础代码都在这个模块，纯Java

- ext
> 对core的扩展，混合了scala代码

- http
> 基于spray提供http支持

- logback
> 对logback的支持和扩展

- orm
> 就是nutz原来的dao模块

- shiro
> 对shiro的支持和扩展

- web
> web的基础支持

- mvc
> 就是nutz原来的mvc模块

- spring-mvc
> 提供对spring mvc的支持


*注意：不是特别必要，不在core中增加新特性，新增的功能特性放到ext中。*