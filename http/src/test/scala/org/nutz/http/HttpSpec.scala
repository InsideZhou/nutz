package org.nutz.http

import org.junit.runner.RunWith
import org.scalatest.FreeSpec
import org.scalatest.junit.JUnitRunner

import scala.collection.JavaConversions._

@RunWith(classOf[JUnitRunner])
class HttpSpec extends FreeSpec {
    "encode" - {
        "map" in {
            assert("zhoujian=%E5%91%A8%E5%89%91&qinains=%E8%A6%83%E4%BA%AE%E6%9C%8B&chenjidong=%E9%99%88%E7%A7%AF%E4%B8%9C"
                == Http.toQuery(Map("zhoujian" -> "周剑", "qinains" -> "覃亮朋", "chenjidong" -> "陈积东")))
        }
    }
}
