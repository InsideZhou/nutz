package org.nutz.http

import org.junit.runner.RunWith
import org.scalatest.FreeSpec
import org.scalatest.junit.JUnitRunner
import spray.http.Uri

@RunWith(classOf[JUnitRunner])
class SpraySpec extends FreeSpec {
    "Uri" - {
        "Query" - {
            "value cannot be null" in {
                intercept[NullPointerException] {
                    val query = Uri.Query(Map("zhoujian" -> "周剑", "qinains" -> "覃亮朋", "chenjidong" -> "陈积东", "nobody" -> null))
                    query.toString()
                }
            }
        }
    }
}
