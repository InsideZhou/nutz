package org.nutz.http

import java.io.File

import akka.actor.ActorRefFactory
import org.nutz.json.{Json, JsonFormat}
import org.nutz.lang.{Lang, Strings}
import org.nutz.mapl.Mapl
import spray.client.pipelining._
import spray.http._
import spray.httpx.marshalling._

import scala.collection.JavaConversions._
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.reflect.ClassTag

/**
 * 提供通过http发送请求的服务。
 */
abstract class ClientService[T: ClassTag](implicit actorRefFactory: ActorRefFactory) extends FormDataMarshaller with JsonUnmarshaller with ResponseSupport {
    implicit var executionContext: ExecutionContextExecutor = actorRefFactory.dispatcher

    implicit val objectToJsonMarshaller = Marshaller.delegate[T, String](ContentTypes.`application/json`) {
        (en) => toJson(en)
    }

    implicit val mapToFormDataMarshaller = Marshaller.delegate[Map[String, _], FormData](MediaTypes.`application/x-www-form-urlencoded`.withCharset(HttpCharsets.`UTF-8`)) {
        (map) => toFormData(map)
    }

    implicit val entityUnmarshaller = jsonUnmarshaller[T]
    implicit val entityListUnmarshaller = jsonListUnmarshaller[T]

    def toJson(en: Any) = Json.toJson(en, JsonFormat.compact().setIgnoreNull(false))

    def toFormData(en: Any): FormData = {
        val map = en match {
            case m: Map[_, _] => m
            case _ =>
                Mapl.toMaplist(en) match {
                    case map: java.util.Map[_, _] => map.toMap
                    case _ => throw Lang.makeThrow("对象无法转成FormData")
                }
        }

        FormData(map.map {
            case (k, v) if null != k && null != v => (k.toString, v.toString)
            case (k, v) if null != k => (k.toString, null)
            case (k, v) if null != k => (null, null)
        })
    }

    def upload(uri: Uri, params: Map[String, _]): HttpRequest = {
        val parts = params.map {
            case (k, part: BodyPart) => k -> part
            case (k, f: File) =>
                java.nio.file.Files.probeContentType(f.toPath) match {
                    case null => k -> BodyPart(f, k)
                    case ct => k -> BodyPart(f, k, ContentType(MediaType.custom(ct)))
                }

            case (k, v) => k -> BodyPart(HttpEntity(Strings.sNull(v)), k)
        }

        val form = MultipartFormData(parts)
        Post(uri, form)
    }

    def post(uri: Uri, params: Map[String, _]): HttpRequest = Post(uri, params)

    def post[A](uri: Uri, en: A)(implicit marshaller: Marshaller[A]): HttpRequest = Post(uri, en)

    def post(uri: String, params: Map[String, _]): HttpRequest = post(Uri(uri), params)

    def post(uri: String, en: T): HttpRequest = post(Uri(uri), en)

    def get(uri: Uri): HttpRequest = Get(uri)

    def get(uri: String): HttpRequest = get(Uri(uri))

    def get(path: String, params: Map[String, String]): HttpRequest = get(Uri(path).withQuery(params))

    def ask(req: HttpRequest): Future[HttpResponse] = {
        val pl = HttpUtils.pipeline()
        pl(req)
    }

    def ask[A](req: HttpRequest, unmarshaller: (HttpResponse) => A): Future[A] = {
        val pl = HttpUtils.pipeline() ~> unmarshaller
        pl(req)
    }

    def askReport(req: HttpRequest) = {
        val pl = HttpUtils.pipeline()
        pl(req) map {
            case resp: HttpResponse => resp.status.isSuccess
        }
    }

    def askEntity(req: HttpRequest) = {
        val pl = HttpUtils.pipeline() ~> unmarshal[T]
        pl(req)
    }

    def askEntityList(req: HttpRequest) = {
        val pl = HttpUtils.pipeline() ~> unmarshal[java.util.List[T]]
        pl(req)
    }
}

