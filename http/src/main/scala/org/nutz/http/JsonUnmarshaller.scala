package org.nutz.http

import org.nutz.json.Json
import org.nutz.log.Logs
import spray.http._
import spray.httpx.unmarshalling.{Deserialized, FromResponseUnmarshaller, MalformedContent}

import scala.reflect.ClassTag

trait JsonUnmarshaller {
    private val log = Logs.get()

    def jsonUnmarshaller[T: ClassTag] = new FromResponseUnmarshaller[T] {
        val runtimeCls = implicitly[ClassTag[T]].runtimeClass

        override def apply(resp: HttpResponse): Deserialized[T] = resp.entity match {
            case HttpEntity.Empty => Right(null.asInstanceOf[T])

            case en =>
                try {
                    Right(Json.fromJson(runtimeCls, en.asString(HttpCharsets.`UTF-8`)).asInstanceOf[T])
                }
                catch {
                    case e: Throwable =>
                        log.info(en.asString(HttpCharsets.`UTF-8`))
                        Left(MalformedContent("无法解析响应内容", e))
                }
        }
    }

    def jsonListUnmarshaller[T: ClassTag] = new FromResponseUnmarshaller[java.util.List[T]] {
        val runtimeCls = implicitly[ClassTag[T]].runtimeClass

        override def apply(resp: HttpResponse): Deserialized[java.util.List[T]] = resp.entity match {
            case HttpEntity.Empty => Right(null.asInstanceOf[java.util.List[T]])

            case en =>
                try {
                    Right(Json.fromJsonAsList(runtimeCls, en.asString(HttpCharsets.`UTF-8`)).asInstanceOf[java.util.List[T]])
                }
                catch {
                    case e: Throwable =>
                        log.info(en.asString(HttpCharsets.`UTF-8`))
                        Left(MalformedContent("无法解析响应内容", e))
                }
        }
    }
}
