package org.nutz.http

import akka.actor.ActorRefFactory
import akka.util.Timeout
import org.nutz.log.Logs
import spray.client.pipelining._
import spray.http._
import spray.httpx.encoding.{Deflate, Gzip}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

object HttpUtils {
    private[this] val log = Logs.get()

    def pipeline(headers: HttpHeader*)(implicit factory: ActorRefFactory, context: ExecutionContext, timeout: Timeout = 1.minute) = {
        var pl = addHeader(HttpHeaders.`Accept-Encoding`(HttpEncodingRange(HttpEncodings.gzip), HttpEncodingRange(HttpEncodings.deflate)))

        if (headers.nonEmpty) {
            pl = pl ~> addHeaders(headers.toList)
        }

        if (log.isDebugEnabled) {
            pl ~> ((req: HttpRequest) => {
                log.debugf("%s\n%s", req, req.entity.asString(HttpCharsets.`UTF-8`))
                req
            }) ~> sendReceive(factory, context, timeout) ~> decode(Gzip) ~> decode(Deflate) ~>
                ((resp: HttpResponse) => {
                    log.debugf("%s\n%s", resp, resp.entity.asString(HttpCharsets.`UTF-8`))
                    resp
                })
        }
        else {
            pl ~> sendReceive(factory, context, timeout) ~> decode(Gzip) ~> decode(Deflate)
        }
    }
}
