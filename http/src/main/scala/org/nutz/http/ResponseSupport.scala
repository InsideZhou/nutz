package org.nutz.http

import org.nutz.lang.Lang
import spray.http.HttpResponse
import spray.httpx.unmarshalling._
import spray.httpx.{PipelineException, UnsuccessfulResponseException}

trait ResponseSupport {
    def failResponseHandler[A](resp: HttpResponse): A = {
        throw Lang.makeThrow("[异常响应]%s", resp)
    }

    def deserializationFailHandler[T](error: DeserializationError, resp: HttpResponse): T = {
        error match {
            case err: MalformedContent =>
                err.cause match {
                    case Some(e) => throw new PipelineException(err.errorMessage, e)
                    case None => throw new PipelineException(err.toString)
                }

            case err => throw new PipelineException(err.toString)
        }
    }

    def unmarshal[A: FromResponseUnmarshaller]: HttpResponse => A =
        response =>
            if (response.status.isSuccess) {
                response.as[A] match {
                    case Right(value) => value
                    case Left(error) => deserializationFailHandler[A](error, response)
                }
            }
            else if (response.status.intValue < 500 && response.entity.nonEmpty) {
                failResponseHandler(response)
            }
            else {
                throw new UnsuccessfulResponseException(response)
            }
}
