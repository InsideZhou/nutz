package org.nutz.http

import spray.http._
import spray.httpx.marshalling.Marshaller

import scala.collection.JavaConversions._

trait FormDataMarshaller {
    implicit val utf8FormDataMarshaller = Marshaller.delegate[FormData, HttpEntity](MediaTypes.`application/x-www-form-urlencoded`.withCharset(HttpCharsets.`UTF-8`)) {
        (formData, contentType) =>
            val charset = contentType.charset.nioCharset
            HttpEntity(contentType, Http.toQuery(Map(formData.fields: _*), charset.name()))
    }
}
